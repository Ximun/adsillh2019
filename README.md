# Notes de cours en commun

Ce dépôt centralise les notes de cours mises en commun par (et pour) toutes et tous les étudiants de la licence ADSILLH 2019.

## Liens utiles

### Pads collaboratifs

- Notes de cours: [https://pad.Aquilenet.fr/p/adsillh2019](https://pad.aquilenet.fr/p/adsillh2019)
- Notes pour les projets tuteurés: [https://pad.aquilenet.fr/p/projets-adsillh-2019](https://pad.aquilenet.fr/p/projets-adsillh-2019)

### Discord

zM8FcFV

### Cloud

- Instance NextCloud: [https://cloud.cestlebouquet.fr/index.php/s/PGY6yizpD69Nnqy](https://cloud.cestlebouquet.fr/index.php/s/PGY6yizpD69Nnqy)

### Wikis

- Pizzacoca [http://pidie.pizzacoca.fr/redmine/projects/licence-adsillh/wiki](http://pidie.pizzacoca.fr/redmine/projects/licence-adsillh/wiki)

### Blog

- Site HUGO de Yorick [https://git.epha.se/ephase/cours_lpro-ADSILLH](https://git.epha.se/ephase/cours_lpro-ADSILLH)
