---
title: "Faire Un Bug Report"
date: 2019-10-14T15:06:46+02:00
draft: true
---

J'ai découvert un bug dans un des outils que j'utilise, mais avant de faire un
bug report et pour éviter de me planter, j'aimerais m'y prendre correctement si
possible. Voilà un peu d'aide vue en cours...

## Donner du contexte

- Type du système d'exploitation
- Version de l'OS
- Environnement de bureau (pour un bug graphique par exemple)
- Version du logiciel
- Certains projets fournissent un outil pour préparer cette partie
- Écran ou composant impacté

## Décrire les étapes de reproduction

## Décrire le résultat effectif

## Décrire le résultat attendu

## Donner d'autres éléments si possible

- Logs
- Captures d'écran
- ...

# Les différentes phases

## Ouverture du ticket (par le rapporteur)

## Qualification (triage)

- Détection du module ou de la fonction impactée, à partir des information
fourniezs par le rapporteur
- Assignation au responsable du module, de la fonction impactée
- Définition si c'est une réelle anomalie ou un comportement normal

## recherche / Diagnostic / Correction

Le responsable va essayer de reproduire le bug, trouver où cela se situe et
tenter de corriger le bug.

- Reproduciton
- Demande d'informations complémentaires
- Correction (patch, pull request)

## Clôture

- Publication d'une nouvelle version
- Validation de la correction, par le rapporteur ou un autre contributeur
