---
title: "Radicale"
date: 2019-09-19T13:20:35+02:00
draft: false
---

[Radicale](https://radicale.org) est un serveur ultra-léger pour héberger des listes de contact,
agendas, journaux, todolist et quelques autres et les partager via CardDAV, CalDAV et HTTP.
Radicale va nous être utile dans le contexte du projet tuteuré, car on va s'en servir pour
héberger les calendriers qui vont se synchroniser avec **vdirsyncer** pour ensuite être traités
par **khal**, le logiciel auquel nous comptons contribuer.

## Installation de la VM

Pour tester radicale, installons-le sur une VM de Debian Buster. On partira du postulat que la VM
est installée et accessible via une adresse IP locale. Dans notre exemple:
192.168.122.137.

Connectons-nous dessus et commençons à installer ce dont nous avons besoin

```bash
# J'ai décidé d'installer radicale sur un environnement virtuel 
# python. Si ce n'est pas fait il faut installer l'outil
# virtualenv de python pour les gérer. (On part du principe
# qu'on n'utilise que python3)
sudo apt-get install python3-virtualenv
sudo apt-get install python3-pip

# Maintenant on crée l'environnement virtuel qui nous servira à
# installer et faire tourner radicale, sans oublier de
# sélectionner python 3 comme interpréteur python par défaut !
sudo mkdir /envs
sudo python3 -m virtualenv -p python3 /envs/radicale

# Maintenant qu'il est créé, on se place dans notre environnement
# virtuel On devrait avoir (radicale) devant notre prompt shell
# pour indiquer Qu'on est bien dedans...
source /envs/radicale/bin/activate

# On y installe notre radicale et on crée à l'intérieur de notre
# environnement virtuel le dossier qui accueillera les données de
# radicale. Vous pouvez le mettre ailleurs si vous voulez...
pip install radicale
sudo mkdir /envs/radicale/collections
deactivate
```

Normalement, l'installation de radicale est terminée. On peut vérifier ça en lançant
un petit ```/envs/radicale/bin/radicale --help```

## Installation du serveur web

Maintenant, on va mettre en place le serveur web qui va reccueillir les requêtes
HTTP destinées à radicale de la part des clients et les rediriger vers l'application.
On va utiliser Apache pour cet exemple, avec lequel on paramètrera un
[reverse proxy](https://fr.wikipedia.org/wiki/Proxy_inverse)

```bash
# Alors on installe Apache
sudo apt-get install apache2

# On va avoir besoin de mettre en place un reverse proxy
# Donc on doit activer le module apache qui s'occupe de ça
sudo a2enmod proxy_http

# Maintenant, écrivons la configuration de notre serveur
# Apache pour radicale
sudo vi /etc/apache2/sites-available/000-radicale.conf
```

Voici une configuration très sommaire pour faire fonctionner radicale dans une VM.
Si vous vouliez l'installer sur un serveur accessible depuis l'Internet, il faudrait
s'occuper de la sécurité, comme installer les ceritificats nécessaires à https, etc.
Dans ce cas là, on simplifie les choses...

```bash
<VirtualHost *:80>
    # On pourrait choisir un ServerName puis modifier le
    # /etc/hosts de notre machine hôte pour éviter d'avoir
    # à taper l'adresse IP de notre VM dans un navigateur
    # web. Faites-le si vous voulez, et décommentez cette
    # ligne dans ce cas là:
    #ServerName radicale.dev

    ErrorLog ${APACHE_LOG_DIR}/error_radicale.log
    CustomLog ${APACHE_LOG_DIR}/access_radicale.log combined
    
    ProxyPreserveHost On
    ProxyRequests On
    ProxyPass / http://127.0.0.1:5232
    ProxyPassReverse / http://127.0.0.1:5232
</VirtualHost>
```
Maintenant, on active notre Virtual Host, on relance Apache et lance notre application
pour voir si ça fonctionne...

```bash
sudo ln -s /etc/apache2/sites-available/000-radicale.conf /etc/apache2/sites-enabled/000-radicale.conf
sudo systemctl restart apache2
sudo /envs/radicale/bin/radicale --config "" --storage-filesystem-folder=/envs/radicale/collections
```
![radicale interface web](/posts/img/radicale-webco.jpg)

Et voilà! Normalement, depuis un navigateur web de notre machine hôte, on n'a plus qu'à
taper l'adresse IP de notre VM et tomber sur notre interface de radicale.

## Configuration de radicale

On va maintenant écrire un peu de configuration pour nous simplifier la tâche. J'ai
pris le parti de mettre tous les fichiers de configuration à l'intérieur de mon
virtualenv, mais vous pouvez les mettre où bon vous semble...

Pour commencer, on va s'occuper de créer les utilisateurs et sécuriser leur
mots de passe grâce à la commande **htpasswd**, un outil qui vient avec
Apache et qui permet de stocker des usernames dans un fichier et les
associer à un mot de passe hashé par la fonciton **bcrypt**.

### Utilisateurs

```bash
$ sudo htpasswd -B -c /envs/radicale/users Ximun
New password:
Re-type new password:

# Et pour ajouter des utilisateurs
$ sudo htpasswd -B /envs/radicale/users Link
New password:
Re-type new password:
```
Nos utilisateurs sont créés. Cependant radicale a besoin d'une dépendance
pour pouvoir utiliser l'authentification par bcrypt:

```bash
# On s'assure d'abord qu'on est bien dans notre
# virtualenv...
source /envs/radicale/bin/activate
pip install --upgrade radicale[bcrypt]
```

### Fichier de conf

Ensuite, on écrit un peu de configuration dans un fichier ```/envs/radicale/config```

```bash
# On déclare notre système d'authentification
[auth]
type = htpasswd
htpasswd_filename = /envs/radicale/users
htpasswd_encryption = bcrypt

# On ne permet la connexion au service uniquement depuis
# notre machine locale (car c'est le reverse proxy qui
# fait que l'on peut se connecter au service depuis
# l'extérieur
[server]
hosts = 0.0.0.0:5232

# Enfin on indique où nos données sont stockées
[storage]
filesystem_folder = /envs/radicale/collections
```
Maintenant, il devrait suffire de pointer vers l'emplacement de la config
en lançant la commande:

```bash
sudo /envs/radicale/bin/radicale --config /envs/radicale/config
```
Évidemment maintenant vous n'avez accès qu'aux comptes utilisateurs
déclarés avec ```htpasswd```.

## Running as a service

Pour terminer, on va créer le service systemd qui nous permettra de gérer l'application
comme n'importe quel autre service, ce qui nous permettra entre autres de facilement
la lancer au démarrage du serveur et garder un œil dessus...

### Créer un user system

On va commencer par créer un utilisateur système **radicale** à travers qui le service
va se lancer:

```bash
sudo useradd --system --shell /sbin/nologin radicale
```
L'étape d'après est de rendre le dossier de données des agendas uniquement accessible
en lecture/écriture/exécution à ce même utilisateur.

```bash
sudo chown -R radicale: /envs/radicale/collections
sudo chmod -R 700 /envs/radicale/collections
```

### Créer le service

Pour créer service, on édite le fichier ```/etc/systemd/system/radicale.service```:

```bash
[Unit]
Description=Un serveur léger pour CalDAV (calendriers) et CardDAV (contacts)
After=network.taraget
Requires=network.target

[Service]
ExecStart=/envs/radicale/bin/radicale --config /envs/radicale/config
Restart=on-failure
User=radicale
UMask=0077
PrivateTmp=true
ProtectHome=true
PrivateDevices=true
ProtectKernelTunables=true
ProtectKernelModules=true
ProtectControlGroups=true
NoNewPrivileges=true
ReadWritePaths=/envs/radicale/collections

[Install]
WantedBy=multi-user.target
```

### Lancer le service

Il n'y a plus qu'à lancer le service! :)

```bash
# lancer le service au démarrage du serveur
$ sudo systemctl enable radicale
# Démarrer le service
$ sudo systemctl start radicale
# Vérifier son status
$ systemctl status radicale
# Et lire les logs en temps réel...
journalctl -f --unit radicale.service
```
Et voilà, un beau service CalDAV et CardDAV auto-hébergé! :)
Vous pouvez consulter la
[configuration officielle](https://radicale.org/documentation/)
dont je me suis beaucoup inspiré tout en l'adaptant à mes pratiques.
Encore une fois, n'oubliez pas de sécuriser votre serveur Apache
si vous voulez l'installer sur une machine publique!

N'hésitez pas à créer vos agendas, on s'en servira dans
[la suite avec vdirsyncer](/posts/vdirsyncer) ;)


