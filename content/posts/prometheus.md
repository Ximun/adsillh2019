---
title: "Prometheus"
date: 2019-11-21T09:28:44+01:00
draft: true
---

# Prometheus

Dans le cadre de notre alternance chez [yaal](https://yaal.fr), nous allons
devoir mettre en place / utiliser un outil de supervision du nom de
**Prometheus**. Cet article décrit sa découverte...

Je me suis basé sur ces deux articles fort intéressants:

- https://computingforgeeks.com/how-to-install-prometheus-and-node-exporter-on-debian/
- https://linuxfr.org/news/decouverte-de-l-outil-de-supervision-prometheus

## Installation

Pour découvrir Prometheus, j'ai installé deux VM (sous Debian 10). L'une
accueille le serveur principal Prometheus, l'autre est un **node** qui receuille
des infos et les envoie au serveur principal...

![](../img/architecture-prometheus.svg)

### Installation serveur principal

#### Sources

```bash
# Création du groupe / utilisateur
sudo addgroup --system prometheus
sudo useradd /sbin/nologin --system -g prometheus prometheus

# Création des dossiers de conf et de lib
sudo mkdir /var/lib/prometheus
for i in rules rules.d files_sd; do sudo mkdir -p /etc/prometheus/${i}; \
done;

# Téléchargement des sources
sudo apt-get install -y wget
mkdir -p /tmp/prometheus && cd /tmp/prometheus
curl -s https://api.github.com/repos/prometheus/prometheus/releases/latest \
  | grep browser_download_url \
  | grep linux-amd64 \
  | cut -d '"' -f 4 \
  | wget -qi -

# Extraction
tar xvf prometheus*.tar.gz
cd prometheus*/

# Installation des binaires...
sudo mv prometheus promtool /usr/local/bin/

# ... du ficiher de configuration
sudo mv prometheus.yml /etc/prometheus/prometheus.yml

# ... et des consoles
sudo mv consoles/ console_libraries/ /etc/prometheus/
cd ~/
rm -rf /tmp/prometheus
```

#### Service systemd

Nous allons créer une service systemd qui va gérer le programme.

Création et édition du fichier `/etc/systemd/system/prometheus.service`

```bash
[Unit]
Description=Prometheus
Documentation=https://prometheus.io/docs/introduction/overview/
Wants=network-online.target
After=network-online.target

[Service]
type=simple
User=prometheus
Group=prometheus
ExecReload=/bin/kill -HUP $MAINPID
ExecStart=/usr/local/bin/prometheus
  --config.file=/etc/prometheus/prometheus.yml
  --storage.tsdb.path=/var/lib/prometheus
  --web.console.templates=/etc/prometheus/consoles
  --web.console.libraries=/etc/prometheus/console_libraries
  --web.listen-address=0.0.0.0:9090 

SyslogIdentifier=pometheus
Restart=always

[Install]
WantedBy=multi-user.agent
```

#### Changement de droits

```bash
for i in rules rules.d files_sd; do sudo chown -R prometheus:prometheus /etc/prometheus/${i}; done
for i in rules rules.d files_sd; do sudo chmod -R 775 /etc/prometheus/${i}; done
sudo chown -R prometheus:prometheus /var/lib/prometheus/
```

#### Démarrage du service

```bash
sudo systemctl daemon-reload
sudo systemctl start prometheus
sudo systemctl enable prometheus
```

### Installation du node

```bash
# Création du user
sudo addgroup --system node
sudo useradd /sbin/nologin --system -g node node

# Téléchargement de l'archive
curl -s https://api.github.com/repos/prometheus/node_exporter/releases/latest \
| grep browser_download_url \
| grep linux-amd64 \
| cut -d '"' -f 4 \
| wget -qi -

# Extraction des binaires
tar -xvf node_exporter*.tar.gz
cd  node_exporter*/
sudo cp node_exporter /usr/local/bin

# Confirmation de l'installation
node_exporter --version
node_exporter, version 0.18.1 (branch: HEAD, revision:3db77732e925c08f675d7404a8c46466b2ece83e)
  build user:   root@b50852a1acba
  build date:   20190604-16:41:18
  go version:   go1.12.5
```

#### Création du service systemd

Création/Édition du fichier `/etc/systemd/system/node_exporter.service`

```bash
[Unit]
Description=Node Exporter
Wants=network-oneline.target
After=network-oneline.target

[Service]
User=prometheus
ExecStart=/usr/local/bin/node_exporter

[Install]
WantedBy=default.target
```

#### Lancement du service

```bash
sudo systemctl daemon-reload
sudo systemctl start node_exporter
sudo systemctl enable node_exporter
```

#### Déclaration d'un nouveau node

Maintenant, il faut retourner sur notre serveur principal **Prometheus** et
déclarer nos nodes (au grand nombre de 1) dans la config
`/etc/prometheus/prometheus.yml`.

L'idée ici est de rajouter un **job** dans lequel on va déclarer notre nouveau
**node_exporter**

```bash
- job_name: 'nodes'
  static_configs:
    -targets: ['<ip_du_node_exporter>:9090']
```

On relance notre service `prometheus` et à partir de maintenant, les donées du
**node_exporter** sont exploitables depuis le serveur principal... On peut ainsi
déclarer plusieurs serveurs regroupés par node en fonction de notre parc...

## Interface Web

À partir de maintenant, si tout s'est bien passé l'interface web est accessible
sur `http://[ip_hostname]:9090`.
