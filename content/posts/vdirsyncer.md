---
title: "Vdirsyncer"
date: 2019-09-16T11:43:23+02:00
draft: false
---

Dans la recherche de projet auquel contribuer pour le projet tuteuré, il y a pour l'instant comme idée
de contribuer à [khal](https://github.com/pimutils/khal), un gestionnaire d'agenda en ligne de commande
écrit en python est très sympathique à utiliser!

Parmi les fonctionnalités qu'offre **khal**, il y a la synchronisation des calendriers avec des serveurs
CalDAV! Ô joie! C'est là qu'intervient [vdirsyncer](https://github.com/pimutils/vdirsyncer), qui est
chargé d'aller se synchronyser avec les serveurs CalDAV distants (et CardDAV aussi d'ailleurs).
Il permet même de faire la synchronisation entre deux serveurs, et par uniquement localement!

Voyons comment installer et configurer **vdirsyncer**...

<div class="alert alert-info">Ce billet utilise le contexte mis en place sur le billet
qui parle de [radicale](/posts/radicale) qui est une solution légère d'auto-hébergment d'agendas
CalDAV et de contacts CardDAV.</div>

## Installation

Vdirsyncer s'installe côté client, puisque c'est lui qui se charge d'aller récupérer les agendas et
contacts hébergés sur un serveur distant...

Sur la [documentation](https://vdirsyncer.pimutils.org/en/stable/installation.html), plusieurs méthodes
d'installation sont indiquées. J'ai pris le parti d'utiliser pip et les virtualenv... En admettant
que vous ayiez ces deux outils installés sur votre environnement.

```bash
virtualenv ~/.virenv/vdirsyncer
~/.virenv/vdirsyncer/bin/pip install vdirsyncer
```

Et si vous voulez éviter de devoir vous rendre à chaque fois dans votre virtualenv pour exécuter vdirsyncer,
il est possible de rajouter un alias dans votre ```.bashrc```. Comme ça il suffira de rentrer ```vdirsyncer```
dans votre terminal pour y accéder directement:

```bash
# ~/.bashrc
alias vdirsyncer="~/.virenv/vdirsyncer/bin/vdirsyncer"
```

## Configuration

Le [config.example](https://github.com/pimutils/vdirsyncer/blob/master/config.example) du dépôt du projet
est très bien fait. Cependant vous aurez à le créer vous même. J'ai choisi pour ça le ```XDG_CONFIG_HOME```.
À savoir:

```bash
mkdir ~/.config/vdirsyncer
touch ~/.config/vdirsyncer/config
```

Jetons un œil au fichier de configuration...

```bash
[general]
status_path = "~/.local/share/vdirsyncer"

# CARDAV

# Un block [pair <nom>] désigne deux dépôts à synchronier.
# Ici, on veut synchroniser nos contacts entre notre dossier
# local et notre carnet d'adresse en ligne.
[pair ximun_contacts]
a = "ximun_contacts_local"
b = "ximun_contacts_remote"

# Synchronise toutes les collections trouvées. Il faut lancer
# `vdirsyncer discover` si de nouveaux calendriers ou carnets
# ont été ajouté
collections = ["from a", "from b"]

# Va afficher ce qui concerne
metadata = ["displayname"]

# Il se peut qu'un conflit apparaisse entre deux dépôts.
# On doit choisir ce qu'il se passe dans ce cas:
# - `null` - annule la synchronisation (par défaut)
# - `a wins` ou `b wins` - déclare quel dépôt sera le plus à jour
# - ["command", "vimdiff"] - lance une commande particulière
conflict_resolution = null

# Maintenant, on doit entrer les infos concernant chaque dépôt
# précédemment déclaré.
# - `type` - "filesystem", "carddav" ou "caldav"
# - `path` - le chemin pour y accéder/écrire les synchronisations
# - `fileext` - l'extension à donner aux fichiers.
[storage ximun_contacts_local]
type = "filesystem"
path = "~/.local/share/contacts/"
fileext = ".vcf"

# Pour les dépôts distants, il faut s'y connecter, donc un login
# et un mot de passe sont nécessaires, ainsi que l'url du dépôt
# en question.
[storage ximun_contacts_remote]
type = "carddav"
url = "http://192.168.122.137"
username = "ximun"
password.fetch = ["prompt", "Mot de passe CardDAV"]

# Et on refait à peu prêt la même chose pourl les calendriers.
# C'est ce qu'on utilisera après avec khal...
[pair ximun_calendar]
a = "ximun_calendar_local"
b = "ximun_calendar_remote"
collections = ["from a", "from b"]

[storage ximun_calendar_local]
type = "filesystem"
path = ".local/share/agendas"
fileext = ".ics"

[storage ximun_calendar_remote]
type = "caldav"
url = "http://192.168.122.137"
username = "ximun"
password.fetch = ["prompt", "Mot de pass CalDAV"]
```

### Mot de passe

<div class="alert alert-danger">Attention, des vrais choix sont à faire concernant le mot de passe...</div>

Il existe plusieurs façons de gérer les mots de passe pour se connecter aux dépôts distants.

<code>password = "motDePasseEnClair</code> dans ce cas le mot de passe est directement inscrit en clair dans le fichier de conf.
 
<code>password.fetch = ["command", "~/get-passwd.sh", "more", "args"]</code> pour gérer dans un script la récupération du mot de passe
 
<code>password.fetch = ["prompt", "Votre mot de passe..."]</code> affiche un prompt avec un message personnalisé pour entrer son mot de passe.

<code>password.fetch = ["command", "keyring", "get", "example.com", "foouser"]</code> utilise le paquet python [keyring](https://github.com/jaraco/keyring/) qui accède aux mot de passes du système d'exploitation.

## Synchronisation

Normalement, à partir de là tout est en place pour lancer la synchronisation des contacts. Alors on y va:

<div class="alert alert-info">Encore une fois, cette configuration est liée à l'hébergement sur la VM
détaillé sur le billet sur [radicale](/posts/radicale). Il faut l'adapter à votre propre serveur.</div>

```bash
# Lance la découverte des données:
$ vdirsyncer discover

# vdirsyncer propose de créer le fichier nommé avec un hash pour stocker ses données
# et les contacts/agendas... Autant accepter....
warning: No collection "[hash]" found for storage ximun_contacts_local.
Should vdirsyncer attempt to crete it? [y/N]: y
warning: No collection "[hash]" found for storage ximun_calendar_local.
Should vdirsyncer attempt to crete it? [y/N]: y
Saved for mes_contacts: collections ...

# C'est fait :)
# Maintenant on synchronise les données depuis le serveur!
$ vdirsyncer sync
Mot de passe CardDAV:
Syncing ximun_contacts/[hash]
Syncing ximun_calendar/[hash]
Copying (uploading) item [hash] to mes_contacts_local/[hash]
Copying (uploading) item [hash] to mes_contacts_local/[hash]
Copying (uploading) item [hash] to mes_contacts_local/[hash]
Copying (uploading) item [hash] to mes_contacts_local/[hash]
Copying (uploading) item [hash] to mes_contacts_local/[hash]
...
```
Normalement, les contacts et agendas ont bien été récupérés s'il y en a sur l'hébergement.
Pour s'en assurer, il suffit de lancer un ```ls -al ~/.local/share/contacts/[hash]/``` et
de voir qu'il y a bien un fichier par contact, la même chose sur ```~/.local/share/calendars/[hash]```
concernant les agendas.

Maintenant que **vdirsyncer** est bien synchronisé avec notre serveur CalDAV distant,
en avant pour manipuler nos agendas avec **khal**! :)
