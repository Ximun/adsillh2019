---
title: "Khal"
date: 2019-09-21T15:40:32+02:00
draft: false
---

[**khal**](https://lostpackets.de/khal) est le logiciel auquel nous voulons contribuer pour le projet
tuteuré. Il s'agit d'un logiciel écrit en python qui nous permet de gérer
des agendas.

Ce billet vient à la suite de ceux sur [**radicale**](/posts/radicale) et
[**vdirsyncer**](/posts/vdirsyncer), les outils qui nous permettent
respectivement d'héberger nos agendas CalDAV et de les synchroniser sur
une machine locale (ou entre deux serveurs CalDAV).

**khal** offre la possibilité d'utiliser **vdirsyncer** pour manipuler
nos agendas distants. C'est ce que nous allons voir ici.

## Installation

Étant donné que le but est de contribuer au logiciel, nous allons installer
**khal** à partir des [sources](https://github.com/pimutils/khal). Cependant
nous allons installer tout ceci dans environnement virtuel python **en mode
éditable**. Ce qui nous permettra de pouvoir tester notre code modifié
sans avoir à réinstaller tout le paquet à chaque fois.

```bash
# Création du virtualenv
virtualenv .virenv/khal

# On se place dedans
source .virenv/khal/bin/activate

# On se dans son dossier de développement et on clone le dépôt
git clone https://github.com/pimutils/khal.git
cd khal

# Et voici la commande pour installer khal en version éditable
pip install -e .
```
En regardant de plus près on comprend que la commande ```pip install -e```,
au lieu de créer un exécutable de khal, a créé un lien vers le dossier
de développement de khal.

```bash
$ ls -al .virenv/dev/khal/bin
```

Attention, j'utilise ArchLinux qui utilise l'interpréteur python3 par
défaut, ce qui n'est pas le cas pour d'autres distributions comme
Debian par exemple. Assurez vous de l'utiliser pour installer khal
car il nécessite (au moment de la rédaction de ce billet) une version
supérieure ou égale à python 3.4

Un ```khal --version``` servira à voir que l'installation s'est bien déroulée.
Normalement, étant donné que vous avez installé **khal** depuis les sources
vous devriez par défaut avoir une version de développement.

## Configuration

La configuration de votre **khal** se fait dans ```~/.config/khal/config```.
Pour entrer dans les détails, la [page officielle](https://khal.readthedocs.io/en/latest/configure.html)
est très bien documentée. Je vous montre la petite configuration que j'ai mise en
place.

La chose à noter est que **radicale** stocke ses calendriers en leur donnant un [hash]
gigantesque. Donc par défaut **khal** va leur donner ce [hash] en nom de calendrier.
Il ne faut donc pas oublier de nommer le calendrier dans la configuration de khal
afin d'éviter de devoir taper le [hash] dans la commande ```khal``` à chaque qu'on
veut faire un traitement sur le calendrier.

```bash
[calendars]

	[[Personnel]]
	# Attention, [hash] dépend du hash de votre serveur radicale
	# (si vous  utilisez radicale), récupéré par vdirsyncer et
	# visible dans ~/.local/share/calendars/
	path = ~/.local/share/calendars/[hash]/
	color = dark green

[locale]
timeformat = %H:%M
dateformat = %d/%m/%Y
longdateformat = %d/%m/%Y
datetimeformat = %d/%m/%Y %H:%M
longdatetimeformat = %d/%m/%Y %H:%M
```
