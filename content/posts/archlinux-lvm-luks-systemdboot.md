---
title: "Archlinux Lvm Luks Systemdboot"
date: 2019-11-24T18:01:26+01:00
draft: true
---

# ArchLinux + LVM + LUKS + Systemd-boot

Ça fait un moment que je voulais me trouver une bonne routine pour installer une
ArchLinux chiffrée... C'est l'occasion également pour moi de découvrir LVM, dont
un seul volume entièrement chiffré me permettra beaucoup de souplesse à
l'intérieur au niveau du "partitionnement", ainsi que Systemd-boot (autrefois gummiboot)
pour tester une alternative à GRUB simplissime... (attention toutefois,
Systemd-boot est seulement destiné aux UEFI)

## Préparation

L'idée ici est donc d'avoir un disque dur qui a une première partition `/boot`
(500Mo pour être tranquille), ainsi qu'une seule deuxième partition de toute
la taille restante du disque, entièrement chiffrée, qui viendra accueillir un
grand et unique volume physique LVM dans lequel on va installer tout le système
(le `/`, le `/home` et la ``swap``)

{{< mermaid >}}
graph TB
  subgraph /sda1
  /boot
  end
  subgraph /sda2
    subgraph conteneur LVM chiffré
      /
      /home
      swap
    end
  end
{{< /mermaid >}}

## Partitionnement

On boot donc sur notre nouvelle clé `archlinux`, et on va pouvoir s'attaquer au
partitionnement...

Tout d'abord, on passe le clavier en français (azerty)

```bash
loadkeys fr
```

Pour cet article, on va admettre qu'on est sur de l'UEFI. Si vous utilisez une
machine en mode BIOS, il faudra utiliser un label type `dos`.

En admettant que notre disque dur est identifié en `sda` (faites en fonction si
c'est autrement), on va gérer les partitions ainsi avec `fdisk` (évidemment,
vous perdrez vos données s'il y en a... Veillez à bien avoir sauvegardé vos
données au préalable si besoin)

```bash
fdisk /dev/sda

# Tout d'abord, on crée une nouvelle table de partition
# GPT (DOS si vous êtes en mode BIOS)
g

# On va créer la partition de boot
# On déclare une nouvelle partition
n

# Choisir partition primaire
p

# Numéro de la partition
# laisser valeur par défaut (1)

# Premier secteur
# laisser valeur par défaut

# Dernier secteur
# on choisit une taille de 512M
+512M

# On répète l'expérience avec notre deuxième partition

# Création d'une nouvelle partition
n

# Numéro de partition (2 par défaut)
# Laisser valeur par défaut

# Premier secteur
# Laisser valeur par défaut

# Dernier secteur
# Laisser valeur par défaut pour occuper toute la place
# restante sur le disque dur
```

Nos partitions sont déclarée, il reste à changer leurs types. Toujours dans
l'utilitaire `fdisk /dev/sda`:

```bash
# Changer un type de partition
t

# Choisir la première partition
1

# Entrer le type de partition désiré (EFI System)
1

# Répéter l'opération pour la deuxième partition
t

# Numéro de la partition
2

# Entrer le type de partition désiré (Linux LVM)
31
```

A priori, la table de partition devrait être correcte. On vérifie en entrant la
commande `p` dans `fdisk` et on devrait avoir quelquechose qui ressemble à ça:

```bash
Disk /dev/sda: 465.78 Gib
Disk model: <dépend de votre disque dur>
Units: <idem>
Sector size: <idem>
I/O size: <idem>
Disklabel: gpt (si efi, dos pour bios)
Disk identfier: XXXXXXX-XXXX-XXXX...

Device      Start   End   Sectors   Size    Type
/dev/sda1   ...     ...       ...   512M    EFI System
/dev/sda2   ...     ...       ...    ...    Linux LVM
```

Lorsqu'on est bon, on peut écrire la table de partition sur le disque et quitter
l'utilitaire `fdisk`.

```bash
w
```

## Création du conteneur chiffré

On va maintenant chiffrer intégralement notre deuxième partition qui accueillera
nos volumes logiques. Pour ceci, on va utiliser le standard LUKS implémenté dans
`dm-crypt`, via la commande `cryptsetup`

```bash
cryptsetup --verify-passphrase --cipher aes-xts-plain64 --key-size 512 --iter-time 5000 --hash sha512 luksFormat /dev/sda2
```

### Drapeaux:

- `--veriphy-passphrase`: Demander une vérification de la passphrase (il vaut
mieux être sûr étant donné que si vous perdez cette passphrase, **impossible de
récupérer les données sur le disque !!!**
- `--cipher aes-xts-plain64`: Choix du protocle de chiffrement
- `--key-size 512`: Taille de la clé de chiffrement
- `--iter-time 5000`: temps de génération de la clé, relatif au standard PBKDF
- `--hash sha512`: fonction utilisée pour hasher la passphrase
- `luksFormat`: Utilise LUKS
- `/dev/sda2`: Partition à chiffrer 

À noter que si vous avez des remarques à me faire quant aux choix fait pour
cette commande, n'hésitez pas! :)

Écrivez `YES` pour confirmer, et tapez deux fois votre passphrase. Plus elle
sera longue, et plus le chiffrement sera difficile à casser. **RETENEZ-LA
BIEN!!!**

Dorénavant, la partition est **chiffrée**. Il va falloir l'ouvrir à chaque début
de session, et maintenant aussi afin de travailler dessus et créer notre
conteneur et nos volumes LVM. On passe toujours par `cryptsetup`

```bash
cryptsetup open /dev/sda2 system
```

A noter que vous pouvez utiliser le nom que vous voulez à la place de *system*.
C'est le nom qui identifiera votre partition une fois dédchifrée. Entrez votre
passphrase pour ouvrir la partition. Elle est désormais accessible sur
`/dev/mapper/system`.

## Remplissage du conteneur avec des zéros (optionnel)

C'est l'étape paranoïaque :) On rempli toute la partition avec des zéros afin
d'empêcher de connaître la taille des données écrites à l'intérieur. Ce n'est
pas une obligation, surtout que ça peut être TRÈS long...

```bash
dd if=/dev/zero of=/dev/mapper/system bs=16M status=progress
```

## Conteneur et volumes LVM

On va maintenant déclarer nos conteneurs LVM à l'intérieur de notre partition
chiffrée. Il y a plusieurs avantages à utiliser LVM:

- On ne chiffre qu'une seule partition pour chiffrer tout le système (sinon il
aurait fallu chiffrer chacune des partitions)
- Les volumes LVM sont très souples et s'il faut les redimensionner dans le
passé, ce sera extrêmement simple.

La [page wiki d'Ubuntu](https://doc.ubuntu-fr.org/lvm) est plutôt bien faite
pour comprendre le principe.

### Création du volume physique principal

```bash
pvcreate /dev/mapper/system
```

### Création du groupe de volumes

```bash
vgcreate sysgroup /dev/mapper/system
```

### Création des volumes

```bash
lvcreate -L sysgroup 2G -n swap
lvcreate -L sysgroup 50G -n root
lvcreate -l 100%FREE sysgroup -n home
```

On a donc créé un volume physique LVM (system) à l'intérieur duquel on a créé un
groupe de volume (sysgroup) à l'intérieur duquel on déclare trois volumes:

- Une **swap** de 2G
- Un volume **root** `/` de 50G
- Un volume **home** qui prend tout l'espace restant

Bien sûr vous pouvez adapter les tailles!

## Création des systèmes de fichiers

On va devoir à présent formater les volumes et la partition de boot dans les
systèmes de fichiers adéquats.

```bash
# Le boot EFI en FAT32 (du ext2 pour du BIOS)
mkfs.fat -F 32 /dev/sda

# Du ext4 pour le home et le root
mkfs.ext4 /dev/mapper/sysgroup-home
mkfs.ext4 /dev/mapper/sysgroup-root

# Et enfin la swap
mkswap /dev/mapper/sysgroup-swap
```

## Montage des partitions

Afin d'installer le système, on va devoir monter nos partitions maintenant
qu'elles sont formatées. On va les monter dans `/mnt`

```bash
mount /dev/mapper/sysgroup-root /mnt
mkdir /mnt/{home,boot}
mount /dev/mapper/sysgroup-home /mnt/home
swapon /dev/mapper/sysgroup-swap
mount /dev/sda1 /mnt/boot
```

## Installation du système de base ArchLinux

Toutes les partitions sont montées, on peut installer le système de base... Si
vous êtes en wifi, vous allez d'abord devoir configurer votre accès grâce à la
commande `wifi-menu`...

```bash
pacstrap base base-devel efivar linux linux-firmware /mnt
```

Une fois le système installé, on chroote dedans...

```bash
arch-chroot /mnt
```

On peut vérifier que l'on a bien accès aux variables efi via la commande `efivar
--list`. C'est indispensable pour booter grâce à `systemd-boot`. Si vous êtes en
BIOS, il faudra suivre le wiki d'ArchLinux pour installer GRUB ou un autre boot
manager qui gère le BIOS.

### Locales / Réglages

Une fois qu'on a chrooté dans le nouveau système, on édite les fichiers
suivants:

- `/etc/vconsole.conf`
  - `KEYMAP=fr`
- `/etc/locale.conf`
  - `LANG=fr_FR.UTF-8
- `/etc/hostname`
  - `<le nom de votre machine>
- `/etc/locale.gen` (décommenter les lignes suivantes)
  - `en_US.UTF-8`
  - `fr_FR.UTF-8`

On exporte les locales

```bash
locale-gen
```

### Heure

Réglage du fuseau horaire:

```bash
ln -sf /usr/share/zoneinfo/Europe/Paris /etc/localtime
```
On règle l'horloge matérielle RTC (Real Time Clock) sur l'heure universelle UTC
(Coordinated Universal Time)

```bash
# On règle le fuseau horaire
ln -sf /usr/share/zoneinfo/Europe/Paris /etc/localtime

# On règle l'horloge matérielle RTC (Real Time Clock) sur l'heure
# universelle UTC (Coordinated Universal Time)
hwclock --sysohc --utc

# On déclare la timezone dans le service
timedatectl set-timezone Europe/Paris
```

## Initialisation du démarrage

Il faut charger les modules adéquats dans le `/boot`, afin que ce dernier soit
capable de déchiffrer la partition chiffrée et démarrer le système. Pour ça, on
doit d'une part installer le module adéquat:

Mise en place des outils nécessaires au boot et au déchiffrement de la partition

```bash
pacman -S lvm2
```
et modification de la ligne `HOOKS` dans `/etc/mkinitcpio.conf`.

```bash
HOOKS=(base udev autodetect modconf block encrypt filesystems keymap keyboard lvm2 fsck)
```

Régénération du `initramfs`

```bash
mkinitcpio -p linux
```

### Installation du bootloader `systemd-boot`

```bash
bootctl --path=/boot/ install

# Entrée
# /boot/loader/entries/arch.conf

# /boot/loader/loader.conf
```
