---
title: "Grafana"
date: 2019-11-21T13:31:27+01:00
draft: true
---

# Grafana

Grafana va nous permettre de requêter, visualiser, et faire parler tout un tas
de données. On va particulièrement s'en servir afin de visualiser les données
récoltées par [Prometheus](/posts/prometheus). On va donc essayer d'installer
Grafana sur le serveur principal de **Prometheus**.

## Installation  Debian 10

En suivant la documentation officielle, en installant le dépôt officiel:

```bash
# Dépendances nécessaires à l'installation (notamment les outils pour ajouter
# des dépôts en ligne de commande, gpg pour récupérer la clé publique signée
# du dépôt et enfin apt-transport-https si nécessaire...
sudo apt-get install software-properties-common gnupg2 apt-transport-https

# Ajour du dépôt
sudo add-apt-repository "deb https://packages.grafana.com/oss/deb  stable main"

# Ajout de la clé publique au trousseau
wget -q -O - https://packages.grafana.com/gpg.key | sudo apt-key add -

# Installation de grafana
sudo apt-get update
sudo apt-get install grafana

# Gestion du daemon via systemd
sudo systemctl daemon-reload
sudo systemctl start grafana-server
sudo systemctl enable grafana-server
```

Si tout va bien, l'instance de Grafana est désormais accessible sur
`<ip_serveur_principal>:3000`. Les identifiants de base sont `admin/admin`.
Essayons de faire le lien avec notre instance de Prometheus...


