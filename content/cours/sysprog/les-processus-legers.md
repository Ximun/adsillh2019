---
title: "Les Processus Legers"
date: 2019-10-01T14:30:03+02:00
clsDate: ["2019-10-01", "2019-10-08"]
draft: draft
---

# Les processus légers

Un **processus légers** (ou thread, fil d'exécution,...) est similaire à un processus car tous les deux
représentent l'exécution d'un ensemble d'instructions du langage machine d'un processeur. Du point de
vue de l'utilisateur, ces exécutions semblent se dérouler en parallèle. Toutefois, là où chaque processus
possède sa propre mémoire virtuelle, **les threads d'un même processus se partagent sa mémoire virtuelle**.
Par contre, tous les threads possèdent leur propre pile d'exécution.

[Wikipédia](http://fr.wikipedia.org/wiki/Thread_(informatique))

## Points Comuns / Différences avec les processus

| Points communs | Différences |
|----------------|-------------|
| Ils permettent aux programmes de faire des tâches concurrentes (réception de données par le réseau pendant que l'interface graphique est à mise à jour) | Les processus sont exécutés dans un espace mémoire qui leur est propre. |
| Ils permettent d'exécuter des tâches en parallèle si l'architecture le permet (système multiprocesseurs ou multicœurs) | Les threads partagent le même espace mémoire |

## Avantages des threads

- **partage d'informations**: Cela demande plus de travail de faire communiquer
des processus normaux ensemble (Mise en place
d'[IPC](https://fr.wikipedia.org/wiki/Communication_inter-processus)).
- Créer et rendre un thread opérationnel par le noyau de l'ordre de 10 fois plus
rapide.
- Le partage d'informations entre threads existe de fait car ces derniers
partagent le même espace mémoire (variables globales et allouées sur le tas).

Les threads apparaissent lors d'un ```htop``` (en vert)

## Anatomie d'un processus et d'un thread en mémoire

{{< columns >}}

### <center>Processus</center>
![Anatomie Processus](/cours/sysprog/anatomie-processus.gif)

<--->

### <center>Thread</center>
![Anatomie Thread](/cours/sysprog/anatomie-thread.png)

{{< /columns >}}

## Informations partagées/non partagées par les threads

- Espace mémoire
- PID et PPID (ID de processus et de processus parent)
- La table des fichiers ouverts (fd)
- Les gestionnaires de signaux

<div class="alert alert-info">
À noter que les processus légers <strong>ne partagent pas</strong>:

<ul>
<li>Leur ID de processus léger (Thread ID)</li>
<li>Leur pile (stack) -> pas de partage des variables locales lors d'appels
de</li>
fonctions)
<li>errno</li>
<li>La mémoire locale de processes léger (Thread Local Storage TLS)</li>
</ul>
</div>

# L'API POSIX Thread (Pthreads)

L'API Pthreads est issue de POSIX.1c (1995) et intégrée dans SUSv3. Elle est
devenue l'API des processus légers standard sous UNIX.

Elle couvre:

- Les types de données
- La gestion des threads
- L'exclusion mutuelle (mutex)
- Les variables conditionnelles
- Les verrous
- etc.

```
man 7 pthreads
```

## Conventions de l'API

Convention habituelle:

- Succès: retourne 0 (ou un entier positif)
- Échec: retourne -1 et errno est positionnée

Convention de l'API Pthreads:

- Succès: retourne 0
- Échec: errno est positionnée

<div class="alert alert-info">
Faut-il comprendre qu'en cas d'échec, aucune valeur n'est retournée?
Faison un test... TODO
</div>

<div class="aler alert-info">
Je n'écrirai sur ce site que les exemples pour pyton. Regarder le 
[PDF](https://openics.org/teaching/adsillh/05_Les-processus-legers.pdf) pour les
exemples en C.
</div>

## Création d'un processus léger

```python 3
#!/usr/bin/python3

import os
from threading import Thread
import time

def tmain(arg):
	print("[T] %s" % (arg))
	time.sleep(3)

def main():
	thread = Thread(target=tmain, args=('Hello World!',))
	print(thread)

	try:
		"""démarre le thread"""
		thread.start()
	except Eception as e:
		print('Unable to create thread')
		sys.exit(os.EX_OSERR)
	
	try:
		"""attend la fin de l'execution du thread"""
		thread.join()
	except Exception as e:
		print('Unable to join thread')
		sys.exit(EX_OSERR)
	
	print("Thread finished")

if __name__=='__main__':
	main()
```

Exécution du script:

```bash
& ./hello-world.py
<Thread(Thread-1, initial)>
[T] Hello World!
Thread finished
```
On remarque la pause de 3 secondes indiquée dans la fonction exécutée dans le
processus léger ```tmain()```.

Explications du script:

- Le programme entre dans la fonction ```main()```
- Un thread est créé dans la variable via l'instanciation d'un objet Thread.
  - **target**: appel de la fonction à exécuter dans le thread
  - **args**: passe un 1-uplet contenant 'Hello World' en paramètre à la
fonction ```tmain()```
  - Via le print, on voit que la variable **thread** est rempli par l'ID de
processus léger (1)
- La fonction ```tmain()``` est exécutée, elle affiche le paramètre passé en
fonction
- En python, il n'y a pas besoin d'appeler ```pthread_exit()``` pour terminer le
processus léger. Il se termine à la fin de l'exécution de la fonction appelée.

## Création de plusieurs processus légers

```python
#!/usr/bin/python3

import os
import sys
from threading import Thread
import time

def tmain(arg):
  print("[T%d] Hello World!" % (arg))
  time(1)

def main():
  if len(sys.argv) != 2:
    print("Wrong arguments")
    sys.exit(os.EX_USAGE)
  
  count = int(sys.argv[1])
  
  threads = []
  
  for i in range(count):
    print("Creating thread n° %d" % (i+1))
    thread = Thread(target=tmain, args=(i+1,))
    
    try:
      thread.start()
    except Exception as e:
      print("Unable to create thread")
      break
    threads.append(thread)
  
  for t in threads:
    try:
      thread.join()
    except Exception as e:
      print("Unable to join thread")
      continue

if __name__ == "__main__":
  main()
```

# En plus

## Sections critiques

Une section critique est un fragment de code qui accèsde à une **ressource
partagée** et qui doit être exécutée de façon atomique au regard des autres
entités (threads) qui souhaitent y accéder.

Dans le cas des threads l'accès à une variable globale est une section critique.

```python
#!/usr/bin/python3

import os
from threading import Thread, Lock

LOOP_COUNT = 10000000
THREADS_COUNT = 2

glob = 0
lock = Lock()

def tmain():
  global glob
  for i in range(LOOP_COUNT):
    lock.acquire()
    loc = glob
    loc += 1
    glob = loc
    lock.release()

def main():
  threads = []
  
  for i in range(THREADS_COUNT):
    print("Creating thread n° %d" % (i+1))
    thread = Thread(target=tmain)
    try:
      thread.start()
    except Exception as e:
      print("Unable to create thread")
      break
    threads.append(thread)
  
  for t in threads:
    try:
      thread.join()
    except Exception as e:
      print("Unable to join thread")
      continue
  
  print("%d =? %d" % (glob, THREADS_COUNT * LOOP_COUNT))
  
if __name__ == "__main__":
  main()
```
Exécution du script:

```bash
Creating thread n° 1
Creating thread n° 2
20000000 ?= 20000000
```
Le script met un certain temps à s'éxécuter... Voyons ce qui se passe de plus
près...

- Créations des variables **LOOP_COUNT** et **THREADS_COUNT**
- Déclaration de la variable **glob = 0** (la variable globale qui est ici la
**section critique** du script)
- La déclaration de la variable **lock** dans laquelle on instancie la classe
**Lock()**
[infos](https://docs.python.org/3.7/library/threading.html#threading.Lock)
- Le programme entre dans la fonction **main()**
- Création de la liste **threads**
- Crée autant de threads que le nombre contenu dans la variable
**THREADS_COUNT** et les stocke dans la liste **threads**

Que se passe-t-il dans la fonction **tmain()** lancée dans chaque thread?

- Le pthread accède à **glob** globalement
- Le premier pthread à acquérir le lock bloque l'exécution des autres, puis:
  - incrémente la variable **glob**
  - libère le lock et rend l'exécution possible pour le prochain

- Pour terminer, le programme attend la fin d'exécution des threads, et on
vérifie que la valeur de toutes les incrémentations de **glob** correspond
bien au nombre de threads multiplié par le nombre de boucles.

La classe **threading.Lock()** permet de définir une **section critique**, à
laquelle les threads ne peuvent pas toucher en même temps. Si les threads se
mettaient à incrémenter **glob** en même temps chacun de leur côté, ils
récupèreraient parfois la même valeur de **glob** et penseraient l'incrémenter
indépendemment alors qu'ils lui donneraient exactement la même valeur. Voici ce
que donnerait l'exécution d'une telle fonction:

### Sans la classe ```threading.Lock()```

```python
#!/usr/bin/python3

import os
from threading import Thread

LOOP_COUNT = 10000000
THREADS_COUNT = 2

glob = 0

def tmain():
  global glob
  for i in range(LOOP_COUNT):
    loc = glob
    loc += 1
    glob = loc

def main():
  threads = []
  
  for i in range(THREADS_COUNT):
    print("Creating thread n° %d" % (i+1))
    thread = Thread(target=tmain)
    try:
      thread.start()
    except Exception as e:
      print("Unable to create thread")
      break
    threads.append(thread)
  
  for t in threads:
    try:
      thread.join()
    except Exception as e:
      print("Unable to join thread")
      continue
  
  print("%d =? %d" % (glob, THREADS_COUNT * LOOP_COUNT))
  
if __name__ == "__main__":
  main()
```
Exécution:

```bash
Creating thread n° 1
Creating thread n° 2
12838547 ?= 20000000
```
On voit bien qu'à la fin de l'exécution de tous les threads, la valeur finale de
```glob``` n'est pas celle attendue.

<div class="alert alert-danger">
Voir le cours pour l'utilisation des <b>Mutex</b> et des <b>variables
conditions</b> pour le langage C.
</div>
