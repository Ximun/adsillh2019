# Introduction à la Programmation Système - Appels Systèmes

Il s'agît de créer des programmes de **bas niveau** qui s'interfacent directement avec le **noyau** du système
d'exploitation et les bibliothèques du cœur du sytème.

Le **noyau** est la **couche logicielle** qui contrôle le matériel et fourni l'environement dans lequel les programmes peuvent être exécutés.

<center>![](/cours/sysprog/couches.gif)</center>

## Les normes

### La norme POSIX

Dans les années 70, chaque constructeur livrait sa propre version de UNIX propriétaire. Ils se sont mis
d'accord sur certaines conventions, la norme **POSIX**, définie par l'**IEEE** (Institue of Electrical and
Electronics Engineers) en 1988. POSIX devient presque un label pour ces systèmes d'exploitation.

POSIX spécifie:

- les interfaces utilisateurs et les interfaces logicielles
- la ligne de commande standard et l'interface de script ([Bourne shell](https://fr.wikipedia.org/wiki/Bourne_shell))
- les services I/O (entrées / sorties) de base (fichiers, terminaux, réseau)
- les attributs que doivent supporter les fichiers
- une interface de programmation standard

### Single UNIX Specification

Cependant, l'IEEE vend très cher la documentation POSIX, et ne permet pas sa publication sur Internet.
Certains se tournent donc vers une nouvelle norme **SUS** (Single UNIX Specification). Il s'agit d'un
ensemble de spécifications permettant de certifier un système d'exploitation comme étant un UNIX.

Cette fois-ci, c'est la certification qui a un coût très élevé...

### Linux Standard Base

Les distributions Linux se regroupent et créent donc la **LSB** (Linux Standard Base), sous la coupe du **FSG**
(Free Standard Group) qui a pour but de concevoir et standardiser la structure interne des systèmes d'exploitation
basés sur GNU/Linux.

## Arborescence d'un système UNIX

Parmis les normes associées aux système UNIX figure tout ce qui a trait à l'arborescence des fichiers.
À partir de la racine **/** , on trouve plusieurs dossiers importants:

| Dossier    | Rôle |
|:-----------|:-----|
| **/dev/**  | "devices", périphériques physiques ou virtuels comme les tty, disques durs, etc. |
| **/sys/**  | sert à récupérer des infos de la part du noyau en temps réel (consommation de mémoire, batterie pour les portables,...) |
| **/proc/** | un tas d'infos sur les processus en cours |
| **/usr/**  | librairies et programmes installés par le ou les utilisateurs du système |
| **/var/**  | caches, logs, ... |
| **/sbin/** | binaires destinés au super-utilisateur (root) |
| **/run/**  | sockets, services,... en cours de fonctionnement |

## Architecture en couches

<center>![architecture en couches](/cours/sysprog/couches.jpg)</center>


Chaque couche repose sur la couche inférieure, même s'il est possible de bypass une ou plusieurs
couches dans certains cas particuliers (c'est le cas par exemple pour les machines virtuelles:
elles sont exécutées sur la couche Application mais elles utilisent des directement des
ressources Machine/Assembleur).

Pour intéragir avec le noyau, on ne le fait jamais directement mais on intéragit avec la couche
des **appels systèmes**.

Une librairie très connue fournit l'API nécessaire pour effectuer ces appels systèmes:
c'est la **Librairie Standard C (libc)**.

## Descripteurs de fichiers

Lorsq'un programme doit écrire dans un fichier par exemple, il passe par le noyau. Le
noyau identifie ce fichier par un **file descriptor**, qui est un nombre entier.

Le file descriptor est contenu dans une **table** qui montre les adresses (file descriptor) de tous les fichiers physiquement enregistres sur le disque dur, *ouverts* a un instant t dans des processus.

C'est le noyau qui se charge de gerer cette table.

# Appels systèmes

Voici ce qu'il se passe grossièrement lorsqu'un programme **hello_world.c** est compilé
puis exécuté:

```c
printf("Hello World")
```
{{< katex >}}
USER
\left\{
	\begin{array}{l}
		printf(...) \\
		\quad \downarrow \\
		[libc] \rightarrow printf \rightarrow write \rightarrow "write\_kernel()" \\
		write\_kernel() \\
		\{ \\
		\quad \fbox{13} \fbox{ARGS} \leftarrow \footnotesize prépare\ une\ zone\ mémoire\ avec\ ces\ instructions \\
		\quad trap(); \rightarrow \footnotesize {bascule\ le\ processeur\ en\ mode\ noyau} \\
		\} \\
	\end{array}
\right. \\
KERNEL
\left\{
	\begin{array}{l}
		system\_call() \rightarrow \footnotesize{côté\ noyau} \\
		\footnotesize{\rightarrow \ va\ lire\ la\ zone\ mémoire\ (ARGS)} \\
		\footnotesize{\rightarrow \ exécute\ le\ "write()"} \\
		\footnotesize{\rightarrow \ récupère\ la\ valeur\ écrite\ par\ le\ noyau} \\
		\footnotesize{\rightarrow \ La\ renvoie\ dans\ la\ zone\ mémoire\ de\ retour}
	\end{array}
\right.
{{< /katex >}}

à suivre...
