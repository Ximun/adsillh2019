---
title: "Les Ipc"
date: 2019-10-22T16:13:08+02:00
draft: true
---

# Les Communications Inter Processus

## Mémoire partagée

<center>
{{< mermaid >}}

graph BT
  subgraph Processus1
    mem1["Mémoire: Shared Libraries"]
  end
  subgraph Processus2
    mem2["Mémoire: Shared Libraries"]
  end
  subgraph kernel
    shmem["Zone de mémoire martagée"]
  end
 
  shmem --Projection--> mem1
  shmem --Projection--> mem2

{{< /mermaid >}}
</center>

Les **IPC** (Inter Processus Communication) recouvrent trois mécanismes de
communicatio inter-processus:

- Les files de messages (**message queues**)
- La mémoire partagée (**shared memory**)
- Les sémaphores (**sempahores**)

Il existe sous UNIX deux APIs d'IPC:

{{< columns >}}

**System V IPC** (1983):

- Plus compliquée à utiliser
- Non fondée sur le système de **descripteurs de fichiers**

<--->

**IPC POXIX** (2000):

- Également connue sous le nom de SuSv4
- API moderne
- Sous Linux, nécessite noyau >= 2.6.10

{{< /columns >}}

## Files de messages / Message queues

Il s'agit d'un mécanisme qui sert à **envoyer ou recveoir des données entre
processus**. Les données ne sont pas envoyées à destination d'un processus en
particulier (?). Ce mécanimse est **proche des tubes**, à la différence que les
données **ne sont pas lues comme un flux d'octets**. On écrit ou lit un **message
complet**.

### Envoi de messages dans une file

```c
#include <fcntl.h>
#include <mqueue.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main (int argc, char **argv) {
  mqd_t mq;
  int priority;
  
  if (argc != 4) {
    fprintf(stderr, "Usage: %s queue priority message\n", argv[0]):;
    exit(EXIT_FAILURE);
  }
  if (sscanf(argv[2], "%d", &priority) != 1) {
    fprintf(stderr, "Invalid priority: %s\n", argv[2]);
    exit(EXIT_FAILURE);
  }
  if ((mq = mq_open(argv[1], O_WRONLY | O_CREAT, 0644, NULL)) == (mqd_t) -1)
    perror("Opening message queue failed");
    exit(EXIT_FAILURE);
  }
  if (mq_send(mq, argv[3], strlen(argv[3]), priority) == -1) {
    perror("Unable to send message to queue");
    exit(EXIT_FAILURE);
  }
  if (mq_close(mq) == -1) {
    perror("Unable to close queue");
    exit(EXIT_FAILURE);
  }
  return EXIT_SUCCESS;
}
```


