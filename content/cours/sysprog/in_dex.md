---
title: "In_dex"
date: 2019-09-16T17:51:17+02:00
draft: false
---

| Terme               | Définition              |
|---------------------|-------------------------|
| Programme | Un fichier qui s'exécute |
| Application | Écosystème complet qui comprend des programmes, bibliothèques, images, ... |
| Processus | Instance d'un programme en cours d'exécution dans la mémoire |
| [MMU](https://fr.wikipedia.org/wiki/Unité_de_gestion_mémoire) | Élémént de la carte mère qui s'interface entre le processeur et la mémoire, pour organiser les processus comme non fragmentés |
