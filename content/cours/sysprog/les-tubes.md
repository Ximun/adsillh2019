---
title: "Les Tubes"
date: 2019-10-21T08:03:18+02:00
draft: false
---

# Les tubes

Les **tubes** (ou pipes) sont un mécanisme de communication entre processus
(IPC: InterProcess Communication). Leur utilisation dans un shell est bien
connue sous UNIX (sous forme du caractère ```|```):

```bash
ps faux | cut -d ' ' -f1 | sort -u
```

## Caractéristiques des tubes

- Les tubes sont un mécanisme de transfert de données sous forme de flux entre
processus. Le flux écrit d'un côté du tube **peut être lu de l'autre**.
- Les tubes sont créés directement par le noyau.
- Une fois créés, les tubes sont manipulables par des descripteurs de fichiers.
- Les tubes ne sont accesibles que les processus qui y sont associés.
- Les tubes persistent **le temps de la vie du processus**, ils disparaissent à
la terminaison de ce dernier.
- Les tubes sont portables et disponibles sur tous les UNIX connus.

## Appel de ```pipe()```

La création de tube passe par un appel système au noyau:

```c
#include <unistd.h>

int pipe(int pipefd[2]);
```

En cas de succès, renvoi **0**. En cas d'échec, renvoi **-1** et positionne *errno*
en conséquence.

- **pipefd** est un tableau de descripteurs de fichiers qui sera rempli par le
noyau avant le retour de l'appel.
- **pipefd[0]** est ouvert en lecture.
- **pipefd[1]** est ouvert en écriture.
- Les tubes sont un canal de communication **half-duplex** (1 seul sens de
communication)

<div class="alert alert-info">
Chaque lecture ou écriture dans le tube fait transiter les données de l'espace
utilisateur vers l'espace noyau (et inversement).
</div>

## Mode half-duplex

Implémentons un processus half-duplex... Voici le design pattern:

- **pipe(fds)**: Création du pipe
- **fork()**: Fork du processus
- Processus père: **close(fds[0])** ferme l'accès en lecture du pipe.
- Processus fils: **close(fds[1])** ferme l'accès en écriture du pipe.
- Processus père écrit des données au fils: **write(fds[1])**.
- Processus fils lit des données du père: **read(fds[0])**.

<center>
{{< mermaid >}}
graph TB
  subgraph child
  cfds0["fds[0]"]
  cfds1["fds[1]"]
  end
 
  subgraph parent
  pfds0["fds[0]"]
  pfds1["fds[1]"]
  end
 
  subgraph kernel
  pipe
  end
 
  cfds1 -.->|CLOSED|pipe
  pipe -.->|CLOSED|pfds0
  pipe ==read==> cfds0
  pfds1 ==write==> pipe
{{< /mermaid >}}
</center>

```c
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#define BUFMAX 256

int main () {
  char *buffer[BUFMAX];
  pid_t pid;
  int n, fds[2];
  
  if (pipe(fds) == -1) {
    perror("Unable to create pipe");
  }
  
  pid = fork();
  if (pid == -1) {
    perror("Unable to fork");
  }
  else if (pid > 0) { /* parent */
    if (close(fds[0] == -1) {
      perror("unable to close pipe from parent");
    }
    write(fds[1], "I am your father\n", 17);
  }
  else { /* child */
    if (close(fds[1] == -1) {
      perror("Unable to close pipe from child");
    }
    n = read(fds[0], buffer, BUFMAX);
    write(STDOUT_FILENO, buffer, n);
  }
  exit(EXIT_SUCESS);
}
```
Exécution du programme

```bash
$ gcc -Wall pipe_half-duplex.c -o pipe_half-duplex
$ ./pipe_half-duplex
I am your father
```

## Mode full-duplex

Pour faire des tubes full-duplex, il suffit de 2 tubes utilisés dans des
directions opposées. Design pattern:

- **pipe(p2c)**
- **pipe(c2p)**
- Processus père: **close(p2c[0])** ; **close(c2p[1])**
- Processus fils: **close(p2c[1])** ; **close(c2p[0])**
- Processus père écrit des données au fils: **write(p2c[1],...)**
- Processus fils qui écrit des données au père: **read(c2p[1],...)**

<center>
{{< mermaid >}}

graph TD
  subgraph kernel
  pipe2["pipe"]
  pipe1["pipe"]
  end
  subgraph parent
  pfds0["fds[0]"]
  pfds1["fds[1]"]
  end
 
  subgraph child
  cfds0["fds[0]"]
  cfds1["fds[1]"]
  end

 
  pipe1-.->pfds0
  cfds1-.->pipe1
  pipe2-.->cfds0
  pfds1-.->pipe2
  pfds1==>pipe1
  pipe1==>cfds0
  cfds1==>pipe2
  pipe2==>pfds0

{{< /mermaid >}}
</center>

## Cas particuliers

- En général, on ferme les extrémités du tube avant de l'utiliser.
- Une lecture ```read()``` sur une extrémité du tube déjà fermée retourne 0.
- Une écriture ```write()``` sur une extrémité du tube déjà fermée
retourne -1 avec ```errno``` positionné à **EPIPE**. De plus, le signal
**SIGPIPE** est envoyé au processus qui essaie d'écrire.

## Les filtres UNIX

Les **filtres** UNIX sont des programmes qui lisent leurs données en entrée
depuis l'entrée standard **stdin** et écrivent leurs données en sorties sur la
sortie standard **stdout**.

Parmis les filtres le splus utlisés il y a *cat*, *cut*, *grap*, *sed*, *sort*,
*uniq*, *head*, *tail*, *wc*,...

## Les filtres et les tubes

On peut imaginer un programme pour lequel on souhaite avoir une pagination. Dans
l'idéal on utlisera **less** (ou **more**) plutôt que d'en écrire un... Voici le
design pattern:

- **pipe()**
- **fork()**
- Le parent produit les données.
- Le fils pagine les données du père.
- Le fils duplique la sortie en lecture du tube sur son entrée **stdin**.
- Le fils exécute le programme de pagination (qui lira les données depuis
**stdin**.
- Le père écrit ses données dans le pipe.

### Duplication de descripteur de fichiers

**dup2()** transforme **newfd** en une copie de **oldfd**.

- Si **oldfd** n'est pas un descripteur de fichier valable, alors l'appel échoue
et **newfd** n'est pas fermé.
- Si **oldfd** est un dfescripteur de fichier valable et **newfd** a la même
valeur que **oldfd**, alors **dup2()** ne fait rien et renvoie **newfd**.

```bash
man dup2
```

```python
#!/usr/bin/python3

import os, sys

pager = 'less'

def main():
	
	# création du pipe
	pipein, pipeout = os.pipe()

	# Fork du processus
	try:
		pid = os.fork()
	except OSError as e:
		print("Unable to fork")
		os.exit(os.EX_FAIL)
	
	if pid > 0: # parent
		# Fermeture du fd en lecture
		try:
			os.close(pipein)
		except Exception as e:
			print("Unable to close pipe from parent")

		for i in range(1000):
			try:
				os.write(pipeout, bytes(str(i) + '\n', 'ascii'))
			except Exception as e:
				print("Unable to write into the pipe")

		# Ferme le fd
		os.close(pipeout)
		os.wait()
	
	else: # child
		# Ferme le fd en écriture
		try:
			os.close(pipeout)
		except Exception as e:
			print("Unable to close pipe from child")

		# Duplique le fd en lecture (sortie du pipe)
		# Sur l'entrée standar du processus fils
		try:
			os.dup2(pipein, sys.stdin.fileno())
		except Exception as e:
			print("Unable to duplicate stdin file descriptor")

		# Ferme le fd en écriture
		os.close(pipein)

		os.execlp(pager, pager)

if __name__ == '__main__':
	main()
```

<div class="alert alert-danger">
Voir le PDF du cours pour le même programme en C
</div>
