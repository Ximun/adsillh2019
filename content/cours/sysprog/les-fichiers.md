---
titile: "Les Fichiers"
date: 2019-09-17T17:00:00+02:00
draft: false
---

# Fichiers 

Dans la philosophie d’UNIX, **tout est fichier**.

Les entrees/sorties (**input/output** ou I/O), sont symbolisées par les
manipulations de fichiers.

Autrement dit, les entrées et sorties des programmes que l'on exécute sont
representées par des fichers dans lesquels on écrit ou on lit.

En effet, UNIX traite tout de la meme manière, puisque tout est fichier...

La manipulation se fait au travers de **descripteurs de fichiers** (file
descriptors = **fd**) et d’**appels systèmes**.

## Descripteurs de fichiers

<div class="alert alert-info">Un <b>descripteur de fichier</b> est un entier
positif affecté par le noyau pour référencer un fichier utilisé par un
programme en cours d'exécution.</div>

[voir intro](/cours/sysprog/introduction)

Au demarrage d'un programme donné, 3 descripteurs de fichiers sont déjà
ouverts et prêts à être utilisés : le fd 0 (standard input: stdin :
entree standard), le fd 1 (standard output : stdout : sortie standard: )
et le fd 2 (standard error output : stderr : sortie erreur).

| Descripteur | utilisation | Nom POSIX | Nom Python |
|-------------|-------------|-----------|------------|
| 0 | Entrée Standard | STDIN_FILENO | sys.stdin.fileno() |
| 1 | Sortie Standard | STDOUT_FILENO | sys.stdout.fileno() |
| 2 | Sortie Erreur | STDIERR_FILENO | sys.stderr.fileno() |

Les fichiers qui seront ouverts par la suite lors de l'execution du programme
prendront donc un numero de file descriptor superieur a 2.

Commande pour decouvrir les fils descriptors lies a un processus donne.

*Example: On cherche les file descriptors contenus dans la table des fd du
premier processus bash en cours d'execution. Etapes :*

1.
```
ps -fux | grep bash
Resultat PID:  999
```
 
2. 
```
ls -l /proc/999/fd
lrwx------ 1 habsinn habsinn 64 nov.   9 10:38 **0** -> /dev/pts/0
lrwx------ 1 habsinn habsinn 64 nov.   9 10:38 **1** -> /dev/pts/0
lrwx------ 1 habsinn habsinn 64 nov.   9 10:38 **2** -> /dev/pts/0
lrwx------ 1 habsinn habsinn 64 nov.   9 14:57 **255** -> /dev/pts/0
```

## Appels Système liés aux fichiers

### open()

`open()` est un **appel système** qui ouvre le fichier identifié par
*pathname* et retourne un **fd**. En fonction de l'argument *flags*, le
fichier peut:

- être ouvert en lecture (flag : O_RDONLY en C ou os.O_RDONLY en python3)
- être ouvert en écriture (flag: O_WRONLY en C ou os.O_WRONLY en python3)
- les deux (flag: O_RDWR en C ou os.O_RDWR en python3)

L'argument **mode** indique les permissions à placer sur le fichier en cas
de création (masquage de bits)

```c
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int open(const char *pathname, int flags);
int open(const char *pathname, int flags, mode_t mode);
```

`man 2 open` pour la liste des *mode*

Exemple python:

```python
import os, stat
f = os.open("test.txt",
    os.O_WRONLY | os.O_CREAT | os.APPEND,
        stat.S_IRSUR | stat.S_IWUSR | stat.S_IRGRP)
f = open("test.txt", "a", 0o640)
```

Voilà deux manières de réaliser la même action: soit en passant par
`os.open()` soit par `open()`:

- Ouvre le fichier `test.txt`
- Ouverture en écriture seule
- L'écriture se fera en fin de fichier
- Seul le propriétaire peut lire et écrire le fichier, le groupe peut
seulement le lire

<div class="alert alert-info">À noter tout de même que os.open() renvoie
uniquement un <b>fd</b> (donc un numéro) alors open renvoie un object de
type <b>TextIOWrapper</b></div>

### read() et write()

```c
# include <unistd.h>
# include <stdlib.h>

#define BUFFMAX 32 
/*
variable constante (on ne peut pas changer la valeur dans toute l'execution
du programme)
*/

int main (int argc, char *argv) {
	char buffer[BUFFMAX]; 
	/*
	on declare un tableau de longueur BUFFMAX (soit 32 cases dans le
	tableau). Chaque element du tableau doit etre un caractere. en effet,
	il n'ya pas de notion de string dans C. On utilise alors des tableaux
	de caracteres.
	*/
	int count; 
	/*
	definition d'une variable count qui sera un nombre entier.
	*/
	
	count = read(STDIN_FILENO, buffer, BUFFMAX); 
	/*
	Sur le FD 0 (entree standard): la fonction read renvoie ici le nombre
	d'octets qu'elle a lu dans le buffer,cad ce que contient le tableau
	buffer (jusqu'a la position numero 31, si les caracteres ne sont
	pas vides). 31 et pas 32, car le tableau commence par la position 0
	(cf fonctionnement des tableaux et des liste en programmation)
	*/
	if (count == -1){ 
		write(STDERR_FILENO, "Unable to read stdin\n",21);
		/*
		En cas d'erreur sur le read --> Sur le FD 2 (sortie erreur):
		renvoie le nombre d'octets qu'il a ecrit,cad 21octets
		exit(EXIT_FAILURE);
		*/
	}
	write(STDOUT_FILENO,"Input data was: ", 16);
	/*
	Sur le FD 1 (sortie standard): renvoie le nombre d'octets qu'il a
	ecrit,cad 16octets
	*/
	write(STDOUT_FILENO, buffer, count);
	/*
	Sur le FD 1 (sortie standard): il renvoie le resultat de count,
	cad le nombre de caracteres lus par la fonction read
	*/
	
	exit(EXIT_SUCCESS);
}
```

Exemple en python:

```python
#!/usr/bin/python3

import os, sys

def main():
  #buf = sys.stdin.read() # Ctrl-D for EOF
  try:
    buf = input()
  except EOFError:
    buf = ''
  except Exception as e:
    sys.stderr.write("Unable to read stdin: %s", e)
    sys.exit(os.EX_DATAERR)

sys.stdout.write("Input data was: %s" %(buf))
sys.exit(os.EX_OK)

main()
```

### close()
close(fd)

On indique le numero de file descriptor, et cela ferme le fichier correspondant
dans le processus du programme.

## Déplacement dans un fichier

Pour programme **en cours d'exécution**, le noyau maintient une table des
fichiers ouverts (contenant entre autre le **fd**) et la position du curseur
(ou **file offset**).

Il est la **position** à partir de laquelle aura lieu la prochaine leteure
/ écriture. Cete position est exprimée en **octets** depuis le début
du fichier.

À l'ouverture du fichier le curseur est positionné au début du fichier
et chaque lecture ou écriture dans le fichier déplace le curseur du nombre
d'octeets lus ou écrits.

### lseek()

```c
#include <unistd.h>

off_t lseek(int fd, off_t offset, int whence);
/* renvoie le nouvel emplacement en cas de réussite ou -1 en cas d'échec
auquel avec errno positionné. /*
```

| Position | Utilisation |
|----------|-------------|
| SEEK_SET | Le curseur est placé à offset octets depuis le début du fichier |
| SEEK_CUR | Le curseur de lecture/écriture est avancé de offset octets depuissa position actuelle |
| SEEK_END | Le curseur est placé à la fin du fichier + offset octets |

Si *whence* (d'où?) vaut SEEK_CUR ou SEEK_END, *offset* peut être négatif.

#### Exemples...

```python
import os, io

# Ouverture du fichier
f = open("file.txt", "w+b")
# Positionnement au début du fichier
f.seek(0, io.SEEK_SET)
# à la fin du ficiher
f.seek(0, io.SEEK_END)
# avant le dernier octet du fichier /!\
f.seek(-1, io.SEEK_END)
# 10 octets avant la position actuelle
f.seek(-10, io.SEEK_CUR)
# 10000 octets après la fin du fichier
f.seek(10000, io.SEEK_END)

f.close()
```
