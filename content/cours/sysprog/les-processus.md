---
title: "Les Processus"
date: 2019-09-17T14:28:14+02:00
clsdate: 2019-09-17
draft: false
---

# Les processus

## Le processeur

Un processeur ne peut exécuter qu'un seul calcul à la fois. S'il paraît être multitâche, c'est
que le processeur est capable d'exécuter les instructions que lui donne le noyau petit à petit
mais de façon très rapide. Il faut toutefois noter que la technologie multicoœur permet
de simuler aujourd'hui le comportement de plusieurs processeurs travaillant de façon
concomitante.

{{< mermaid >}}

graph TD
	subgraph CPU Multicoœur
		cpu1[C#1]
		cpu2[C#2]
		cpu3[C#3]
		cpu4[C#4]
		reg1[R]-->cpu1
		reg2[R]-->cpu1
		reg3[R]-->cpu2
		reg4[R]-->cpu2
		reg5[R]-->cpu3
		reg6[R]-->cpu3
		reg7[R]-->cpu4
		reg8[R]-->cpu4
		cpu1-->reg9[R]
		cpu1-->reg10[R]
		cpu2-->reg11[R]
		cpu2-->reg12[R]
		cpu3-->reg13[R]
		cpu3-->reg14[R]
		cpu4-->reg15[R]
		cpu4-->reg16[R]
	end

{{< /mermaid >}}

La commande ```lscpu``` ou ```cat /proc/cpuinfo``` permet de récupérer des infos sur le processeur.

| Nom       | Infos         |
|-----------|---------------|
| Socket    | Emplacement physique du CPU |
| Architecture | Façon dont est construit le processeur |
| Boutisme ("byte order") | Façon dont les données sont traitées |
| Famille | Identifie les types d'instructions compatibles |

Les programmes, **exécutables** résidant dans le système de fichier, s'instancient par
le noyau dans des **processus**. Ainsi il peut y avoir plusieurs instances d'un
même programme.

Le noyau orchestre ainsi l'exécution de ces processus. Ils ont chacun
un **PID** (Process ID) et un espace d'adressage en mémoire vive.

## Les programmes

Un programme contient toutes les informations nécessaires à la création
du processus qui découle de exécution:

- Un binaire au format [**ELF**](https://fr.wikipedia.org/wiki/Executable_and_Linkable_Format)
(Executable and Linkable Format)
- Des **instructions** compéhensibles par la machine (découlant de la compilation)
- Le point d'entrée de la première instruction
- Des données
- Des informations pour le debugger
- Des informations sur les librairies partagées

### Informations sur les exécutables

La commande **file** permet de déterminer plein d'informations sur un exécutable.

```bash
$ file /bin/ls
/bin/ls: ELF 64-bit LSB pie executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, for GNU/Linux 3.2.0, BuildID[sha1]=a8ff61ef3efee1ff8a611497215c9af0ebe181c1, stripped
```

On y trouve entre autre le format ELF... L'exécutable lui-même peut également
donner beaucoup d'informations...

```bash
& head -c 64 /bin/ls | hexdump -C
00000000  7f 45 4c 46 02 01 01 00  00 00 00 00 00 00 00 00  |.ELF............|
00000010  03 00 3e 00 01 00 00 00  e0 5a 00 00 00 00 00 00  |..>......Z......|
00000020  40 00 00 00 00 00 00 00  90 13 02 00 00 00 00 00  |@...............|
00000030  00 00 00 00 40 00 38 00  0b 00 40 00 19 00 18 00  |....@.8...@.....|
```
En regardant [la page wiki du format ELF](https://fr.wikipedia.org/wiki/Executable_and_Linkable_Format),
on peut comparer les octets d'en-tête avec la valeur affichée par le **hexdump** et reconstruire
les infos données par **file**...

### Du point de vue du noyau

Un processus est une **entité abstraite** du point de vue du noyau. Ce dernier lui
alloue des ressources pour son exécution.

Du point de vue du noyau, un processus consiste:

| En mode utilisateur (user-land)   | En mode kernel (kernel-land)   |
|-----------------------------------|--------------------------------|
| Une portion d'espace-mémoire | Une structure de données de l'état du processus |
| Le code du programme | La table des fichiers ouverts |
| Les variables auquelles le code a accès | La table de mémoire allouée |
| | Le répertoire courant |
| | La priorité |
| | Le gestionnaire d'évènements (singaux, fin de programme, etc.) |
| | Les limites du processus |

Le **PID** est partagé entre le noyau et l'espace utilisateur

## Programmation

### Exécution du main

Un programme en C commence par convention par l'exécution de la fonction **main**.

- **argc**: le nombre d'arguments du programme
- **argv**: tableau de pointeurs sur les arguments

L'invocateur du programme exécute l'appel système **execvp** et utilise les arguments
passés au programme comme paramètres.

#### Exemples en C et python

```c
#include <stdio.h>
#include <stdlib.h>

int main (int argc, char **argv) {
	for(int i=0; i<argc; i++) {
		printf("argv[%d]=%s\n", i, argv[i]);
	}
	exit(EXIT_SUCCESS);
}
```

```python
#!/usr/bin/python3

import sys

def main():
	i = 0
	for arg in sys.argv:
		print("argv[%d]=%s" % (i, arg))
		i += 1

main()
```

### Terminaison du main

Il y a plusieurs façons de terminer un programme

#### Terminaison normale

- Retour du main
- exit() (ou _exit() - syscall)
- Nettaoyage de toutes les ressources de la libc
	- appel de *close* sur tous les FD ouverts
	- appel de gestionnaires de sorties (exit handlers)

#### Terminaison anormale

- Réception d'un signal (**kill** par exemple)

<div class="alert alert-info"><p>Par convention sous UNIX, un programme qui termine
avec un <strong>code de retour égale à 0 a été exécuté avec succès.</strong> Dans
n'importe quel cas contraire, le programme a échoué.</p><p><strong>echo $?</strong> affiche
le code sortie de la dernière commande exécutée ;-)</p></div>

La fonction **atexit** enregistre la fonction donnée pour que celle-ci soit
automatiquement appekée lorsque le programme se termine normalement.

<div class="alert alert-info">Les exemples en C sont sur le PDF</div>

```python
import sys, os, atexit

def trigger_exit():
	print("Invoking exit handler")

def main():
	print("Invoking main()...")
	atexit.register(trigger_exit)
	print("before exiting...")
	sys.exit(os.EX_OK)

main()
```

## Anatomie d'un processus en mémoire

La mémoire allouée a chaque processus est composée de parties appelées **segments**.

(photo Habib?)

<center>![Processus en mémoire](/cours/sysprog/pile.png)</center>

### Segment Text

Contient les instructions en langage machine du programme. Segment en lecture seule.

### Segment des données initialisées

**Initailized data segment**. Variables explicitement initiliasées avec une valeur.
Ces valeurs sont lues depuis le fichier exécutable quand il est chargé en mémoire.

### Segment des valeurs non initialisées

**Uninitialized data segment** ou **bss segment**. Contient les variables globales et statiques
qui n'ont pas été explicitement initialisées. L'espace requit par ces variables est alloué par le
noyau à l'exécution du programme.

### La pile

**Stack**. Segment dynamique qui contient des *blocs de pile* **stack frames**. Il y en a une par fonction
appelée. Une **frame** contient les variables locales de la fonction et la valeur de retour.
C'est cet esapce qui peut être rempli accidentellement par une fonction qui boucle à l'infini.

```c
# Dans ce cas, l'appel de la fonction va initialiser la variable e
# dans la stack.
int main() {
	int a = 3;
	int b;
	foo(a, b);
}

void foo(int a, int b) {
	int e;
}
```
### Le tas

**Heap**. Zone où la mémoire (pour les variables) peut être synamiquement allouée à l'exécution du programme.

## Hiérarchie des processus

Sous UNIX, les processus sont définis sous forme **arborescente**:

- Chaque processus a un **unique** parent (sauf *init* de PID=1)
- Chaque processus peut avoir 0 ou plusieurs processus fils

On peut appréhender cette arborescence avec des commandes comme ```ps faux```, ```ps tree``` ou ```htop```

### Création de processus

Un programme qui crée un processus le fait en créant un **fork**. Le procesuss fils commence son exécution
juste après le fork et le processus père continue son exécution. Le processus fils est un copie de son parent,
et le segment **Text** est partagé entre les deux. En effet, la copie pure n'est pas nécessaires et seules les
zones modifiées occupent de la mémoire supplémentaire.

<div class="alert alert-info">
<p>
<b>En cas de succès:</b><br>
Le <strong>PID</strong> du fils est renvoyé au processus parent, et 0 est renvoyé au fils.
</p>
<p>
<b>En cas d'échec:</b><br>
<strong>-1</strong> est renvoyé au parent, aucu processus n'est créé et <strong>errno</strong> contient
le code d'erreur approprié.
</p>
</div>

```python
import os, sys

def main():
	print("Starting process with PID=%d" % (os.getpid()))
	try:
		pid = os.fork()
	except OSError as e:
		print("Unable to fork")
	
	if pid == 0:
		print("Starting child with PID=%d" (my parent PID=%d" % (os.getpid(), os.getppid()))
	else:
		print("Still in process with PID=%d" % (os.getpid()))
	print("Finishing process with PID=%d" % (os.getpid()))
	
	sys.exit(os.EX_OK)

main()
```

os.getpid() retourne donc le PID fils quand le fork est créé, alors que 
os.getppid() retourne le PID parent.

### Terminaison d'un processus

À la terminaison d'un processus, le noyau se charge de nettoyer les ressources allouées,
(fermeture de FD, libération de la mémoire,...). Le noyau stocke également le status de
terminaison du processus jusqu'à ce que le processus parent en prenne conscience. Si le
processus parent se termine avant le processus fils, le processus fils devient le fils
du processus ```init``` de PID=1.

Lorsqu'un processus père crée un fork dans lequel il exécute un autre programme, il doit
attendre que ce processus fils se termine pour pouvoir gérer les retours de processus
terminé. Cela s'effectue grâce à la fonction ```os.wait()``` ou ```os.waitpid()```

```python
import os, sys, time

def main():
	print("Starting parent process with PID=%d" % (os.getpid())
	for i in range(1,4):
		try:
			pid = os.fork()
		except OSError as e:
			print("Unable to fork")
		
		if pid == 0:
			print("Starting child process with PID=%d (my parent PID=%d)" % (os.getpid(), os.getppid()))
			time.sleep(i*3)
			print("Finishing child process with PID=%d sec" % (os.getpid(), i*3))
			sys.exit(EX_OK)
	
	cont = True
	while cont:
		try:
			pid, status = os.waitpid(-1, os.WNOHANG)
		except OSError as e:
			cont = False
		if pid > 0:
			print("Process with PID=%d has terminated with status=%d" % (pid, status))
	
	print("Finishing parent process with PID=%d" % (os.getpid()))
	sys.exit(os.EX_OK)

main()
```

### Zombie

Un processus **zombie** est un processus qui a terminé son exécution mais dont le processus
parent n'a pas encore pris connaissance du status de terminaison.

### Ressources partagées en tre père et fils

Après un fork les deux processus se partagent **la table des fichiers ouverts**. C'est-à-dire
que si pour un fichier donné, si l'offset est modifié (read, write, lseek) par l'un des deux,
cela s'applique à l'autre.

Cela permet entre autre de faire collaborer plusieurs processus sur les mêmes données.

### Cas d'usage d'un fork

Un programme peut se dupliquer pour exécuter en parallèle des parties de lui-même:

- Serveur web qui crée plusieurs processus pour traiter plusieurs requêtes à la fois
- Traitement asynchrone des données reçues par le réseau  par un programme qui
commence à les traiter avant d'avoir tout reçu

Ou peut souhaiter récupérer le résultat d'un autre programme exécuté pour le gérer
lui-même:

- Un shell
- Un éditeur de code qui lance la compilation du programme en cours d'édition
et qui affiche le résultat

## Exécution d'un autre programme

Sous UNIX, la **création** d'un nouveau processus et **l'exécution** d'un programme
sont deux opération bien distinctes.

<div class="alert alert-danger">
<p>
L'exécution d'un nouveau processus ne crée pas un nouveau processus, par contre cela
remplace le programme appelant le nouveau en allant le chercher sur le système de
fichiers.
</p>
<p>
À l'exécution, tous les segments mémoires sont réinitialisés comme si le programme
avait été exécuté normalement.
</p>
</div>

C'est pourquoi il faut s'assurer de créer un fork dans un programme pour récupérer
le résultat d'un autre programme et le traiter dans le processus parent.

### La famille exec

L'exécution du programme se fait grâce à l'appel système ```execve```.

```bash
man 2 execve
```

Il existe [plusieurs façons différentes](https://docs.python.org/fr/3.7/library/os.html?highlight=execve#os-process)
de réaliser cet appel système.

## TP: Programme qui exécute un autre programme

### Consigne

Réaliser un programme en python et en C. On lui passe un autre commande en argument
(par exemple ```ls -l /tmp```. Le programme doit afficher:

- La commande à exécuter tel quelle
- Le retour de la commande
- Si la commande s'est bien passé ou non + le code de status

### Code python

```python
import os, sys

def main():
	line = "-> " + " ".join([arg for arg in sys.argv[1:]])
	print("La commande à exécuter:")
	print(line)
	cmd = sys.argv[1]
	args = sys.argv[1:]
	execute(cmd, rags)

def execute(cmd, args = []):
	print("Début du fork pour l'exécution de la commande...")
	try:
		pid = os.fork()
	except OSError as e:
		print("Erreur lors du fork :(")
		os.exit(os.EX_FAIL)
	
	if pid == 0:
		print("-> Résultat de la commande:")
		try:
			os.execvp(cmd, args)
		except FileNotFoundError as e:
			print("Commande [%s] introuvable: %s" % (cmd, e))
		except Exception as e:
			print("Impossible d'exécuter la commande [%s]: %s" % (cmd, e))
	
	else:
		npid, status = os.wait()
		if status == 0:
			print("Tout s'est bien déroulé. Le processus PID=%d s'est terminé avec le status %d" % (npid, status))
		else:
			print("Il y a eu une erreur à lors de l'exécution de la commande. Le processus PID=%d s'est terminé avec le status %d" % (npid, status))
	
	sys.exit(EX_OK)

main()
```
			
