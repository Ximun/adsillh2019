---
title: "Ldap"
date: 2019-10-03T19:01:46+02:00
draft: true
---

# Intro

LDAP (Lightweight Directory Access Protocol) est à l'origine un protocole
permettant l'interrogation et la modification des services d'annuaire. Il a
cependant évolué pour représenter une norme pour les systèmes d'annuaire,
incluant un **modèle de données**, un **modèle de nommage** un **modèle de
réplication**.
