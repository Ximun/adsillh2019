---
title: "Ldap Et Postfix"
date: 2019-10-10T14:24:06+02:00
draft: false
---

# LDAP et Posftix

Le but ici est d'installer un serveur de mail Postfix et qu'il fasse référence
aux utilisateurs Posix pour la gestion des comptes.

## Installation de Postfix

```bash
apt-get install potfix

# Si besoin, relancer la configuration
dpkg-reconfigure postfix
```

### Configuration

Lors du dpkg-reconfigure, choisir:

- **Site Internet**
- **Nom de courrier**: nom du hostname par défaut

## Implémentation du Schéma MailGroup

Afin que chaque utilisateur ait une adresse mail correspondante à son propre
compte ldap, il faut charger un schéma de Mail dans les schémas LDAP, qui inclu
un champ ```mailAlternateAdress``` ou ```mailAlias```.

Dans un second temps, on indiquera une règle postfix qui reconnaîtra l'adresse
mail alternative de l'utilisateur et fera le lien avec son adresse mail locale.

### Chargement du modèle de mail

J'ai trouvé ce [schéma
postfix-book](https://github.com/variablenix/ldap-mail-schema/blob/master/postfix-book.schema).
Je ne sais pas si c'est le plus approprié mais ça suffit pour le TP. Le schéma
[**MailAlias**](https://docs.oracle.com/cd/E19455-01/806-5580/appendixa-6/index.html)
semble également intéressant...
On charge notre schéma dans LDAP:

```bash
curl https://raw.githubusercontent.com/variablenix/ldap-mail-schema/master/postfix-book.schema > /etc/ldap/schema/postfix-book.schema

# On va avoir besoin de l'outil schema2ldif afin de convertir notre schema en
# format ldif.
apt-get install schema2ldif
cd /etc/ldap/schema/
schema2ldif postfix-book.schema > postfix-book.ldif

# Et pour finir on charge la règle qui va charger le modèle dans notre conf
# LDAP.
ldapadd -Q -Y EXTERNAL -H ldapi:/// -f /etc/ldap/schema/postfix-group.ldif

# Sans oublier de redémarrer notre service LDAP
systemctl restart slapd
```
### Modification des utilisateurs

Selon le mode d'édition de notre arborescence LDAP, il faut ajouter à chaque
utilisateur l'*objectClass* **PostfixBookMailAccount** et l'attribut *mailAlias*
**<utilisateur>@<LDAP domain>**

### Création de l'alias dans la configuration Postfix

Il nous reste à indiquer à Postfix comment faire le lien entre l'attribut
**mailAlias** et l'attribut **mail** lorsqu'il reçoit et envoie un mail.

Ça se passe dans le fichier d'alias (le créer s'il le faut)
**/etc/postfix/ldap-aliases.cf** ou avec le nom que vous voulez.

```bash
server_host = ldap://127.0.0.1
# Normalement on indique le port

# Indique en dessous de quel domaine on cherche
search_base = dc=kaamelott,dc=chateau

# On active le bind
bind = yes

# ld dn utilisé
bind_dn = cn=admin,dc=kaamelott,dc=chateau

# Le mot de passe
bind_pw = root

# Indique sur quel attribut chercher un filtre
query_filter = (&(objectClass=posixAccount)(mailAlias=%s))

# Indique par quel attribut remplacer
result_attribute = mail
result_format = %s
```
