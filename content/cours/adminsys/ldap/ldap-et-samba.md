---
title: "Ldap Et Samba"
date: 2019-10-10T09:56:53+02:00
draft: false
---

# LDAP et Samba

## Installation de Samba

Le but est d'installer un serveur samba qui fonctionne à travers le schéma de
notre configuration LDAP. On commence par installer samba.

```bash
apt-get install samba smbldap-tools
```
## Liaison avec LDAP

D'après la [doc de
Debian](https://wiki.debian.org/LDAP/OpenLDAPSetup#For_SAMBA_LDAP_support), la
paquet ```samba``` semble avoir un fichier ```samba.schema``` deprécié, et un
fichier ```samba.schema.gz``` qui est correct. On le décompresse dans le
répertoire des schémas ldap, (avec le fichier ldif):

```bash
zcat /usr/share/doc/samba/examples/LDAP/samba.schema.gz > /etc/ldap/schema/samba.schema
zcat /usr/share/doc/samba/examples/LDAP/samba.ldif.gz > /etc/ldap/schema/samba.ldif
```

Il faut maintenant charger le schéma ```samba.ldif``` dans la configuration de
ldap:

```bash
ldappadd -Q -Y EXTERNAL -H ldapi:/// -f /etc/ldap/schema/samba.ldif
```

On vérifie qu'on voit bien le nouveau schéma samba:

```bash
# ldapsearch -LLLQY EXTERNAL ldapi:/// -b cn=schema,cn=config
"(objectClass=olcSchemaConfig)" dn
dn: cn=schema,cn=config

dn: cn={0}core,cn=schema,cn=config

dn: cn={1}cosine,cn=schema,cn=config

dn: cn={2}nis,cn=schema,cn=config

dn: cn={3}inetorgperson,cn=schema,cn=config

dn: cn={4}samba,cn=schema,cn=config

```

Normalement, le paquet ```smbldap-tools``` vient avec la commande
**smbldap-config**. Si ce n'est pas le cas, il peut être nécessaire de
l'utiliser depuis les sources (ce que j'ai eu à faire)

```bash
apt-get install dpkg-dev # Si nécessaire
apt-get source smbldap-tools
cd smbldap-tools-0.9.9
./configure
make

# On peut maintenant exécuter smbldap-config
./smbldap-config.cmd

# Plusieurs paramètres à indiquer (ou à laisser par défaut)
Samba Configuration File Path [/etc/samba/smb.conf]
Smbldap-tools Configuration Directory Path [/usr/local/etc/smbldap-tools]
workgroup name [WORKGROUP] > chateau # Ou ce que vous voulez
netbios name [] > samba
logon drive [] >
logon home [\\samba\%U]
logon path [\\samba\profiles\%U]
home directory [/home/%U]
default users' homeDirectory mode [700]
default user netlogon script []
default password validation time [45]
ldap suffix [] > dc=kamelott,dc=chateau
ldap group suffix [] > ou=groups,dc=kamelott,dc=chateau
ldap user suffix [] > ou=habitants,dc=kamelott,dc=chateau
ldap machine suffix []
Idmap suffix [ou=Idmap]
sambaUnixPooldn object [sambaDomainName=chateau]
ldap master server [127.0.0.1]
ldap master port [389]
ldap master bind dn [] > admin
ldap master bind password [] > root
ldap slave server [127.0.0.1]
ldap slave port [389]
ldap slave bind dn []
ldap slave bind password []
ldap tls support [0]
SID for domain chateau [xxxxxx]
unix password encryption [SSHA]
default user gidNumber [513]
default computer gidNumber [515]
default login shell [/bin/bash]
default skeleton directory [/etc/skel]
default domain name to append to mail address []
```
Il peut être utile, avant de continuer, de faire un backup de notre DIT Ldap...

```bash
slapcat -l backup.ldif
```
