---
title: "Installation"
date: 2019-11-07T10:56:37+01:00
draft: false
---

# Installation de Docker

Nous allons maintenant installer `docker` sur un
système Debian 10.  L'installation suivie est décrite
[ici](https://linuxwize.com/post/how-to-install-and-use-docker-on-debian-10)

```bash
# Installation des paquets nécessaires à l'ajout de dépôt via HTTPS
sudo apt-get update
sudo apt install apt-transport-https ca-certificates curl software-properties-common gnupg2

# Importation de la clé gpg de docker via curl
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -

# Ajout du dépôt Docker
# $(lsb_release -cs) retourne le nom de la distribution Debian (buster ici)
sudo add-apt-repository "deb https://download.docker.com/linux/debian $(lsb_release -cs) stable"

# Installation de Docker Community Edition
sudo apt-get update
sudo apt-get install docker-ce

# Vérifier le Docker Daemon est bien lancé
sudo systemctl status docker

# Ne pas oublier d'ajouter l'utilisateur en cours au groupe 'docker', car par
# défaut docker ne peut être lancé que via les droits sudo
sudo usermod -aG docker $USER

# Enfin, on peut essayer de lancer l'image 'hello-world' dans un conteneur
# afin de vérifier que docker fonctionne bien !
docker run hello-world
```

# Premier conteneur de  serveur web: nginx

Maintenant, on peut essayer de lancer un conteneur basé sur l'image
`nginx` et essayer de contacter le serveur qui tourne sur ce même conteneur.

```bash
docker run --name petit-serveur -p 8080:80 -d nginx
```

Explication des flags:

- `--name` sert à donner un nom à notre conteneur
- `-p` indique les deux ports qui vont communiquer:
  - à gauche: le port à ouvrir sur la machine hôte
  - à droite: le port sur lequel se connecter dans le conteneur (ici,
80 car on installe ici un serveur web)
- `-d` sert à lancer le conteneur en tâche de fond. Sans ce flag, on peut
remarquer que le terminal affiche les logs de connexion sur le serveur web.

`nginx` indiqué en bout de ligne de commande correspond au nom de l'image
que docker va récupérer directement sur le docker-hub et lancer dans
le conteneur.

Pour vérifier que notre serveur web nginx tourne bien dans notre conteneur,
il n'y a plus qu'à lancer un navigateur et vérifier:

![navigateur web visualisant la page web par défaut de nginx sur
localhost:8080](/cours/adminsys/docker/nginx-test.jpg)

Docker est maintenant installé et complètement utilisable sur notre système,
on peut essayer maintenant de lancer un serveur web fonctionnant avec PHP
et une base de données persistante...

# Introduction aux volumes

Pour l'instant, à l'arrêt d'un conteneur, toutes les possibles modifications
sont perdues. Il va donc falloir trouver un moyen pour faire en sorte que
nos données restent persistantes. Pour cela on va utiliser les **volumes**.

En effet, notre serveur web va devenir vraiment utile si l'on peut choisir
les fichiers qu'il va servir... Pour cela on va avoir recourt aux volumes. On
va "lier" le contenu d'un dossier sur notre machine hôte, dans le contenu
du dossier lu par le serveur web duc conteneur. Pour utiliser les volumes,
cela se fait grâce au flag `-v`.

Créons un dossier avec un `index.html` très simple pour tester...

```bash
mkdir web
touch web/index.html
echo "Bonjour Monde!" > web/index.html
docker run --name petit-serveur -p 8080:80 -v /home/user/web:/usr/share/nginx/html -d nginx
```

Et maintenant, si l'on retourne sur notre navigateur:

![index.html depuis conteneur nginx](/cours/adminsys/docker/nginx-volume.jpg)

On commence à comprendre l'intérêt de docker... On peut par exemple en
une seule ligne de commande lire le même site web mais avec des serveurs
différents! Il suffit de remplacer `nginx` par `apache` ou le nom de l'image
associé par exemple, sans avoir à remonter toute une machine virtuelle et
la configuration qui va avec...

Dans un prochain temps, on va pouvoir lier plusieurs conteneurs ensemble
et les faire communiquer, par exemple une appliction `php` avec une base
de données `mariadb`... Pour cela, on peut toujours le faire en ligne de
commande mais il existe un outil qui permet de gérer ça plus simplement:
[docker-compose](/cours/adminsys/docker/docker-compose)
