---
title: "Docker Compose"
date: 2019-11-07T16:42:16+01:00
draft: false
---

# docker-compose

La ligne de commande de `docker` est pratique, mais peut devenir vraiment
longue... `docker-compose` est un outil qui vient en plus de `docker`
et qui permet de simplement définir nos conteneurs et leur éventuelle
communication via un fichier de configuration en format **yaml**.

On va s'en servir ici pour déployer un site wordpress qui dialogue avec
une base de données, en rendant le contenu de l'un et de l'autre persistant
(donc en utilisant les volumes)

## Installation

Si le dépôt docker a bien été ajouté comme décrit dans la partie
[installation](/cours/adminsys/docker/installation), l'installation de
docker-compose devrait être aussi simple que ça:

```bash
sudo apt-get install docker-compose
```

## Données persistantes

Il va falloir sauvegarder les fichiers de notre site et de notre de base de
données dans des dossiers persistants sur notre machine hôte.

```bash
# Pour les données de la base de données
mkdir db-data

# Il faut aussi récupérer les sources de Wordpress
wget https://wordpress.org/latest.tar.gz
tar xzvf latest.tar.gz
```

## Écriture du docker-compose

```yml
wordpress:
  image: webdevops/php-apache-dev
  volumes:
    - ./wordpress:/app
  ports:
    - 8080:80
  links:
    - db

phpmyadmin:
  image: phpmyadmin/phpmyadmin
  ports:
    - 8081:80
  links:
    - db

db:
  image: db
  volumes:
    - ./db-data:/var/lib/mysql
  environment:
    - MYSQL_ROOT_PASSWORD=root
```

### Application `web`

Chaque conteneur est identifié par son nom, et toutes sa configuration est
identée à l'intérieur.

Ici, l'application `wordpress` est basée sur l'image
[webdevops/php-apache-dev](https://dockerfile.readthedocs.io/en/latest/content/DockerImages/dockerfile/php-apache-dev).
le dossier `./wordpress` dans lequel on a décompressé les sources
de Wordpress est lié avec le contenu du dossier servir par serveur web
déclaré dans l'image `webdevops/php-apache-dev`, situé dans `/app`. Le
port 8080 de notre machine communique avec le port 80 du conteneur.

Jusque ici rien de nouveau par rapport à la ligne de commande. Pourtant
le plus important est la déclaration de `links`. C'est cette ligne qui va
faire que nos conteneurs vont pouvoir communiquer.

### Application `phpmyadmin`

Elle va nous servir à passer par une interface web pour manipuler nos bases
de données, disponible sur le port `8081`.

### Application `db`

Il s'agit de notre base de données (ici `mariadb`). Il ne faut pas oublier
de renseigner la variable d'environnement `MYSQL_ROOT_PASSWORD=root`, sinon
nous ne pourrons pas nous connecter en tant que root à la base de données
à partir de Wordpress ou Phpmyadmin.

A noter qu'il est possible de renseigner d'autres variables d'environnement
pour créer des utilisateurs différents. Tout est expliqué sur la
[page](https://hub.docker.com/_/mariadbd) de l'image `mariadb`

## Lancement des conteneurs

Pour lancer nos conteneurs il faut donc se placer dans le bon dossier et le
fait de lancer `docker-compose up` fait que docker va directement lire le
fichier `docker-compose.yml`, télécharger les images si besoin et lancer
les conteneurs.

Il est possible de récupérer directement l'image de Wordpress, sans avoir à
télécharger et décompresser les sources. Mais j'aime bien avoir quand même la
main sur les fichiers de mon application...

```bash
docker-compose up -d
```

On peut maintenant créer la base de données qui accueillera notre Wordpress
en se connectant à l'instance Phpmyadmin (login: root, pass: root) sur
`localhost:8081`, puis se rendre sur notre instance Wordpress et procéder
à son installation sur `localhost:8080`.

## Fermeture des conteneurs

```bash
docker-compose down
```

Voila qui va fermer les conteneurs lancés plus tôt avec
`docker-compose`. Toutes les modifications seront sauvegardés dans nos
dossiers `./wordprress` et `./db-data`, et nous retrouverons exactement
l'état dans lequel on les a laissé lors d'un nouveau `docker-compose up`.

## Dockerfile

L'étape d'après est d'écrire nos propres images avec
[Dockerfile](/cours/adminsys/docker/dockerfile)
