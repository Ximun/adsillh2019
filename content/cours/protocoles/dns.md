---
title: "Dns"
date: 2019-09-26T14:45:55+02:00
draft: true
---

# DNS (Domain Name System)

Service permettant d'étblir une correspondance entre une adresse IP et un nom de domaine,
et plus généralement de trouver une information à partir d'un nom de domaine. Il a été
créé en 1983 par Paul Mockapetris.

Aux origines de TCP/IP, les réseaux étaient très peu étendus. Les administrateurs avaient
donc recours à des fichiers appelés **tables de conversion manuelle**, généralement
nommés **hosts** ou **hosts.txt**. Il est d'ailleurs toujours trouvable et configurable
dans ```/etc/hosts```, et peut servir par exemple à rediriger toutes les requêtes
facebook dans le vide (au hasard).

```bash
127.0.0.1	facebook.com
::1				facebook.com
```
En 1987, le fichier HOSTS.TXT contenait 5500 entrées, tandis que 20000 hôtes étaient
présentes sur Internet.

## Rôle du DNS

Résoudre un nom de domaine consiste à trouver l'adresse IP qui lui est associée.
Le protocole DNS supporte l'IPv4 et l'IPv6, et utilise généralement UDP sur le
port 53.

## Hiérarchie

Le système doit fonctionner sur un accord global, car toutes les adresses doivent
correspondre aux mêmes adresses. Il y a donc, un peu comme pour le protocole
NTP, des serveurs racines qui distribuent l'information.

Seulement, les annuaires sont énormes. Si toutes les adresses devaient être
inscrites au même endroit, cela constitueraient des annuaires gigantesques,
et qui changeraient constamment. On a donc résonné en sous-domaines...

### TLD (Top Level Domain)

Les domaines de premier niveau sont gérés indépendamment. Il en existe
de plusieurs sortes:

- **gTLD**: domaines génériques (.com ou .org ...)
- **ccTLD**: country code (fr, be, ch ...)

### Sous-domaines

| Nom | Type |
|-----|------|
| **fr com eu tv musee ...** | TLD |
| **u-bordeaux aquilenet ffdn ...** | Domaine |
| **www ftp cpanel ...** | Service |

Le **FQDN** (Full Qualificative Domain Name) est le nom total qui permet de retrouver
exactement le bon service. (par exemple: **www.emi.u-bordeaux.fr**)

## Résolution

Les hôtes n'ont qu'une connaissance limitée du système des noms de domaine.
Quand ils doivent résoudre un nom, ils s'adressent à un ou plusieurs serveurs de noms
dits *récursifs*, c'est-à-dire qui vont parcourir la hiérarchie DNS et faire suivre
la requête à un ou plusieurs autres serveurs de noms pour fournir une réponse.

Les adresses IP de ces serveurs récursifs sont souvent abtenues via DHCP ou encore
configurés en dur sur la machine hôte. Les fournisseurs d'accès à Internet mettent
à disposition de leurs clients ces serveurs récursifs.


## Résolution inverse

Pour trouver le nom de domaine associé à une adresse IP, on utilise un principe
semblable.

Dans un nom de domaine, la partie la plus générale est à droite: par ewemple **org**
dans wikipedia.org. Le mécanisme de résolution parcourt donc le nom de domaine
de droite à gauche.

Dans un adresse IP, c'est le contraire: 91 est la partie la plus générale de
91.198.174.192. On va donc résoudre l'adresse IP depuis la fin:

```bash
$ host 91.198.174.192
2.174.198.91.in-addr.arpa domain name pointer ae1-100.cr1-esams.wikimedia.org.
```
## Enregistrements DNS

### A

Redirige vers une adresse IPv4

### CNAME

alias vers un autre nom de domaine

### MX record

Serveurs SMTP à contacter pour envoyer un mail.

### NS

Le nom des serveurs qui font autorité dans la zone. Ces serveurs connaissent la
totalité des adresses des machines de ce domain-là.

### PTR

À l'inverse d'une entrée de type A, une entrée PTR indique à quel nom d'hôte correspond
une adresse IPv4. Si elle est spécifiée, elle doit contenir l'enregistrement
inverse d'une entrée DNS A.

### SOA

Donne des informations sur l'autorité ou le responsable de la zone.

### TTL

Chaque record est  associé à un TTL (Time To Live) qui détermine combien de temps il
peut être conservé dans un cache. Typiquement il est d'un jour (86400s)

## DNS Dynamique

Une extension du DNS nommée DNS Dynamique (DDNS) permet à un client de mettre à jour
une zone avec des informations qui le concernent. C'est par exmple utile quand des
clients obtiennent une adresse IP par DHCP et qu'ils souhaitent que le DNS
reflète le nom réel de la machine.

## Mise à jour du DNS

Les mises à jour se font sur le serveur primaire du domaine, les serveurs secondaires
recopiant les informations du serveur primaire dans un mécanisme appelé **transfert de zone**.

## Commandes pour consulter les DNS

```bash
$ nslookup aquilenet.fr
Server:         10.5.255.254
Address:        10.5.255.254#53

Non-authoritative answer:
Name:   aquilenet.fr
Address: 185.233.100.8
Name:   aquilenet.fr
Address: 2a0c:e300::8

$ dig aquilenet.fr
; <<>> DiG 9.14.6 <<>> aquilenet.fr
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 49615
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 5, ADDITIONAL: 11

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;aquilenet.fr.                  IN      A

;; ANSWER SECTION:
aquilenet.fr.           1656    IN      A       185.233.100.8

;; AUTHORITY SECTION:
fr.                     148199  IN      NS      d.ext.nic.fr.
fr.                     148199  IN      NS      g.ext.nic.fr.
fr.                     148199  IN      NS      d.nic.fr.
fr.                     148199  IN      NS      f.ext.nic.fr.
fr.                     148199  IN      NS      e.ext.nic.fr.

;; ADDITIONAL SECTION:
d.ext.nic.fr.           56897   IN      A       192.5.4.2
d.ext.nic.fr.           56897   IN      AAAA    2001:500:2e::2
d.nic.fr.               56897   IN      A       194.0.9.1
d.nic.fr.               56897   IN      AAAA    2001:678:c::1
e.ext.nic.fr.           56897   IN      A       193.176.144.22
e.ext.nic.fr.           56897   IN      AAAA    2a00:d78:0:102:193:176:144:22
f.ext.nic.fr.           56897   IN      A       194.146.106.46
f.ext.nic.fr.           56897   IN      AAAA    2001:67c:1010:11::53
g.ext.nic.fr.           56897   IN      A       194.0.36.1
g.ext.nic.fr.           56897   IN      AAAA    2001:678:4c::1

;; Query time: 1 msec
;; SERVER: 10.5.255.254#53(10.5.255.254)
;; WHEN: jeu. sept. 26 16:54:19 CEST 2019
;; MSG SIZE  rcvd: 36
```

## Liens utiles

- http://www.frameip.com/dns
- http://www.misfu.com/cours/tutoriel/domain-name-service-dns--179.html
