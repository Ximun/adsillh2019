---
title: "Dhcp"
date: 2019-09-26T09:40:36+02:00
draft: true
---

# Protocole DHCP

Dynamic Host Configuration Protocol. Il s'agit d'un protocole qui permet à un ordinateur qui se connecte sur un réseau local
afin d'obtenir dynamiquemen et automatiquement sa configuration IP. Le but étant la simplification
de l'administration d'un réseau.

Lorsqu'une machine démarre sur un réseau, sa configuration réseau est vierge.
