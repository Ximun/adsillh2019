---
title: "Ntp"
date: 2019-09-26T11:29:31+02:00
draft: true
---

# NTP

Network Time Protocol. Les ordinateurs utilisent des horloges quartz qui ont la fâcheuse
tendance à dériver au bout d'un certains temps. Avec le développement informatique,
la synchronisation des horloges des systèmes informatiques communicants entre eux est
devenue absolument nécessaire.

Cependant, le temps est une mesure arbitraire et relative. Les machines doivent donc
se mettre d'accord sur une même heure commune partagée. Elles se sont fixées sur
l'heure [UTC](https://fr.wikipedia.org/wiki/Temps_universel_coordonné).

NTP permet de synchroniser, via un réseau informatique, l'horloge locale d'ordinateurs sur
une référence d'heure.

La RFC 1305 spécifie plusieurs aspects:

- La description du protocole réseau
- Les modes de fonctionnement
- Les algorythmes à mettre en place dans les machines

> La version 4 de NTP est une révisoin importante publiée dans la RFC 5905 en juin 2010.
> Un protocole SNTP (Simple Network Time Protocol) a également vu le jour.

NTP set un protocle permettant de synchroniser l'horloge d'un ordinateur avec celle d'un serveur
de référence. Il est basé sur **UDP** selon trois principes:

- Architecture
- Messages
- Algorythme

## Architecure

NTP prévoit une diffusion arborescente de proche en proche d'une heure de référence à
partir d'une ou plusieurs machines racines garantes. Mais ensuite, il est tout à fait
possible de s'échanger l'heure en plusieurs machines. On est finalement sur un réseau
pair-à-pair, à part pour les réseaux primaires.

Dans la temrinologie NTP, les serveurs de stratum 1 sont appelés **serveurs primaires**.

Les autres sont appelées **serveurs secondaires**.

<center>![Schéma de l'architecture d'un réseau maître NTP](/cours/adminsys/ntp-network.png)</center>

> Schéma de l'architecture d'un réseau maître NTP. Les flèches jaunes indiquent une connexion
> directe dédiée entre des horloges de précision et entre des serveurs informatiques
> de diffusion maîtres. Les flèches rouges indiquent une connexion via un réseau
> informatique.

# Ressources utiles

- https://www.frameip.com/ntp
