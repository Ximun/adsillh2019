---
title: "Index de vocabulaire des Base de Données"
date: 2019-09-16T16:01:40+02:00
draft: false
---

## Base de données relationnelles

| Terme           | Définition                           |
|-----------------|--------------------------------------|
| Base de données relationnelle | BDD qui va être utile dans le cas où il faut stocker et consulter des données qui ne sont pas indépendantes les unes des autres |
| Domaine | Ensemble de valeurs de qualité atomique (non décomposables - booléens, réels, énumérations finies etc.) |
| Produit cartésien | Toutes les compositions possibles de deux domaines |
| Relation / Table | Sous-ensemble de combinaisons issues d'un produit cartésien |
| Table | Une relation spécifique |
| Ensemble | |
| Attribut / Colonne | Nom donné à une colonne, composé d'un identifiant et d'un domaine |
| Degré d'une relation | nombre d'attributs |
| Tuple | Un élément de la **relation**. Correspond à une ligne de la table |
| n-uplet / Ligne | Un enregistrement dans la relation |
| Cardinalité | Nombre d'éléments (lignes) de la relation |
| Prédicat | Expression logique soit vraie, soit fausse |
| Contrainte | Prédicat qui défini les valeurs possibles d'un attribut |
| Dépendance référentielle | Type de contrainte |
| Schéma de bases de données | Ensemble de schémas de relations liés par des dépendances référentielles |
| Base de données | Ensemble de relations (extensions) associé au schéma de base de données et vérifiant toutes ses contraintes |
| Déterminant | Ensemble qui une fois connu suffit à définir une relation |
| Clef/Identifiant | Plus petit ensemble possible d'un déterminant qui permet de connaître toute la relation |
| Schéma de Relation | Schéma qui contient les attributs et les contraintes décrivant les dépendances entre les attributs |
| Clé étrangère | Clé qui fait le lien entre deux relations qui ont été tronquées |

### Modèle Entité-Association 


| Terme           | Définition                           |
|-----------------|--------------------------------------|
| Entité | Objet Physique réel |
| Association | Liaison entre deux entités |
