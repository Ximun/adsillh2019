---
title: "Encodage"
date: 2019-10-22T10:36:10+02:00
draft: true
---

# Encodage

```bash
Martine Â©crit en UTF-8
```

A l'origine, les caractères étaient encodés sur 7bits (ASCII). Le problème,
c'est que le reste du monde utilise beaucoup plus de caractères. Au début,
le 8ème bit (à l'origine le bit de parité) a été utilisé pour augmenter
le nombre de caractères encodés (ISO-8859-1 latin1). Cet encodage fonctionne
pour l'Europe de l'Ouest, mais ne contient toujours pas assez de place pour
les lettres du pays de l'Europe de l'Est. Il y a donc une nouvelle norme
ISO-8859-2 latin2, etc...

Lorsqu'on reçoit un fichier, on ne sait pas quel encodage utiliser pour le lire,
surtout avec Internet.

| Bits | Norme | Nom |
|------|-------|-----|
| 7 | IBM8(à 437 | ASCII |
| 8 | ISO-8859-1 | latin1 |
| 8 | ISO-8859-2 | latin2 |
| 8-16 | Chinoise | BIG65 |
| 8 | ISO-8859-15 | latin9 (créé pour l'€ et œ) |
|...|||

## Unicode

Depuis les années 90, l'idée a émergé d'avoir une table de caractère qui
contient tous les caractères du monde. C'est Unicode. On a prévu d'encoder
Unicode sur 32 bits, même si pour l'instant seulement 20 bits d'encodage
sont ouverts. UTF-8 est une manière de représenter Unicode par des octets.

En UTF-8, pour encoder les caractères de ```Ux0000 - Ux007f```...
Voir ```man UTF-8```
