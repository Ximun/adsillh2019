---
title: "Droit Relatif Aux Base De Donnees"
date: 2019-10-25T09:07:14+02:00
draft: true
---

# base de données

> On entend par base de données un recueil d'œuvres, de données ou d'autres
éléments indépendats, disposés de manière systématique ou méthodique, et
individuellement accessibles par des moyens électroniques ou par tout autre
moyen. (L.112-3 CPI)

On remarque une définition un peu bancale...

## Droits relatifs aix bases de données

On va repérer deux droits distincts:

- Droit relatif à l'agencement des matières
- 

## Droit relatif à l'agencement des matières

> "Jouissent de la protection instituée par le présent code sans préjudice des
droits de l'auteur de l'œuvre originale les auteurs d'anthologie ou de recueils
d'œuvres ou de *données diverses*, tels que les bdd, qui, par le choix ou la
disposition des matières, constituent des créations intellectuells"

Certains magistrats pensent donc que le fait d'ordonner des bases de données SQL
relève du droit d'auteur, alors qu'il n'y a aucune originalité à ça. La façon
d'ordonner les données dans une base est du domaine des idées, et certains
acteurs qui veulent nuire à d'autre peuvent utiliser la déclaration de cette loi
bancale.

## Droit des producteurs de Base de données

L'idée est de ne pas pouvoir repomper une base qui existe, surtout si elle doit
être rendue publique.

Cette loi avait vu le jour sous impulsion de France Telecom, dont des
concurrents faisaient de l'annuaire un annuaire inversé. Cependant, 
