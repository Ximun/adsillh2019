---
headless: true
---

<!-- --- title: "Licences" date: 2019-10-04T09:18:35+02:00 clsDate:
["2019-10-04, "2019-10-07"] draft: false --- -->

# Qu'est-ce qu'une licence ?

C'est une offre de contrat de la part du fournisseur, qui définit **les
conditions d'utilisation d'une œuvre**. Ce n'est pas vraiment un *contrat*,
qui suppose que les co-contractants sont de force égale pour pouvoir
négocier.

Le terme juridique est **pollicitation**.

Cette licence est basée sur le droit d'auteur ou le copyright. Une licence
**s'appuie sur le droit**. La licence sert justement à indiquer la façon
dont s'exerce le droit pour l'ayant droit.

# Types de licences

## "Licence privative" (propriétaire)

Tous les droits sont réservés par leur titulaire. C'est le cas de la
majorité des logiciels du commerce. Dans la quasi totalité des cas, le
client n'est **propriétaire que du support**, pas du logiciel. Le fournisseur
dégage toute responsabilité pour *vice caché* (bogues) dans les limites
du droit applicable (il y a tout de même peu de sanctions pour vice caché).

Le fournisseur peut arrêter la maintenance du logiciel à tout moment et
mettre les utilisateurs dans la panade...

## "Licence partagicielle" (shareware)

Licence privative autorisant la diffusion gratuite du logiciel, mais pour
lequel une contribution est demandée au bout d'un période d'essai.

> Un des derniers connu en date: Winrar

Cette licence date de la transition entre le monde des logiciels et le monde du
réseau, lorsque les petits logiciels n'avaient encore pas beaucoup de moyens
de se faire connaître et avaient besoin d'une politique de diffusion virale.
L'idée est de soutenir la maintenabilité du logiciel par ces dons.

Aujourd'hui, l'Internet met directement en communication l'utilisateur et
le développeur.

## "Licence gratuitiel" (freeware)

> terminologie française plus nuancée car le terme *Free* anglais signifie
à la fois *libre* et *gratuit* qui sont deux notions bien distinctes.

Licence privative autorisant la fourniture gratuite du logiciel mais ne
donnant pas nécessairement d'autres droits.

### Modèle de la régie publicitaire

<div class="alert alert-danger"> Les services coûtent énormément plus
cher que les logiciels.  </div>

Google offre un **service** gratuit, mais la question du service, différemment
du logiciel, nécessite un maintient important des infrastructures. Le
modèle est très différent: Le client est l'annonceur et Google est une
régie publicitaire. L'usager est la marchandise.

## "Licence libre" (free software)

Licence donnant de nombreux droits aux utilisateurs.

- Libre n'est pas nécessairement gratuit - Il n'est pas l'équivalent d'un
"freeware"!

## "Licence de domaine public volontaire"

L'auteur renonce à ses droits patrimoniaux et place son œuvre dans le
domaine public.

## Récapitulatif des types de licence

> Schéma gnu...

Dans certains cas, il se peut que des logiciels soient diffusés de plusieurs
façons différentes, par exemple en utilisant des licences de catégorie
différente (en libre téléchargement, donc à fois sous licence libre et
sous licence pivative en tant que "gratuiciel" et "partagiciel"
