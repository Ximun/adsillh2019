---
title: "Logiciel Libre"
date: 2019-10-07T11:57:31+02:00
clsDate: ["2019-10-07", "2019-10-14"]
draft: false
---

# Logiciel Libre

Le logiciel libre est une innovation **juridique** et non pas **technique**. On
s'appuie sur le **droit d'auteur** pour créer un contrat qui va organiser les
rapports entre les usagers et les créateurs. Cela crée des *droits* mais aussi
des *devoirs*.

Le but est d'annuler le coût de transaction juridique en permettant l'émergeance
de modèles énonomiques déconcentrés, adaptés à l'économie des biens matériels.

## Les quatre libertés

Ont en commun les logiciels libre:

- Liberté d'utilisation pour **tout** usage (pas de version *pro*, *home*,
*premium*, etc.)
- Liberté de copie du logiciel original
- Liberté de consultation et de modification du code source
	- nécessite l'accès au code source -> devoir de mettre à disposition
	le code
source également lors de la copie
- Liberté de rediffusion du logiciel modifié
	- Capitalisation du savoir
	- Mutualisation des développements

# Différences entre licences libres

> *"Un logiciel m'interdisant de prendre un code source, de le modifier,
le refermer et le vendre derrirère ne me rend pas vraiment libre!"*

> *"La liberté vient du fait de la collaboration. Un logiciel pour être
libre doit forcément être repartagé dans les mêmes conditions!"*

Ce sont deux conceptions complètements différentes de la liberté.

## Mode d'action des licences libres

On remarque trois façons de contribuer à un logiciel libre:

- Contribution au *cœur* du logiciel (correction de bug, etc.)
- Participation par ajout de *greffon*
- Création d'un module qui *utilise* le logiciel en arrière-plan

On obtient ainsi une **oœuvre composite** du logiciel de base. Étant donné que
l'œuvre composite doit avoir l'aval des ayant-droits, le créateur du logiciel
indique dans sa licence les droits qu'il offre aux contributeurs.

Les licences libres veillent à ce que **leurs termes** s'applique à toute œuvre
dérivée.

Il existe différents types de licences libres:

- Licences "persistantes"
- Licences "évanescentes"
- Licences "diffusives"

### Licences persistentes

Licence à "copyleft faible".

> _exemples_: LGPL, CeCILL-C (<- licence française)

La **LGPL** par exemple dit que tout ce qui est au même niveau que le logiciel
de base doit rester sous licence LGPL. Tout ce qui utilise le logiciel (module
ou greffon) peut être sur n'importe quelle licence (même privative).

L'œuvre dérivée peut donc être sous licence privative. Cette licence ne garantie
que l'œuvre originale, mais la rend *pérenne*, *persistante*. Un tel logiciel
est sous licence libre, mais permet à n'importe quelle boîte d'utiliser le code.

Ce qui doit rester libre reste libre.

### Licences évanescentes

Aussi appelés *permissives*, *non copyleftées*

> _exemples_: BSD, CeCILL-B

Ces licences indiquent que les parties libres du logiciel peuvent ne plus
l'être. Les versions modifiées d'un logiciel sous licence permissive peuvent ne
pas être diffusées et gardées sous licence non-libre.

[The 2-Clause BSD Licence](https://opensource.org/licenses/BSD-2-Clause)

Les liences permissives sont en général courtes, étant donné que plus les
licences sont restrictives, plus il va falloir être explicite (et inversement).

> La CeCILL-B permet de changer la licence du code modifié.

#### Objectif d'une telle licence

Imaginons le cas d'une norme de protocole réseau. Un texte déclarant une norme
est toujours susceptible d'être comprise de façons différentes. La meilleure
façon de diffuser une norme est de **diffuser également le code source**. Étant
donné que le code source est donné, autant donné la possibilité de le
ré-utiliser afin d'être sûr que les normes soient correctement implamentées...

### Licences "diffusives"

Aussi appelées à *copyleft fort*.

L'œuvre dérivée doit être distribuée selon les "termes" de la licence du
module. Mais **pas forcément la même licence**. Les modules fortement liés
doivent donc être couverts par des licences libres compatibles avec les termes
de la licence du module.

> "Si tu utilises la valeur de mon code sous licence libre, alors tu dois
redistribuer tes modifications sous licence libre également."

C'est la façon qu'a jugé bonne la FSF pour contrer les licences privatives,
jugeant que les licences permissives étaient trop en faveur des privateurs.

Les licences diffusives peuvent induire des conflits juridiques lorsqu'on
cherche à lier ensemble deux modules sous licences diffusives différentes.
Certaines licences "mineures" possèdent des clauses permettant de résoudre ces
onfiglts. La CeCILL-A cède explicitement le pas à la GPL par exemple...

exemple: [5.3.4](http://cecill.info/licences/Licence_CeCILL_V2.1-fr.html)

# Déclenchement des licences

Les premières licences libres ont été crées en un temps où les usages des
réseaux étaient peu développés (mise en œuvre centralisée des logiciels par
leurs usagers - personne physique ou morale). Ces licences sont déclenchées **par
la réception d'un exemplaire du logiciel**.

Or, le développement des systèmes de partage de fichiers pair-à-pair fait que
des personnes peuvent être redistributrices de logciels sans avoir décidé
d'accepter la licence. Ces licences ont du être amendées pour tenir compte de
réceptions involontaires, comme c'est le cas avec le pair-à-pair.

## SaaS

L'émergence des logiciels SaaS (Software as a service) fait que de nombreuses
personnes utilisent des logiciels libres sans pouvoir bénéficier des termes de
leurs licences. **L'utilisation à distance ne constitue pas une réception du
logiciel**!

Le souci est que si le service SaaS en question utilise du logiciel libre,
l'utilisateur n'est plus en mesure d'exercer son droit car il ne **reçoit**
plus une copie du logiciel. De nouvelles formes de licences ont donc été
crées afin de remédier (partiellement) àce problème: les licences
déclenchées par **l'utilisation**.

> AGPL v1.0 d'Affero, reprise par la FSF en v3.0

<div class="alert alert-info">
Dès le moment que l'on intéragit avec une licence AGPL, on bénéficie des termes
de la licence. C'est-à-dire que l'on doit <strong>recevoir un exemplaire du code
source</strong>.
</div>

Pour autant, le déclenchement par l'usage ne résout pas réellement les problèmes
posés par le passage à un environnement SaaS. [...!]

# Multi-licenciage

L'ayant-droit d'un logiciel peut choisir de diffuser celui-ci avec le type de
licence de son choix. Il peut même diffuser le même logiciel, par plusieurs
canaux différents, avec des licences différentes. On parle de *dual-licensing*.

