---
title: "Cas Pratiques"
date: 2019-10-14T09:01:00+02:00
clsDate: 2019-10-14
draft: false
---

# Cas pratiques

## Libre téléchargement

Logiciel dont le marché est très large, constitué d'entités non concurrentes ou
dont ce n'est pas le cœur de métier (par exemple: la bureautique. Tout le monde
en a besoin sans être des concurrents). C'est le cas des bibliothèques et
logiciels de service.

Le but est réellement que la communauté du logiciel s'élargisse, on choisira
donc les licences libres **persistantes** ou **diffusives**.

Lorque le marché est suffisamment large, des acteurs ont un large intérêt
stratégique à avoir un logiciel performant. Ces mêmes acteurs vont donc financer
la fondation qui va améliorer le logiciel et ainsi profiter à tous.

## Consortium fermé

Logiciel métier dont le marché est étroit (par exemple: logiciel de
banque).  Deux banquiers non concurrents vont mutualiser la production de
leur logiciel.  Il y a une **mutualisation** de  développement entre les
membres du consortium.

- Titularité des droits au prorata des apports
- Totale liberté d'associer de nouveaux membres à des évolutions futures du
travail réalisé en commun
- Aucun désir de donner gratuitement aux concurrents
  - Libre par forcément gratuit!

## Partenariat privilégié

Logiciel ou bibliothèque métier potentiellement utilisable par une communauté
plus large. On va choisir une distribution double sous licences libres:

- Fourniture au partenaire sous licence évanescente pour lui permettre
d'inclure du logiciel au sein de produits dont les caractéristiques sont
cachées aux concurrents.
- Mise en libre accès sous licence diffusive pour contributions de la communauté
et la réalisation éventuelle de logiciels analogues mais au code source
accessible à tous.

Imaginons le cas où un industriel possède le logiciel sous licence BSD. A côté
de ça, le logiciel version GPL a intégré des modifications de la part de la
communauté. Maintenant, l'industriel réclame les modifications sous sa version
BSD. Ça reste impossible car les contributeurs sont les titulaires des droits de
ces modifications.

<div class="alert alert-info">
Les deux branches vivent leur vie indépendemment.
</div>

<div class="alert alert-info">
Souvent, les auteurs de logiciels libres vont se regrouper en **fondation** ou
**consortium** afin de mutualiser les ressources qui vont permettre de les
défendre en cas de litige.
</div>

# Pourquoi développer sous licence libre?

Ce sont des outils idéaux pour la préservation du patrimoine intellectuel. Le
coût est nul.

- L'ajout des mentions de licences dans chaque fichier source est suffisant.
- Le dépôt à des organismes de type APP (pour un coût dérisoire) apporte une
preuve d'intériorité.
- Mutualisation de l'effort de développement
- Meilleure pérennité pour les clients (devient un argument commercial)
