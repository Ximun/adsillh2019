---
title: "Compilation Et Decompilation"
date: 2019-10-18T08:20:15+02:00
draft: false
---

# Compilation et Décompilation

Aujourd'hui, très peu de logiciels sont écrits en langage machine. On
utilise des langages de plus haut niveau qui sera traduit en langage machine
par un compilateur.

- Le *code source* est le programme écrit en langage de haut niveau
- Le *code objet* est le programme traduit du code source dans un langage
de bas niveau

La question se pose de savoir s'il est possible de faire le chemin inverse
et de déduire un code source à partir d'un code objet. C'est ce qu'on
appelle la **décompilation**.

Le soucis est que le langage machine est extrêmement bas niveau et que
des instructions simples en langage de haut niveau vont laisser place à
de nombreuses instructions répétitives et très coûteuses en énergie
humaine à décompiler.

<center>
{{< mermaid >}}
graph LR
  CodeSource --> Compilateur
  Compilateur --> CodeObjet
{{< /mermaid >}}
</center>

Le **Code Objet** est une *œuvre dérivée* du **Code Source**. C'est
pour cela que ce sont les ayants droits du code source qui décident de la
diffusion du code objet. (Et que Microsoft peut faire payer son exécutable
et le diffuser sous licence privative).

De plus, le compilateur est une œuvre en soit, mais le code traduit par
lui ne lui appartient pas. Il n'est pas une *œuvre originale* dont l'auteur
serait celui du compilateur.

> Cependant, dans un système d'exploitation, du code est ajouté au
début et à la fin d'un programme (notamment pour charger les variables
d'environnement par exemple) au moment de la compilation. Dans la pratique,
l'auteur du compilateur déclare renoncer à ses droits pour ces parties.

## Interdiction de la décompilation

Compiler et décompiler créent des *œuvres dérivées* de l'œuvre originale.
Ces copies ne peuvent donc être exploitées qu'avec l'accord de l'ayant
droit des œuvres originales.  La décompilation est donc **interdite**,
sauf si permission explicitement donnée par l'ayant-droit.

## Modèle économique de la création logicielle

La vision promue par la Commission Européenne est
parfaitement décrite dans les considérants de la directive
[91/250/CE](https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=C606EEF08CE5CF44A0C307DD8975B7C9.tplgfr35s_3?cidTexte=JORFTEXT000000713568&categorieLien=id):

Le logiciel doit figurer dans le secteur industriel des **biens
substituables**, et doit respecter la directive de **concurrence libre et
non faussée**.

{{< columns >}}

### Biens substituables

Deux biens sont dits substituables si leur utilisation ou acquisition permet
de satisfaire le même besoin (thé ou café). Comme il est très rare que
deux biens soient parfaitement substituables, on dira souvent que des biens
sont « substituables » au sens large du terme, c'est-à-dire imparfaitement
substituables : ils ont des caractéristiques communes et procurent un niveau
de satisfaction équivalent.

[référence](https://www.pimido.com/sciences-politiques-economiques-administratives/economie-generale/fiche/biens-substituables-biens-complementaires-396863.html)

<--->

### Concurrence libre et non faussée

Le droit communautaire ne fournit pas de définition de la concurrence libre
et non faussée. Toutefois, celle-ci est essentielle pour la réalisation du
marché intérieur dans lequel les entreprises doivent pouvoir se concurrencer
à conditions égales dans tous les États membres. La concurrence soutient la
réussite économique, à la fois en protégeant au mieux les intérêts des
consommateurs européens et en assurant la compétitivité des entreprises,
des produits et des services de l'Europe sur le marché mondial.

[référence](https://www.atelier-europe.eu/blog/2006/06/qu-est-ce-que-la-concurrence-libre-et-non-faussee.html)

{{< /columns >}}

L'adaptation des droits patrimoniaux vise donc à adapter ces modalités
d'exploitation économique aux spécificités de ce type d'œuvres.

<center>
{{< mermaid >}}
graph LR
  subgraph Concepteur
  Idée --> conc[Conception]
  conc[Conception] --> Logiciel
  style conc fill:#f66
  Logiciel --> Production
  end
 
  Production -.-> id[Idée]
 
  subgraph Concurrent
  id[Idée] --> id2[Conception]
  style id padding:30px;
  id2[Conception] --> id3[Logiciel]
  style id2 fill:#f66
  Logiciel ==>|impossible de bypass| id3
  id3[Logiciel] --> id4[Production]
  end
{{< /mermaid >}}
</center>

Il a été considéré que la **conception** d'un logiciel, la mise en œuvre
qui découle de l'idée est le plus long et le plus difficiel à réaliser,
et constitue la vraie valeur du logiciel. Donc une nouvelle idée venant
à un concurrent, qu'il aurait eue à partir du logiciel réalisé par le
premier, doit repartir au stade de l'idée et repasser par toutes les étapes
de conception afin de qu'une certaine *temporalité* soit commune aux deux
concurrents. Simplement décompiler le code objet du premier avantagerait
énormément le concurrent.

Le législateur permet donc qu'il y ait concurrence sur le marché du
logiciel, mais oblige les acteurs à passer par les **mêmes coûts** afin
de permettre à l'auteur du premier logiciel d'améliorer son œuvre avant
que des concurrents reprennent son idée.

Tout "raccourci" est interdit, c'est donc pourquoi il est interdit de
réutiliser des morceaux de logiciels directement par un concurrent
(décompilation).

## Formats de données

Se pose également la nécessité de traiter le problème des [**marchés
captifs**](https://fr.wikipedia.org/wiki/March%C3%A9_captif) qu'induisent
les **formats de données**.

En effet, dans le cas où un logiciel B concurrent d'un logiciel A arrive
sur le marché:

On peut imaginer que le logiciel A stocke les données de ses utilisateurs
dans un format de données fermé. Même si un client de A est attiré par
ce nouveau logiciel B, ce dernier fonctionne malheureusement avec un autre
format de données incompatible avec celui du logiciel A. Ce client va se
retrouver *captif* du logiciel A, à cause de la valeur qu'il a placée dans
le *coffre-fort* de ses données, fermé.

C'est pourquoi [**l'interopérabilité**](/cours/droit/interoperabilite)
est encouragée.
