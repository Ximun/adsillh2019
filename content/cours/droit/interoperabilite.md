---
title: "Interoperabilite"
date: 2019-10-18T08:58:32+02:00
draft: false
---

**Formats de données**

Au delà du matériel et du logiciel, le patrimoine immatériel d'une entité
est constitué de ses **données**.

- Représentent un coût de création et de collecte
- Sont parfois irremplaçable

On se rend compte que la vraie valeur est dans **les données**, et se pose
la question des **formats de données**.

La dématérialisation des données conditionne leur accès à l'utilisation de
logiciels manipulant le format sous lequel ces données sont stockées. Les
formats de données **fermés** sont le moyen de conserver captive une
clientèle.

La pérennité des données est tout autant conditionnée par l'existence
des logiciels que des supports et des matériels de lecture (plus le support
est ancien, plus le coût est élevé).

# Interopérabilité

<div class="alert alert-info">
L'interopérabilité est la capacité pour deux systèmes informatiques ou logiciels
quelconques d'intéragir ou de s'échanger des données.
</div>

## Compatibilité ≠ Interopérabilité

La notion de **compatibilité** est beaucoup moins forte que celle
d'**interopérabilité**.

Deux produits sont compatibles quand ils peuvent fonctionner ensemble
(accord contractuel entre les parties). Il s'agit d'un acte économique
qui permet d'encloisoner plus de personnes dans un modèle et d'étendre
ainsi un monopole.

Cependant, il y a interopérabilité quand deux produits peuvent fonctionner
ensemble **lorsque l'on sait pourquoi**, c'est-à-dire lorsque leur moyen de communnication
est normé et connu. Cela permet de créer un espace de discussion
autour d'un format de données public, qui va permettre à un nouvel arrivant
d'y entrer sans problème.

### Différence entre "standard" et "norme"

Un standard est établit par deux entreprises entre elles (ou plus). La
**norme** vient en plus pour "fixer" un standard entre tous les acteurs,
qui devront s'y soustraire. Un organisme, censé représenter l'intérêt
général, organise la discution entres divers acteurs afin d'établir
une **norme**. Un standard gagnant la bataille avec ses "concurrents" peut
également devenir un norme.

En Union Européenne, on a vu que la décompilation est interdite mais il est
indiqué qu'il est possible d'aller regarder dans un logiciel pour comprendre
comment il est fait, **dans un objectif d'interopérabilité**. Ceci est
encadré par trois conditions sctrictes:

- Avoir une licence dans le logiciel étudié
- Ces informations ne doivent pas être disponibles ailleurs
- Accès limité aux parties du logiciel nécessaires

Les informations obtenues ne peuvent pas être utilisées à d'autres fins
et ainsi "porter atteinte au droit d'auteur". Il faut également documenter
le processus **afin de ne pas être accusé de contrefaçon**. Ça prouve
également que l'on a étudié et utilisé uniquement la partie autorisée.

> Questions:
> 
> - Décompilation interdite mais OK pour interopérabilité? Écrit tel
quel dans la loi?
> - Qu'en est-il du projet GNU? Décompilation d'UNIX?

# Formats ouverts

L'interopérabilité doit reposer sur des formats ouverts (Sinon elle est
difficile à obtenir). Il est nécessaire de faire la distinction entre
**format ouvert** et **format libre** au niveau de la *gouvernance*,

{{< columns >}}

**Format Ouvert**

exemple: PDF est un format ouvert, mais seul Adobe décide de ses standards.

<--->

**Format Libre**

L'idée d'un format libre est d'ouvrir un forum autour de sa conception.

{{< /columns >}}
