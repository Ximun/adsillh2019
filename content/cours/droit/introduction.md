---
title: "Introduction"
date: 2019-09-17T08:18:38+02:00
clsdate: 2019-09-17
draft: false
---

# Économie des biens immatériels et Économie des biens matériels

L'économie des biens immatériels diffère fondamentalement de celle des biens matériels.
Les biens immatériels sont dits **non rivaux**

- Pas de rivalité pour en consommer
- On peut les partager sans s'appauvrir
- La notion de "vol" n'est pas pertinente car **il n'y a pas de propriétaire**.

Les propriétés juridiques des biens matériels **ne s'appliquent pas** dans le monde
des biens immatériels, même si certaines terminologies comme **propriété intellectuelle**
le laissent penser.

Le coût d'une copie d'une unité diffère entre les deux économies:

- Bien matériel: Voiture supplémentaire -> coût matériel de la création d'une voiture
- Bien immatériel: copie à coût marginal nul (grâce à Internet)
- Un bien numérique peut être distribué gratuitement dès le moment où son développement
a été financé

## L'effet de réseau

La valeur d'un produit augmente avec le nombre de personnes qui l'utilisent (exemple: le smartphone).
En terme de valeur, les créateurs d'écosystème vont attacher beaucoup d'importance à l'entretien
de leur communauté pour valoriser cet effet de réseau.

Contrairement au modèle traditionnel de l'entreprise, à savoir créer une valeur et la monétiser,
le modèle de création de valeur de l'économie des biens immatériels va être de créer de la micro-valeur
ajoutée par le biais des utilisateurs eux-mêmes (ex: Wikipédia).

Ce modèles économique libre reste tout de même compétitif et ultra libéral. Il induit
une concurrence rude pour l'effet de réseau, à savoir une croissance ultra rapide pour devenir référent
pour les utilisateurs.

Cet effet de réseau concerne les utilisateurs du logiciel, mais également les employés d'une entreprise
de l'économie des biens immatériels. Par exemple, une grosse boîte rachète une petite boîte qui fonctionne,
mais un choc des cultures fait que tous les travailleurs qui ont participé à la croissance de cette
petite boîte vont préférer quitter la grosse et en reformer une à leur image.

## Valeur des logiciels

### Tout logiciel à un coût

Le coût de ce logiciel concerne tous les moyens mis en œuvre pour le produire.

### Tout logiciel a une valeur

- Valeur d'usage
	- Découle du service qu'il rend
- Valeur intrinsèque
	- Expertise contenue au sein du code source
	- Maintenabilité, extensibilité, réutilisabilité
- Très difficile à quantifier
	- Décorrélé du coût de production
- Un logiciel non utilisé est un logiciel qui meurt

# Apparition du logiciel

L'informatique est la science du traitement **efficace** de l'information.

Les premiers calculateurs ne possédaient pas de lociciel (ils exécutaient juste des calculs).
Alan Turing a conceptualisé ce que pouvait être un ordinateur ([machine de Turing](https://fr.wikipedia.org/wiki/Machine_de_Turing)).
La machine n'est pas efficace, mais donne l'idée de ce que pouvait être un ordinateur

## L'architecure "de von Neumann"

L'architecture ["de von Neumann"](https://fr.wikipedia.org/wiki/Architecture_de_von_Neumann)
dans les années 40, dit que l'ordinateur traite de l'information donc nécessite un **processeur**
et de la **mémoire**, et que les **programmes** (ce qui pilote les données) qui doivent les traiter
peuvent être stockée dans la même mémoire que celle des données.

Le premier ordinateur est daté de 1947 à l'Université de Manchester.

Le logiciel devient alors une donnée (presque) comme une autre. Apparaît donc des
programmes *auto-modifiables*.

## Apparition du droit du logiciel

Dans les années 60-70, les ordinateurs valaient extrêmement cher mais les logiciels
et la doc venaient en "pack" gratuits fournis avec la machine.

Des clubs d'utilisateurs apparaissent servant à l'échange de bugs, d'amélioration des
logiciels. Il y avait une mutualisation et donc une réduction des coûts de maintenance
logicielle, offrant un avantage compétitif sur les concurrents.

Ces principes offraient déjà un modèle qui fait penser à celui des logiciels libres actuels,
jusqu'à ce qu'apparaissent les premiers logiciels privateurs...

Les premiers concurrents des gros constructeurs tel qu'IBM, vers la fin des années 60,
vont s'attacher à ce que leurs logiciels soient compatibles avec les programmes IBMr,
comme argument de vente principal, le tout pour des machines
moins chères. Cependant, IBM rétorque que si les coûts sont moins chers,c'est
justement parceque le coût des ingénieurs qui ont conçu les logiciels n'est pas compris.

Donc IBM commence à faire payer d'une part la machine, et d'autre part les logiciels.

Apparaît à ce moment la question de savoir "Quel statut juridique s'applique au logiciel?"

### Un choix à faire

Le législateur se demande à ce moment de quel droit existant il va pouvoir s'inspire,
et trois choix s'offrent à lui...

- Droit des brevets
	- Jugé inadapté de par l'objet à réguler et la lourdeur des mécanismes de dépôt et d'entretien
- Droit "sui generis"
	- Taillé sur mesure
	- Mais les droits diffèrent selon les pays.
- Droit d'auteur
	- Processus de création similaire  entre logiciels et oeuvres littéraires
	- Existence de la Convention de Berne (1886) -> protection internationale automatique et immédiate

### Choix du droit d'auteur

C'est donc le **droit d'auteur**, par l'impulsion d'IBM qui fut choisi comme véhicule du droit
du logiciel pour plusieurs raisons, et notamment car il existe déjà un accord international.

- 1980 aux États-Unis (modification du Copyright Act)
- 1985 en France (Loi du 3 juillet 1985)
- 1991 au sein de l'Union européenne (Directuve 91/250/CE)
- 1994 parmi les membres de l'OMC (Accords ADPIC)
- 1996 au niveau mondial (Traité de WCT de l'OMPI)

En 16 ans, on arrive à un accord mondial comme reconnaissance du modèle du droit d'auteur
applicable au logiciel.
