---
title: "Donnees a Caractere Personnel"
date: 2019-10-22T08:16:39+02:00
draft: false
---

# Données à caractère personnel

## Identité

Chacun de nous présente plusieurs apparences de lui-même selon les
circonstances. (Famille, Travail,...). L'identité, c'est la *manière*
dont une entité est reliée à ses apparences. L'identité est donc le
lien créé, au moyen des technologies numériques, entre une personne et
ses très diverses apparences.

Cette identité est perçue à travers trois ensembles de données:

- Ce que l'on déclare de soi (infos fournie à l'inscription d'un site)
- Ce que l'on montre de soi (ses actions après l'inscriptions,
discussions,...)
- Ce que l'on peut connaître (traces que l'on laisse)

Ces dernières traces sont inhérentes au monde du numérique et peuvent
raconter énormément de choses.

La **collecte** quant à elle, existe depuis l'invention de l'administration,
lorsque la société, la cité s'est organisée (La majorité des découvertes
archélogiques en écriture sont administratives). Ces données ont déjà
pu être utilisées contre les personnes (massacre de la Saint-Barthélémy).

Le danger s'est accéléré avec:

- La numérotation des personnes (corrolaire à la révolution industrielle)
- La mécanographie (le fait de traiter l'information automatiquement et
mécaniquement, via le recensement par carte perforée mis au point par
l'ancêtre IBM)

Ce fichage s'est développé en France au XIXème siècle avec le [livret
ouvrier](https://fr.wikipedia.org/wiki/Livret_d%27ouvrier).
Avant l'exode rural de la révoltion industrielle, les gens passaient leur vie entière dans un rayon maximum de 10km. La notion de *vie privée* n'existait pas.
Mais sont arrivées avec la révoution industrielle de nouvelles populations inco
nnues dans des zones industrialisées. Il a fallu créer des réseaux de confiances (les papiers
d'identité) afin d'authentifier les personnes lorsqu'il arrivaient dans une
nouvelle ville.
Les papiers d'identité ont été conçus pour un contrôle social de ces
populations, en France avec la population algérienne.

> Voir le musée vituel de la police: [crimimnocorpus](https://criminocorpus.org/fr/)
"Bertillonage".

En arrivant à noter ces informations sur les cartes que des machines arrivent à
traiter, on peut facilement exercer un contrôle sur une population. L'exemple tragique de la
seconde guerre mondiale aux Pays-Bas l'illustre bien: l'administration
néerlandaise avait fiché sur carte professionnelle toute la population.
Mais un fichier utilisé pour une raison peut l'être pour une autre, et à
l'arrivée des nazis, ces derniers ont sélectionné et déporté une énorme
quantité de juifs extrêmement facilement.
En France, il y avait eu à l'époque des projets similaires,
mais un rejet de la population avait préservé le pays d'un tel projet.

> Et aujourd'hui, l'informatique arrive à faire des **croisements**, et permet des
choses bien bien plus poussées concernant l'identitification...

Lorsque l'informatique arrive dans les années 70, des craintes naissent en
France pour ces mêmes raisons.

En effet à l'époque, il n'y avait que les États qui pouvaient financer les ordinateurs. Mais avec la
démocratisation de l'informatique, les entreprises sont désormais capables d'être en
contact avec la totalité de la population mondiale connectée (et de façon
beaucoup plus intrusives avec notamment les réseaux sociaux par exemple).

A côté, on assiste à un retour en force des États, qui imposent d'accéder plus ou moins secrètement à
ces gisements de données (Edward Snowden). On voit revenir la menace des États comme étant
la plus prégnante. ("Gag orders", [Cloud
Act](https://fr.wikipedia.org/wiki/CLOUD_Act)).

En réaction, plusieurs lois ont été créées, dont la loi "informatique et
libertés" de 1978 en France. On cherche alors à créer une entité qui
protège les gens contre de potentielles dérives étatiques concernant
le fichage que rend possible les nouveaux moyens de traitement de
l'information. Il fallait des organes de contrôle indépendants de l'exécutif
et des admministrations, on crée donc pour l'occasion un modèle juridique
original: Les **autorités administratives indépendantes**, et c'est donc la
CNIL, (qui est composée aujourd'hui de 18 commissaires et 220 employés)
qui est chargée de réguler les acteurs du pays selon la **Loi Informatique
et Libertés** de 1978.

## Loi Informatique et Libertés

### art 1

> L'informatique doit être au service de chaque citoyen. Son développement
doit s'opérer dans le cadre de la coopération internationale. Elle ne
doit porter atteinte ni à l'identité humaine, ni aux droits de l'homme,
ni à la vie privée, ni aux libertés individuelles ou publiques.

En Europe, cette recherche de protection est forte, contrairement aux Etats-Unis
et Canada qui n'ont pas connu d'épuration ethnique sur leur territoire.

### Convention n°108 du Conseil de l'Europe du 28 janvier 1981

47 états membres - Directive 95/46/CE (commencement d'harmonisation, qui dit que
tout pays entrant dans l'UE doit avoir une CNIL indépendante du pouvoir)

### Règlement 2016/679 (RGPD), entrée en vigueur le 25/05/2018

- Application immédiate et uniforme au niveau européen (contrairement à une
*directive* qui impose une intégration d'une règle à l'échelle nationale) qui
permet d'uniformiser la loi à tous les pays memebres.
- Mais il subsiste encore quelques interprétations laissées aux États membres.
(Comme par exemple l'âge de la majorité numérique)

### Missions d'une autorité comme la CNIL

#### Autorisation

- Étude de projets de traitements soumis à autorisation
- Élaboration de doctrines relatives aux différents types de traitements

#### Contrôle

- Contrôles en ligne depuis 2014 (avant, il fallait se rendre sur place)
- Mise en demeure qui peut être publique.

#### Sanction

- Tribunal administratif spécialisé au sein de la CNIL en matière de données personnelles
(de première instance -> Conseil d'état si appel)

#### Conseil

- Tout le monde peut demander de l'aide et des informations à la CNIL.

## Champ d'application

Ne concerne uniquement les personnes physiques. La loi I&L ne s'applique pas aux
entreprises. Dans la première version de la loi, on parlait d'information
**nominative**, car c'est comme cela qu'avant on identifiait les gens. Avec la
numérisation, les numéros relatifs à une personne sont beaucoup plus nombreux
(numéro de téléphone, plaque minéralogique, addresse IP,...). Avec la puissance
de traitement informatique, on se rend compte que ces données nominatives ne
sont plus les seules à identifier un individu, on parle donc de **dponnées à
caractère peronnelle**.

Voir absolument l'article 4 du RGPD qui définit les choses et est très
synthétique.

On s'attache moins au *type de données* que *qui elle concerne*.

Consommation éléectrique: possibilité de savoir exactement quels sont les
appareils, les habitudes de vie des personnes d'un foyer, niveau de richesse...
Sur un échantillongage de 2 secondes, il est possible de savoir quel film
regarde quelqu'un (car avec les nouveaux téléviseurs, la consommation électrique
dépend de la luminosité). C'est pour ça que la CNIL a indiqué au compteur Linky
un pas d'échantillonage de 10000.

### Données sensibles

Le RGPD déclare les données sensibles, et interdit son traitement (voir article
9du RGPD)

Ces questions ne sont pas simple (voir dans le médical comment des données
ethnologiques concernant des enzymes qui peuvent être utiles pour dsavoir si
onpeut utiliser un médicament ou non).

Ces données peuvent être collectés de fait, lorsqu'on s'inscrit sur un parti
politique, un site de rencontre homosexeul etc. mais on s'attend à ce qu'elles
ne soient pas utilisées contre la personne

### Critère de licéité

Appréciée selon les critères suivants:

- Finalité: **déterminée** (savoir pourquoi elles sont collectées),
**explicite** (je dois le savoir, je dois vous le dire), **légitime** (pas pour
tuer des gens par exemple) (voir les 6 bases légales de CNIL)
- Responsable du traitement (qui est responsable de ce traitement? il faut q'il
soit renseigné)
- Destinataires des données traitées (ex: centre du traitement des amendes pour
les radars, sous traitants pour Hadopi)
- Durée de conservation (toujours conservées pour une durée *limitée*,
nécessairement fixée même si pas absolues)
- Mesures de décurité de conservation (pas mal de sanctions de la CNIL pour
manquement de sécurisation des données personnelles, même s'il y a différence
entre obligation de résultat et obligation de moyen, sachant que dans
l'informatique l'obligation de résultat est impossible. En droit, différence
entre l'erreur et la faute)
- Exercice des droits des personnes (information, droit d'accès, de
rectification, d'opposition dans certains cas (on peut se rétracter par
exemple), etc.)

## Base légale (article 6)

Tout traitement doit avoir une base légale.

- Consentement
- Nécessité pour l'exécution d'un contrat
- Respect d'une obligation l"gale
- Sauvegardes des intéreêts vitaux de la personne
- Mission d'intérêt public ou relevant de l'autorité publique dont est investi
le responsable
- Intérêts légitimes poursuivis par le responsable de tratitement ou par un
tiers (A mois que ne prévale l'intérêt légitime des personnes) -> par exemple
lecture de mail pour les classer comme SPAM, sans être forcément en relation
contractuelle avec eux. La base légale devient celle de l'intérêt légitime.

## Anonymisation et réidentification

L'anonymisation des données est un sujet critique à l'ère du néuérique. Exemple:
un maire veut savoir comment les gens se déplacent dans une ville. Il contacte
un opérateur réseau pour connaître le trajet borné de ses utilisateurs, de façon
anonymisé mais toutes les infos. Seulement il ne s'agit d'infos anonmysés....

Imaginons qu'on a toutes les infos anonymisés, il suffit de recouper ces données
en open data avec des posts twitter par exemple, et on peut identifier un
spaghetti une personne avec 4 "punaises" avec une certitude supérieure à 95%. On
appelle  ceci une attaque par corrélation.

L'anonymisation est donc extrêmement difficile à obtenir.

On va essayer pour cela de "découper" la donnée: du plat de spaghettis qui
définit tout le trajet, on passe à de "la semoule" qui donne une information sur
le nombre de personnes à un moment donné, mais qui rend impossible de la relier
avec un trajet dans sa totalité.

# Évolutions du fait du RGPD

## Principales dispositions du RGPD

- Passage d'un régime d'autorisation à un régime répressif (contrôles à
posteriori), sauf dans des domaines tels que la santé, où l'autorisation
demeure. On compte sur des remontées de la part d'utilisateurs à qui il
semblerait qu'il y ait des problèmes.
- Le cadre du RGPD est harmonisé au niveau de l'Union européenne.
- Renforcement du consentement, il doit être **explicite** de la part de la
personne.
- Droit à la portabilité des données personnelles: dès le moment où un organisme
traite les données presonnelles de quelqu'un, il doit prévoir un moyen possible
pour l'utilisateur de récupérer ses données de façon lisible. Il permet à la
fois aux gens de récupérer leurs données, mais il oblige l'organisme à rendre
les données traitables par un éventuel concurrent.
- Protection intégrée de la vie privée: Obligation de minimisation des données
(que celles qui sont nécessaires, de la façon qu'il est strictement nécessaire)
(terme de "vie privée" pas forcément adéquat car la vie privée ne concerne pas
forcément les données. Mais vient du terme nord-américain "privacy by design")
- Obligation de notification en cas de violation: en cas de violation,
obligation du responsable de traitement d'informer l'autorité et les concernés
- Délégué à la protection des données: une personne fonctionnellement
indépendante de la hiérarchie, qui doit avoir les moyens que les traitements de
données se passe bien.

## Obligations du responsable de traitements

- Tenir un registre des traitements: doit décrire de façon suffisamment
détaillée la façon dont sont traitées les données (par exemple: la RH a le
numéro de sécu, etc.) c'est un bon moment pour se demander si les données traitées
sont vraiment utiles.
- Réaliser une analyse d'impact, AIPD (la CNIL a créé un logiciel libre: PIA,
pour aider à réaliser une analyse d'impact)
- Encadrement des transferts hors de l'UE: on ne peut pas envoyer les données
des gens dans des espaces politiques où les données personnelles ne sont pas
respectés (chercher: transfert des données à caractère personnel, adéquation)
- Information des personnes
- Procédures en cas de violations de données: internes ou externes si pas
suffisant

# Gestion de la biométrie

Les données biométriques sont extrêmement sensibles: non révocables car
intimemnt associées aux personnes.

Deux principaux usages:

- L'authentification: pouvoir assurer qu'une personne est bien la bonne (base
centrale non nécessaire)
- L'identification: retrouver une personne dans une base de données à partir de
ses traces (ne doit pas être possible hors fichier de police)
