---
title: "Droits Patrimoniaux"
date: 2019-09-25T15:07:28+02:00
clsdate: ["2019-09-25", "2019-09-27"]
draft: false
---

# Droits patrimoniaux

C'est la matérialisation du droit qu'a l'auteur de tirer profit de son
œuvre. Ces droits sont distincts de la possession physique de l'œuvre. en
effet, on n'achète que la "matière" du tableau, pas ce qu'il représente. Il
est possible d'accrocher un tableau acheté dans son salon, mais il est
impossible de le prendre en photo et de le diffuser.

Ces droits patrimoniaux ont deux caractéristiques. Ils sont:

- Temporaraires
- Cessibles

## Durée des droits patrimoniaux

La durée des droits patrimoniaux dépend des pays et des situations. Cependant
la pression de l'industrie du diverstissement tente d'uniformiser ces droits
mondialement. Actuellement, au sein de l'Union européenne, cette durée
est de 70 ans après le décès de l'auteur.

Il existe certaines abérations. Par exemple, la France a décidé qu'en temps
de guerre, les gens lisaient moins donc la durée des droits patrimoniaux
n'étaient pas décompté en temps de guerre. Zncore mieux, si un auteur
meurt en tempos de guerre, la loi concidère qu'il est mort trop tôt à
cause de la guerre et rajoute 30 ans à la durée de droits patrimoniaux.

> Voir exceptions au droit d'auteur,
> Œuvres collectives,

(...)
