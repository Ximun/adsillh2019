---
title: "Droit Dauteur Adapte Au Logiciel"
date: 2019-10-04T08:19:01+02:00
clsDate: ["2019-10-04", "2019-10-07"]
draft: false
---

# Droit d'Auteur adapté au logiciel

Par son rattachement au droit d'auteur, le logiciel est assimilé à une
œuvre de l'esprit. Le logiciel n'a pas de vocation artistique, notamment en
ce qui concerne sa conception. On s'attachera au service rendu par le logiciel.

On parlera de "droit d'auteur adapté", car nous ne sommes pas dans le même
modèle économique que celui des œuvres d'art.

## Modifications effectuées lors de l'entrée du logiciel dans le Droit
d'Auteur

### Pour les auteurs salariés

#### Actes nécessaires à l'utilisation du logiciel

Peuvent être mises en œuvre toutes les copies nécessaires au bon
fonctionnement du logiciel, par une personne en droit de l'utiliser (copies
dans les différentes parties d'un ordinateur par exemple)

#### Observation du fonctionnement

Tout le monde a l'autorisation de regarder comment fonctionne le logiciel
(du dehors), de le tester dans les moindres recoins.

#### Copies de sauvegarde

Un usager qui dispose du droit de l'utiliser, peut mettre en place une
sauvegarde du logiciel, **si aucun autre moyen de le faire n'est fourni
par l'éditeur.**

#### Décompilation

La rétro-ingénierie du logiciel est **interdite**, sauf à fin
d'interopérabilité.

#### Suppression de l'exception de copie privée

Contrairement aux œuvres d'art, les usagers nécessitent une licence pour
chaque exemplaire du logiciel.

#### Transfert automatique de la titularité des droits patrimoniaux à
l'employeur

Le monde du logiciel étant reconnu comme industrie, les **ayants-droits**
du code développé par les développeurs d'une entreprise sont les
employeurs. Cependant, les **auteurs** restent les employés. Ce sont les
employeurs qui décident de la divulgation du logiciel, du choix de la
licence, etc.

<center>ayant droit =/= auteur</center>

#### Apparition d'un statut d'"auteur prolétaire"

L'employé loue son "esprit" à fin professionnelle, même si le terme de
*prolétaire* s'apparente aux travailleurs qui louent leurs corps.

<div class="alert alert-info">Toutes ces règles sont effectives dans le
cadre du travail de l'employeur, sur le matériel de l'employeur et sur le
temps de travail de l'employé.</div>

#### Droit au respect de l'œuvre

L'auteur salarié ne peut s'opposer à la modification de l'œuvre.

### Les auteurs non salariés

Comprendre les sous-traitants, les frelances, stagiaires,...

S'ils ne sont pas salariés, **le transfert automatique de la titularité
des droits patrimoniaux ne s'exerce pas**, donc les auteurs non rémunérés
gardent la titularité des droits sur leur œuvre.

## Ajout de nouvelles exceptions

- Actes nécessaires pour permettre l'utilisation du logiciel - Observation
du fonctionnement - ...

# Portée de la protetion

<div class="alert alert-danger"> Est couvert par le droit d'auteur adapté
au logiciel tout ce qui relève de l'expression d'algorithmes mathématiques.
</div>

Comprend le code source et les codes objets ainsi que le matériel de
conception préparatoire (expression de ce que logiciel doit faire, etc.)

### Documentation

Elle rentre dans le droit adapté en bénéficiant d'un statut hybride. Les
aspects "originaux" (blagues etc.) sont couverts par le droit d'auteur
classique, alors que les aspects jugés non originaux sont couverts par le
droit d'auteur adapté.

### Éléments graphiques et sonores

Couverts par le droit d'auteur classique.

> Décision "Cryo"

## Quel statut pour les langages?

Le langage est le moyen d'écrire l'œouvre. Il est donc à un niveau
d'abstraction supérieur. Un langage informatique est également une
communication. Il ne peut être revendiqué et monopolisé.

<div class="alert alert-info">Le compilateur, l'interpréteur relève du droit
d'auteur adapté mais la syntaxe, le langage appartient à tout le monde.</div>

Un format de fichiers définit la grammaire d'un langage. Accorder des brevets
sur des formats de fichiers reviendrait donc à pouvoir monopoliser un langage,
donc aussi par extension des éléments de langues humaines.

C'est pour la même raison que lorsque Microsoft rend secret un format de
fichier ```xls``` par exemple, il ne peut se retourner contre LibreOffice
qui par rétro-ingeniering a mis en place un autre format de fichier ouvert
qui comprend le système de fichiers de Microsoft.
