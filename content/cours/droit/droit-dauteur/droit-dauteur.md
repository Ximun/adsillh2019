---
title: "Droit Dauteur"
date: 2019-09-17T12:48:12+02:00
clsdate: ["2019-09-17", "2019-09-24"]
draft: false
---

# Droit d'auteur

En France, le droit d'auteur fait partie du **Code de Propriété Intellectuelle** (CPI)

on note l'oxymore Propriété <> Intellectuelle

## Organisation Hiérarchique du CPI

{{< mermaid >}}

graph TD
	cpi[Code de la Propriété Intellectuelle]-->cpla[Code de la Propriété Littéraire et Artistique]
	cpi-->cpind[Code de la Propriété Industrielle]
	cpla-->da[Droit d'auteur]
	cpla-->dv[Droits 'voisins des droits de l'auteur'<br>interprètes]
	cpind-->db[Droit des brevets]
	cpind-->dm[Droit des marques]
	da-->dp[Droits patrimoniaux]
	da-->dep[Droits extra-patrimoniaux<br><em>- Attachés à la personne de l'auteur ou ses descendants</em><br><em>- N'existent pas dans le système du copyright</em>]

{{< /mermaid >}}

## Justification du Droit d'Auteur

Un droit bride une liberté. Quand il construit un droit, le législateur doit faire
son maximum afin que la balance entre les intérêts de tous les partis soit équilibrée.

Le droit d'auteur est censé enourager globalement la création en garantissant aux créateurs
un monopole **temporaire** sur l'exploitation de leurs créations. Au terme de ce monopole, les
créations gagnent le domaine public, et peuvent ainsi bénificier à toutes et tous.

Le choix des mots est important: une œuvre **s'élève** dans le domaine public et n'y **tombe**
pas comme on l'entend souvent...

[Discours d'ouverture du Congrès littéraire international par Victor Hugo](https://fr.wikisource.org/wiki/Discours_d'ouverture_du_Congrès_littéraire_international)

L'intérêt général, c'est le point d'équilibre qui permet aux individus de jouïr de ces œuvres
tout en respectant ses auteurs.

## Œuvre

L'œuvre une **création de forme**. C'est uniquement la forme qui sera protégée (sculpture, peinture,
le texte d'un roman...) et **non pas les idées et les concepts**. Car ces-derniers sont reconnus
comme étant de libre parcours. Tout le monde a le droit de reprendre les idées de chacun. Dès le
moment où l'idée rentre dans l'espace public, n'importe qui a le droit de s'en saisir.

Il existe une liste non exhaustive des œuvres qui sont susceptibles d'être protégées par
le droit d'auteur.

[Voir sur legifrance](https://www.legifrance.gouv.fr/affichCode.do;jsessionid=2C4F7113FE8FF467F20C2C256BBFB270.tplgfr21s_3?idSectionTA=LEGISCTA000006161634&cidTexte=LEGITEXT000006069414&dateTexte=19920703)

On peut notamment y voir que le logiciel fait partie de cette liste.

### Critères de protection

#### Notion d'"originalité", reflétant la "personnalité de l'auteur"

Peut être définie par le "style" de l'auteur. Ce qui changerait obligatoirement si l'œuvre avait
été créée par quelqu'un d'autre.

#### Les droits sont dévolus aux personnes ayant exercé l'autonomie de leur originalité

- Une photographie conforme ne le permet pas
	- Mais une photo floue, si!
- Cas des photos "non artistiques"
- Cas de photos prises par un animal non-humain
	- Ces photos n'ont pas d'auteur

Il y a un lien indissociable

{{< mermaid >}}
graph TD
	Oeuvre --- Originalité
	Oeuvre --- Auteur
	Auteur --- Originalité
{{< /mermaid >}}

### Critères non-pertinents

#### Le mérite

Le droit n'a rien à voir avec le goût. La seule question est de savoir si c'est original ou pas.

## Créations échappant à la protection

Il est parfois plus simple de savoir **ce qui n'est pas une œuvre**. Une création de forme ne
permettant pas l'expression de la personnalité de son concepteur ne sera pas éligible à la protection
par le droit d'auteur. (Table mathématiques, annuaire des marées,...)

Cependant, leur présentaiton graphique pourra être éligible si elle reflète la personnalité de son
auteur (logos, décorations,...)

## Automaticité de la protection

La protection est réputée acquise dès la conception de l'oeuvre

- L.111-1 CPI
- L.111-2 CPI

> On notera dans ces articles la difficulté du législateur à définir la propriété d'une œuvre.

Aucune formalité n'est nécessaire. Il est toutefois prudent de se pré-constituer des preuves de
paternité et d'antériorité. Il est difficile de prouver la véritable paternité d'une œuvre. C'est
finalement sur l'**antériorité** que ce jouera la question de la **paternité**. Si personne ne
peut prouver avoir écrit Harry Potter avant JK Rolling, la paternité de l'œuvre lui sera attribuée.

- Constat d'huissier
- Dépôt chez un notaire
- Dépôt auprès d'associations spécialisées
- Courrier envoyé à soi-même
- Publication de condensats comme preuve de possession

### Cryptographie

Les fonctions de hashage, à partir d'une œuvre numérique donne un condensat. Si l'on peut prouver
l'antériorité du condensat de son œuvre, alors la paternité peut en être déduite. Cependant, il est
nécessaire de garder l'œuvre à l'octet près pour pouvoir prouver l'origine du condensat.
