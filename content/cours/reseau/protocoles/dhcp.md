---
title: "Dhcp"
date: 2019-10-10T08:07:53+02:00
draft: false
---

# Protocole DHCP

Il y a plusieurs façons de distribuer des adresses IP:

- statique
- dynamaique (DHCP) -> bootp
- automatique

## Statique

Configuration manuelle de l'adressage.

## Dynamique (DHCP)

À la connexion, une machine qui veut une adresse va lancer un broadcast. Le
serveur DHCP va lui répondre et lui attribuer une adresse.

L'inconvénient, c'est que si le serveur DHCP tombe en panne, tout le système
d'attribution d'adresses tombe en panne (Même si ceux qui ont déjà une adresse
peuvent profiter pleinement du réseau).

Les serveurs DHCP doivent retenir les baux distribués, notamment en cas de
crash. Cela rah=joute des points de vulnérabilité. C'est pourquoi en IPv6 on a
préféré tirer une adresse au hasard et vérifier si elle n'est pas utilisée, afin
d'éviter un point central vulnérable.

### Distribution statique et dynamique

On peut configurer le serveur DHCP qui attribue la même adresse IP à chaque
machine avec un hostname particulier. On reste sur une attribution dynamique,
mais qui distribue des adresses statiques.

## Automatique

À la connexion, la machine pioche une adresse au  hasard et lance une
requête ARP sur le réseau en demandant si quelqu'un la possède. Si ce
n'est pas le cas, elle s'attribue cette adresse.

In IPv6, le protocole a prévu de prendre automatiquement une adresse qui
ressemble fortement à l'adresse MAC de l'interface, afin d'être quasiment sûr
que l'adresse soit unique (étant donné que les adresses MAC sont censées être
uniques).

C'est le routeur cette fois-ci qui informe de la plage d'adresses disponibles.
Par exemple au CREMI, le routeur informe que ```2001:660:6101:80::/64``` est la
plage d'adresse disponibles. Ce sont déjà des adresses publiques et les machines
à la connexion prennent une adresse sur cette plage.

### Iconvénients

En IPv6, le fait d'avoir l'adresse MAC inscrite dans son adresse IP identifie
d'une part une macihne sur un réseau, et d'autre part informe la série de
construction de l'interface réseau, donc potentiellement les failles inscrites
dans le firmware.

On peut configurer sa machine pour prendre une adresse aléatoire pour palier à
ça.

# Registre des adresses

C'est l'**IANA** (Internet Assigned Numbers Authority) qui distribue les
les plages d'adresse IP mondialement. L'IANA déclare des **RIR** (Regional
Internet Registry). Notre RIR est le **RIPE** (Réseau IP Européen). Les
RIR déclarent des LIR.

Un **LIR** est un "Local IP Registry". Aujourd'hui, l'IANA et le RIPE n'ont
plus d'addreses IPv4 à distribuer. Éventuellement les LIR en ont encore...
