---
title: "Nat"
date: 2019-10-11T08:08:42+02:00
draft: true
---

# NAT (ou Masquerade)

![NAT](/cours/reseau/nat.jpg)

p.13 polycopié

Mis en place pour éviter que chaque équipement domestique ait un adresse IP
publique. Le principe est que le modem routeur visible publiquement sur Internet
va modifier les en-têtes IP des paquets en provenance des équipements internes,
en remplaçant l'adresse source par la sienne (et en modifiant également le port
source) en ajoutant dans une table une règle qui fait correspondre les réponses
de l'extérieur sur le nouveau port avec l'ancien port source de l'adresse IP
privée de l'équipement original.
