---
title: "Protocoles"
date: 2019-09-18T11:11:13+02:00
clsdate: ["2019-09-18", "2019-09-20"]
draft: true
---

# Couche Transport

## TCP

Client -> SYN "est-ce que je peux me connecter?"
Serveur -> SYN + ACK "oui je suis d'accord"
Client -> ACK "ok nickel"

Si un paquet se perd dans ces trois trajets, l'émetteur
du paquet qui s'est perdu peut se douter qu'il y a eu un problème
et peut alors renvoyer son paquet.

{{< mermaid >}}
sequenceDiagram
	Client->>Serveur: Bonjour, est-ce que je peux me connecter?
	Serveur->>Client: Oui je suis d'accord!
	Client->>Serveur: Ok niquel!
{{< /mermaid >}}

voir PDF p.16

# Couche Application

Une fois que la connection TCP est mise en place, on peut discuter avec l'application
que l'on veut.

## HTTP 1.x

# Routage sur le réseau

Protocoles de routage entre les AS:

- RIP
- OSPF
- BGP

{{< mermaid >}}

graph LR
  1 --- 2
  2 --- 3
  3 --- 4
  4 --- 1

{{< /mermaid >}}

## IGMP

Comme ICMP, mais avec G pour "Group".

- **ICMP**: Internet Control Message Protocol
- **IGMP**: Internet Group Management Protocol

Par exemple utilisé pour envoyer des flux TV depuis un équipement réseau sur un
ensemble de Box d'unmême quartier. (Diffusion plutôt qu'envoyer un flux par
personne).

https://fr.wikipedia.org/wiki/Internet_Group_Management_Protocol

IGMP intervient sur la couche Réseau. Alors que pour du multicast en réseau
local, c'est Ethernet donc les cartes résseaux qui vont trier l'info qu'elles
doivent recevoir ou pas. (Toute l'info est envoyée sur le réseau...)

## Notes (diffusion)

- broadcast
- multicast
- unicast
- anycast: "je veux recevoir tel contenu mais je me fiche de savoir de qui il
vient"

## Torrent

Torrent propose un service avec un **tracker** qui retient qui possède quel
fichier, mais il y a également une notion de **DHT** (Distributed Hash Table).

Dans le magnet récupéré, il y a un identifiant pour le fichier (hash). Les
clients ont eux aussi un ID.

Chacun des participants connaît l'adresse des participants proches. Ceux qui
sont proches d'un fichier sont chargés de se souvenir qui possède "tel" fichier.
On a plus besoin de savoir qui possède la liste des gens qui possède le fichier,
mais de savoir qui est proche du fichier.
