---
title: "Programmation Reseau"
date: 2019-09-25T11:04:29+02:00
clsdate: 2019-09-25
draft: true
---

# Programmation Réseau

## File descriptor / Sockets

En programmation réseau, il y a également des descripteurs de fichiers comme
en Programmation Système. Ils sont appelées **socket**.

### UDP

![DNS sur UDP](/cours/reseau/dns-sur-udp.jpg)

Le ```recvfrom()``` est bloquant. il attend les requêtes pour les traiter et leur répronde.
Côté client, on doit penser à ```close()``` le **fd** (socket) à la fin de la réponse.

### TCP

![Protocole UDP](/cours/reseau/tcp.jpg)

Une fois que la connexion est crée avec ```connect()```, il n'y a plus qu'a effectuer
un ```write()``` exactement comme sur n'importe quel **fd** pour envoyer des données.

En TCP, lorsqu'une connexion s'établie un deuxième **fd** est créé représentant cette
connexion (mais le **fd** principal est toujours là, à l'écoute de nouvelles
connexions sur le serveur). Ce nouveau **fd** contient l'IP et le port de destination 
du client.

{{< mermaid >}}
graph LR
	FD-- connect -->FD2
{{</ mermaid >}}

Lorsque le serveur fait un ```read()```, cette fois il le fait sur ce deuxième **fd**.
Un ```write()``` dans une connexion est bloquant, il attend que les données soient
correctement reçues.

Lors du ```close()```, le **fd** correspondant à la connexion TCP se ferme.

### Détails d'un file dscriptor de connexion

|   |   |
|---|---|
| Adress Family | INET/INET6/... |
| type | DGRAM(UDP) / STREAM(TCP) |
| protocole | UDP/TCP/SCTP... |
| dst | IP/port |
| src | IP/port |
| tampons | |
| actif / passif | |

## API

Ces API ont été développé par les systèmes BSD, et adoptées par tous les systèmes
et tous les langages.

### Langage C

p.20/21 polycopié

Notion d'adresses (IP et port)
