---
title: "Intro 3"
date: 2019-09-11T11:40:37+02:00
draft: true
---

Dans les années 70, en opposition aux réseaux à commutation de circuit, on invente
le réseau à commutation de paquet. Certains disent que c'est Louis Pouzin, via
les datagrammes qui a créé ce concept.
Dans les années 70, en opposition aux réseaux à commutation de circuit, on invente
le réseau à commutation de paquet. Certains disent que c'est Louis Pouzin, via
les datagrammes qui a créé ce concept.

Eventuellement, certains paquets se perdent. Mais il existe des protocoles pour
qu'ils soient renvoyés. On dit que le réseau fait du **Best Effort** pour
transmettre les infos.

## RFC (Request For Comments)

Chaque protocole, notamment **IP**, **TCP** ont leur RFC.

## Checksum

Il faut noter la différence énorme entre **fiabilité** et **sécurité**.

Typiquement, la couche transport est fiable mais pas sécurisée. On préfère
donner la responsabilité de la sécurité à la couche application. Mais la
couche transport est très "bête".

### Exemple de la carte vitale

Le numéro de sécurité sociale comporte 13 chiffres suivis d'une clé de deux chiffres.

r = n % 97
clé = 97
n + clé = n + 97 - n % 97

Le checksum sert donc à vérifier que l'on a bien saisi le numéro de sécurité sociale.

Cependant, cette **fiabilité** de garantie pas la **sécurité**.

### Couche transport

Voir CRC

## Les en-têtes

Voir polycopié p.14

En IPv4 et IPv6, les paquets on une MTU (Maximum Transmission Unit) de 65535 bits.

Il a été décidé en réseau que les octets soient lus en commençant par les octets de poids fort
(voir octets poids fort - faible). Par exemple si on regarde les infos transportées,
l'adresse destination va être lu depuis le début jusqu'à la fin, comme dans le sens de lecture.

## TCP

Par rapport à UDP, TCP apporte une notion de **flux** et de **connection**.

- flux: l'information arrive dans le bon ordre, avec un début et une fin.

Quand TCP envoie les infos trop vites (par exemple un serveur débite ses
données trop vite par rapport au débit du fournisseur d'accès du client)

### Slow Start

- Au départ, le serveur commence à émettre lentement
- Le client renvoie au serveur que tous les paquets sont reçus
- Le serveur augmente son débit de façon exponentielle
- Quand ça commence à s'engorger, des paquets sont perdus
- Le serveur ralenti son débit
- Ce processus est constant et s'adapte

On a ansi un système qui s'adapte sans cesse. Par exemple si un autre
utilisateur utilise la bande passante pendant mon téléchargement,
ce dernier aura un débit plus fort lorsque l'autre utilisateur de mon
accès stoppera son activité sur le réseau.
