---
title: "Authentification"
date: 2020-01-15T14:29:12+01:00
draft: true
---

# Authentification

## Identification / Authentification / Autorisation

## Facteurs d'authentification

### Facteur mémoriel (ce que je sais)

- Mot de passe
- Nom de jeune fille de ma mère
- Code confidentiel
- Nom d'utilisateur
- ...

### Facteur matériel (ce que je possède)

C'est une information contenue dans un objet que je possède

- Clé USB
- carte à puce
- Téléphone mobile
- Badge
- ...

### Facteur corporel (ce que je montre)

- Balayage de la rétine
- Reconnaissance vocale
- Emprunte digitale
- Forme de la main
- ...

### Facteur réactionnel (ce que je fais)

- Signature

## Méthodes de vérification

### Authentification simple

Ne repose que sur un facteur

### Authentification forte

Repose sur **au moins** deux facteurs (ex: login + mot de passe + code généré)

### Exemples

- disconnected token
- connected token
- NFC token
- smartphone token
- RSA SecureID

### Authentification unique (SSO)

Une seule authentification pour accéder plusieurs applications. Permet de
pouvoir demander aux utilisateurs des mots de passe plus forts.

## Mot de passe

L'utilisateur ne choisit pas toujours son moyen d'authentification. Il faut un
**mot de passe** robuste.

### De quoi dépend la robustesse d'un mot de passe?

- Complexité
- Mécanisme de vérification (temps de vérification plus long -> plus robuste)
- Modèle d'attaquant et moyens (en fonction de l'importance des données à
protéger)
- Nombre d'authentification ratées autorisées
- Mécanismes d'alertes

### Recommandations de l'ANSSI

| n | recommendation |
|:-:|:---------------|
| 1 | Utiliser des mots de passes différents pour des systèmes distinctcs |
| 2 | Choisissez un mot de passe qui n'est pas lié à votre identité |
| 3 | Ne demandez jamais à un tiers de vous créer un mot de passe |
| 4 | Modifier systématiquement les mots de passe par défaut |
| 5 | Renouvelez vos mots de passe avec une fréquence raisonnable |
| 6 | Ne stockez pas vos mots de passe dans un fichier sur un poste exposé |
| 7 | Ne vous envoyez pas vos mots de passe sur votre messagerie personnelle |
| 8 | Configurer les logiciels et navigateurs pour qu'ils ne se souviennent pas des mots de passe saisis |

### Protection des internautes: Sanction CNIL

- 8 caractères
- Au moins 3 types de caractères précédents
- Pas de lien avec détenteur
- Renouveler tous les 6 mois
- Différent des mots de passe précédents

### Stockage des mots de passe

- Utiliser un grain de sel pour le calcul des hash des mots de passe
- Faire en sorte d'avoir un sel différent par utilisateur

## PKI (Public Key Infrastructure)

PKI -> Certificats numériques -> Authentification / Signature

### Autorité de cerification

Une AC / CA permet de signer des CSR (Certificate Signing Request) et CRL
(Certificat Revocation List)
