---
title: "Les Modeles Economiques"
date: 2019-10-21T11:48:53+02:00
draft: true
---

# Les modèles économiques

## Le modèles des communautés

Les communatués se perçoivent elles-même souvent comme étant à l'écart des
mécaniques économiques, voire dans une relation antagoniste vis à vis du monde
des entreprises.

De façon désintéressée, les contributeurs espèrent gagner la reconnaissance de
keurs pairs, $etre eux-mêmes fiers du travail qu'ils ont réalisé, échanger avec
des développeurs de haut niveau et produire des travaux utiles tout en
progressant.

## Le modèle des fondations (foundations)

Le modèle des fondations open source, quant à lui, est caractérisé par la
mutualisation des efforts de R&D. Dans ce modèle, les équipes d'ingénieurs de
différentes entreprises collaborent à un même projet. Dans certains cas, la
propriété intellectuelle issue de ces travaux communs peut être cédée à la
fondation.

Ce modèle étant celui de la mutualisation de R&D, il permet parfaitement de
répondre à des besoins génériques communs, qu'aucune entreprise n'aurait les
moyens de financer individuellement.

Il est particulièrement adapté à la production des composants qui ne sont pas
différenciants (brique d'infrastructure), ou ne présentatnt pas de valeur
marchande intinsèque (navigateur web).

## Le modèles des éditeurs Open Source

Un éditeur open source est une entreprise comme les autres. C'est-à-dire à but
**lucratif**. Cependant, elle développe et distribue des produits qu'elle publie
sous une licence Open Source.

Le développement de ses logiciels représente un coût, que l'entreprise finance
grâce à la perspective d'un retour sur investissement et de bénéfices futurs.

Les éditeurs Open Source se trouvent également au centre de l'écosystème du
libre car ils animent les communautés d'utilisateurs, de contributeurs et de
prstataires.

## Le modèle des communautés

## Le modèle des fondations

- Caractérisé par la mutualisation des efforts de R&D
- 
- 
- Permet parfaitement de répondre à des besoins génériques

## Le modèle des éditeurs Open Source

- Entreprise à but luccratif comme les autres. Cependant, développe et distribue
des produits qu'lele produit sous licence libre.
- Développmement des logiciels représente un coût, que l'entreprise finance
grâce à la perspective d'une retour investissement et de bénéfices futurs.
- Les éditeurs Open Source se trouvent également au centre de l'écosystème du
libre car ils animent les communautés d'utilisateurs, de contributeurs et de
prestataires.

### Variantes

- Le modèle de la Fondation Éditrice (eclipse)
- Le modèle de la double licence
    - Le logiciel développé est libre, sous une licence à copyleft fort (GPL ou
AGPL) ce qui ne permet pas de l'intégrer, sous cette licence, dansn un produit
ou service propriétaire.
    - Une deuxième licence, de type *propriétaire*, est vendue aux utilisateurs
qui ouhaitent justement s'affranchir des contraintes du copyleft.
- Le modèle de l'open core (et ses variantes):
    - L'éditeur développe unnoyau technique open source, et un produit
propriétaire au-dessus de ce noyau, ou bien développe deux produits au-dessus
d'un noyau commun, l'un en open source et l'autre, avec des fonctions
additionnelles, vendu sous une licence traditionnelle. Dans ce dernier cas, les
deux vairantes du logiciel sont souvent baptisées **community** et
**entreprise**.

#### Le modèle de l'Open Source professionnel

Il s'agit du modèle "dans lequel l'éditeur tire ses revenus de services
professionnels de la maintenance et du support associés au logiciel qu'il
édite."

#### Le modèle du Cloud

L'éditeur développe un produit Open Source et en tire des revenus en le
proposant sous forme de service dans le cloud.

#### Le modèle de  intégrateurs (et autres prestataires de services)

Anciennement dénommées **SSLL** (Société de Services en Logiciels Libres", ce
sont maintenant des **ENL** (Entreprise Numériques Libres).

Elles font rarement exclusivement dans le Logiciel Libre, car elles doivent
composer avec les exigences de leurs clients (parc existant *legacy*).

Elles tirent leurs revenus de l'intégration et du support de solutions Open
Source qu'elles réalisent pour des entrepruses et des admlinistrations. Leur
étier consiste à proposer des services permettant d'exploiter les logiciels
libres sur la totalité du cycle de vie.

Elles évoluent dans un écosystème composé de deux grands types de partenaires:
les clients (administrations, tertaire, milieu associatif, grands compes
industriels et PME) et les fournisseurs de technologies (éditeurs, communutés,
milieu académique).

### Motivation économique de l'éditeur Open Source professoinnel

La tendance naturelle, pour un éditeur de logiciel, open source non, est de
rechercher à terme un revenu qui soit, en grande partie, passif et récurent. ILe
espère ainsi recouvrer l'investissement initial, en rémunérant par ailleurs la
prise de risque liée à cet investissement.

Pour cela, il int_gre dzna snon offre plusieurs tyoes de services:

- Conseil
- Formation
- Customisation
- Développement de solutions spécifiques

L'éditeur open Source pourra principalement développer cinq sources de revenus
récurrents:

- Support forfaitaire
- La maintenance
- Les garanties (de fonctionnement, juridique, ...)
- Les services complémentaires (en SaaS notamment)
- Les partenariats (payants) avec les intégrateurs

#### Principaux avantages du modèle d'éditeur Open Source
(par rapport à un modèle d'éditeur propriétaire) sont:

- Une activité de développement plus efficace
- Un marketing et une évangélisation plus simple à mettre en œuvre puisque la
solution est librement "testable"
- Des cycles de vente plus courts et mieux ciblés (car les équipes commerciales
peuvent commencer à intervenir une fois que les clients ont commencé à tester la
solution en interne)
- La constitution d'un écosystème favorisatn les apports tiers (club
utilisateur).

#### Principale faiblesse du modèle d'éditeur de l'Open Source:

L'un des principaux incovénients du modèle réside dans la difficulté du
financement par les investisseurs existant (même les "Venture Capitalists"), qui
attendent souvent à trouver des barrières fortes à l'entrée et à la sortie pour
justifier un retour sur investissement.

La situation tend à s'émeliorer, mais il reste toujours difficile de  convaincre
ces investisseurs de la rélité et de la validité de ces modèles économiques.

### Motivation économique de l'éditeur en mode Cloud

Ce modèle à valoriser le service rendu par le logiciel Open Source e, se plaçant
comme "opérateur" et en laissant l'utilisateur n'être qu'un simple
"consommateur" qui paye de façon récurente (monétisation).

L'ouverture du code est justifiée par l'existence d'une communauté qui participe
à la création de valeur (contribution au code), réduisant de ce fait les coûts
de R&D portés par l'éditeur.

## Économies de l'éditeur en mode 'Double Licence'

Dans ce modèle, l emême logiciel, open Source, est publiée sous une licence à
copyleft fort (GPL v3), et vendu sous forme de produit packagé, souvent compilé,
accompagné de services (formations, installation, stockage, backups, etc.)

Une version du même produit existe avec une licence payante pour les éditeurs,
ou des clients, souhaitant développer un produit/service spécifique, mais qui ne
souhaitent pas publier leurs travaux.

# Étude de cas

## Illustrer divers modèles économiques (3 ou 4)

(éditeurs, open core, Cloud, services,...)

- Trouver des sociétés sur Internet
- Dire celui (ou ceux) qu'elle(s) utilisent
- Décrire leurs sources de revenus

## Dire lequel vous paraît le plus pertinent, et pourquoi

## Addendum

Tout ce qui précède se rapporte aux Logiciels Libres, mais...

...la tendance et kes vaeurs issues ont aussi produit:

- Du matériel Libre, ou Open Hardware
- Des contenus libresn comme Wikipédia et la Musique Libre, mais aussi de l'Open
Data
- Des modèles économiques collaboratifs, sous le vocable "open business"
