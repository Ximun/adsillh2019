---
title: "Introduction"
date: 2019-10-14T09:40:50+02:00
draft: false
---

# Sommaire

- Préambule
- Le bouleversement du numérique
- Impacts économiques des modèles libres
- Les divers modèles économiques
- La motivation derrière chaque modèle économique
- Questions / Réponses

# La gratuité est un mythe?

> "Dans un service, si c'est gratuit, c'est vous le produit"

> "Un logiciel libre est gratuit...<br>
> ... une fois qu'il a été payé"

# Brève histoire du logiciel

## Début des ordinateurs

Vente des gros ordinateurs hors de prix. Les logiciels étaient naturellement
libres et fournis avec les machines.

Rapidement après:

- Baisse du coût du matériel
- Uniformisation de la couche de gestion du matériel (création des OS,
naissance d'Unix)
- Naissance de nouveaux langages compilés plus "productifs" -> naissance
des développeurs

Les fournisseurs réalisent que le service est rendu par le programme alors
que le matériel suit une guerre des prix.

## Création de contrats commerciaux relatifs aux programmes

Apparaît la dualité Sources/Exécutables. Plusieurs modèles apparaissent:

- Vente des exécutables, protégées avec des DRM.
- Vente des sources, associées à des clauses restrictives (notamment avec
les bibliothèques fermées utiles au logiciel.
- Mise à disposition de k'exécutable subordonnée au paiement d'une
redevance.

<center>**Le principe de la licence logicielle est né**</center>

# L'ère du numérique

Le logiciel est le premier outil de 'homme qui soit une extension de son
esprit.  C'est une révolution concidérable dans la façon de produire et
traiter la connaissance.

La révolution numérique fait suite à deux révolutions précédentes dans
le champ de la connaissance:

- Révolution de l'écriture
- Révolution de l'imprimerie

## Apparition du réseau

Petit à petit, tout le monde a accès au réseau, et cet accès permet des
échanges "horizontaux".

La numérisation permet pour la première fois la **copie parfaite** d'un
original, et l'accès à l'Internet permet la copie entre deux points avec
un **coût quasiment nul**.

## Économie des biens immatériels

L'économie des biens immatériels diffère fondamentalement de celle des
biens matériels. Ce sont des biens **non-rivaux**.

- Le coût marginal de copie est nul.
- Tout bien déjà financé peut être distribué gratuitement.

La gratuité n'est donc **pas un mythe**.

- Il n'y a pas de limite dnaturelle à l'usage des créations.
- La fixation du prix est artificielle et arbitraire.

Les anciens modèles de transaction basés sur la rareté ont été conçus
pour une économie de **rareté**. Une telle économie peut supporter des
coûts de transaction élevés (brevets, contrats,...)

La révolution numérique précipite la société dans un univers
d'abondance. Un tel mécanisme de transaction n'est absolument plus viable,
puisque les ressources ne sont plus rares. Les licences libres sont un des
outils de transaction adaptés correspondant à la vitesse de l'Internet.

Il faut comprendre la différence entre le **coût de revient** et **la
valeur ajoutée**. Le coût de revient doit bien sûr être couvert, mais
la vraie valeur du logiciel (c'est le même cas pour les œuvres d'art)
vient de la valeur ajoutée.

Les effets de réseau sont **considérables**.

### Émergence du **modèle de la "coopétition":

- Améliore la résilience individuelle par la mutualisation de la prise de risque
- Construction de communautés pour accroître la valeur de l'écosystème

### L'innovation ouverte

- On constate la création de Communautés *captives*.
- Moins d'incitation à innover.
- Moins de résilience en cas d'accidents d'entreprise

### Innovation libre

Vise à partager également la gouvernance du projet. ON acceptede perdre en
**contrôle** ce que l'on gagne en **agilité**. il y a donc plus d'incitations
à faire émerger des coopétiteurs.

### On oubli le modèle de rentes

#### Expansion du marché par la "commmoditisation"

La **commoditisation** est le processus par lequel un produit ou
service, différencié par un autre attriubt que le prix, perd cette
différenciation. Il en résulte une guerre des prix, seul différenciant
restant. La valeur perçue au-delà du simple usage du produit ou service
est nulle.

#### Focalisation sur la **valeur ajoutée**

On préfère investir ans la construction de communautés au sein de
l'écosystème, et promouvoir la compétence pour conserver le leadership
au sein de la communauté.

# Valeur des logiciels

## Tout logiciel à un coût

Lié aux moyens mis en œuvre pour le produire. Il est généralement facile à
quantifier.

## Tout logiciel a une valeur

Une **valeur d'usage** qui découle du service qu'il rend, et une **valeur
intrinsèque** qui comprend:

- l'expertise contenue au sein du code source
- maintenabilité, extensibilité, réutilisabilité
- très difficile à quantifier!
- décorrélée du coût de production

L'important est de bien différencier le **coût** et la **valeur**.

<div class="alert alert-info"> L'éclatement de la bulle Internet vient du
pari fait par des financiers de payer le coût pour espérer récolter les
fruits de la valeur.</div>

# Enjeux et impacts économiques des Modèles Libres

Dans de nombreux domaines, les logiciels libres sont devenues un sujet
stratégique:

> "À terme, la majorité des logiciels seront des logiciels libres"
- *Gartner*

En effet les dernières situations de monopole sont maintenues artificiellement
(Microsoft, Oracle,...). De plus en plus se pose la problématique de
l'indépendance des états, et de la souveraineté de leur défense nationale.

## Impact sur l'industrie informatique

- L'accès aux innovations est plus simple et plus rapide. (ne pas réinventer
la roue)
- Cycles de développement plus courts
- Méthodes de développement coopératives
- "Contre-pouvoir" face à des monopoles et des standards fermés.
- Économies sur les frais juridiques de développement (coût des brevets,
...) et de production (royalties, redevances)

## Avantages

Du fait de sa libre disponibilité, la plus grande diffusion du logiciel
permet d'atteindre, plus rapidement, les seuils critiques d'adoption.

L'affichage de *valeurs positives* apporte un gain en notoriété, et des
retombées favorables en termes d'image (image dynamique, de respect et
d'indépendance).

On constate une plus grande motivation individuelle des différents
intervenants autour de ce type de projet/produit.

On dispose sans surcoût de plus de testeurs, et de plus de retours
utilisateurs.

L'accès à une très grande quantité de briques existantes, réutilisables
et combinables dans presque tous les domaines.

Une plus grande durée de vie des profuits, liée ç a pérennité des
technologies, à une meilleure maintenablité et à l'usage de formats ouverts.

La grande pérennité des connaissances en logiciels libres: les connaissances
d'il y a 10 ans dans le domaine de Linux restent valables aujourd'hui, là
où un éditeur propriétaire est contrait de faire des modifications pour
justifier la succession des versions, et refacturer produit et formations
associée. **La valeur des connaissances en logiciels libres ne diminue pas
avec le temps*.

Les *contributions externes*, quelles qu'elle soient, ne coûtent rien.

## Inconvénients pour l'entrepreneur

- Les revenus à la revente de licences disparassent
- L'avantage concerrentiel lié au secredt du code diminue.
- Il est plus difficile d'enfermer les clients
- La libre concurrence induit souvent une diminution des prix

Il est donc nécessaire de trouver son équilibre économique différemment. S'il
n'est plus possible de vendre des licnces, il est toujours possible de vendre le
logiciel.

<div class="alert alert-info">
Les entrepreneurs diffusant des logiciels libres doivent lutter contre préjugés
qui freinent leur progression et leur adoption dans les entreprises.
</div>

- **La sécurité**: Alors même que tous les spécialistes montrent que la sécurité
par l'obscurité ne peut en aucun cas être un gage de fiabilité réelle.
- **Les responsabilités**: On pense à tord qu'il n'y a pas de *responsable*,
alors qu'il est possible de souscrire du support (ou de développer une expertise
interne).
- **Les coûts dits cachés**: Le déploiement, la configuration et la formation
ont bien un coût, mais il est le plus souvent moindre au final que l'ensemble de
la facture demandée pour des logiciels propriétaires (qui ont aussi de tels
coûts).

## Les risques économiques qui pèsent sur le secteur des Logiciels Libres

### Le brevets logiciels

Bien qu'ils ne soient pas valides en Europe, les logiciels libres sont menacés
par les brevets logiciels dès qu'on envisage de travailler au niveau mondial.

### La qualité intrinsèque

Lorsqu'on base son activité économique sur un ou des logiciel(s) libre(s), sa
qualité influe sur cette activité économique. Mais c'est le même problème pour
un logiciel non libre (qui n'est pas forécement meilleur).

### LA capacité du logiciel à être l'objet d'une activité économique

L'adéquation des logiciels libres à un marché est parfois délicate à établir ou
à démontrer faces au FUD.

### La popularité, la  durée de vie et le facteur communautaire sont versatiles

## Contributions - Collaboration - Communauté

La participation de contributeurs externes est bien une réalité, mais elle est
souvent difficile à maîtriser.

- Les contributions non techniques sont assez fréquentes.
- Les contributions techniques sont assez rares.
- Les *bonnes* contributions techniques sont encore plus rares.

Intégrer des contributions demande du temps. La balance des exigences est
fortement déséquilibrée.

- Les développeurs sont bien plus exigeants que les utilisateurs pour ce qui est
de la qualité technique des développements.
- Les utilisateurs sont toujours plus exigeants que les développeurs en ce qui
concerne l'ergonomie et l'utilisabilité du service rendu.

## L'insertion dans les communautés : être (ou devenir) contributeur

Il faut consacrerdu temps pour mettre et maintenir à jour ses connaissances
(lire de nombreuses listes de diffusion, des sites web, des blogs, etc.)

[...]

## Conclusion

Dans l'écosystème de l'open source, on distingue les modèles suivants:

- Le modèles des communautés
- Le modèles des fondations
- Le modèle des intégrateurs et des prestataires de services (qui appraît comme
étant moins spécifique du logiciel libre).
