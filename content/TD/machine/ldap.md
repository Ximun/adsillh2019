---
title: "Ldap"
date: 2019-09-20T08:48:10+02:00
draft: true
---

# TD LDAP

Voici les consignes du TP.

- Construire un schéma d'annuaire
- Installer un annuaire LDAP
- Installer son interface graphique d'admin
- Reproduire son schéma dans l'annuaire

## Construire un schéma d'annuaire

{{< mermaid >}}

graph TD
	dc=fr --> ou=kaamelott
	ou=kaamelott --> dmdName=groups
	ou=kaamelott --> dmdName=users
	
	dmdName=users --> uid=Arthur
	dmdName=users --> uid=Lancelot
	dmdName=users --> uid=Guenièvre
	dmdName=users --> uid=Perceval
	
	dmdName=groups --> cn=tableronde
	dmdName=groups --> cn=dames

{{< /mermaid >}}
