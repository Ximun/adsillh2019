---
title: "Td1"
date: 2019-09-13T14:06:33+02:00
clsdate: 2019-09-13
draft: false
---

# TD1: Calculs de débits et de latences

(voir feuille de TD)

## Exercice 1

{{< katex >}}
\begin{array}{l|l|l}
	1\,octect & 8\,bits \\
	\hline
	1\,Ko & 1\,000\,octets \\
	\hline
	1\,Mo & 1\,000\,000\,octets \\
	\hline
	1\,Go & 1.10^9\,octets \\
	\hline
	1\,To & 1.10^{12}\,octets \\
	\hline
	16\,Mo & 128\,Mbits & 128\,000\,Kbits \\
	\hline
	2\,To & 2.10^6\,Mo & 16.10^6\,Mbits \\
	\hline
	125\,Ko & 1\,000\,Kbits & 1\,Mbits \\
	\hline
	10^6\,Gb & 10^3\,Tb & 125\,To \\
	\hline
	320.10^6\,Kbits & 40.10^6\,Ko & 40\,Go
\end{array}
{{< /katex >}}

## Exercice 2

### Question 1

Le débit nécessaire à l'envoi des 300 Go dans une journée est de:

{{< katex >}}
\frac{300 Go}{1 j} = \frac{2\,400\,000 Mb}{24 \times 60 \times 60 s} = 24 Mbps
{{< /katex >}}

Une simple ligne ADSL 18Mbps/1Mbps ne suffit donc pas.

### Question 2

Le quota est atteint au bout de:

{{< katex >}}
\frac{2 To}{200 Mbps} \approx \frac{8 \times 2\,000\,000 Mb}{200 Mbps} \approx 87\,600 s \approx 22 h
{{< /katex >}}

Le débit constant pour éviter de dépasser le quota est de:

{{< katex >}}
\frac{2 To}{30 \times 87\,600 s} \approx 6,2 Mbps
{{< /katex >}}

### Question 3

Le temps que met un photon à parcourir la fibre est de:

{{< katex >}}
\frac{200 km}{200\,000 km/s} = 1ms
{{</katex>}}

Donc le nombre d'octets "en transit" dans la fibre est de:

{{< katex >}}
1 ms \times 10 Gbps = 10 Mb
{{< /katex >}}


## Exercice 3

### Question 1

La latence de ce moyen de communication est de 4s (le temps de charger les données, le temps de l'envoi puis le temps de réception). En revanche, le débit est de **2s**.

<div class="alert alert-danger">
Le débit ne concerne que le temps que prend le lanceur entre l'envoi de deux clés USB !
</div>

### Question 2

Le temps que met l'ordinateur à charger 32Go de données sur une clé est de:

{{< katex >}}
\frac{32\,Go}{10\,Mo/s}=\frac{32\,000\,Mo}{10\,Mo/s}=3200\,s
{{< /katex >}}

Ce à quoi doivent s'ajouter les 4s de latence de la Question 1. Donc la latence de ce mode de transmission est de 3204 secondes.

Cette fois-ci, le temps que prend le lanceur entre deux lancers est beaucoup plus long. Le temps avant que la clé soit émise est de 3202 secondes.
Le débit est donc de:

{{< katex >}}
\frac{10\,Mo}{3202\,s}=\fbox{3,125\,Ko/s}
{{< /katex >}}

## Exercice 4

### Question 1

Non, 50ms ne sera pas significatif dans le temps d'envoi complet des données. Ce qui nous importe est le débit auquel j'envoie
toutes mes données.

### Question 2

Le temps d'envoi d'une vidéo de 1Go sur le serveur est de:

{{< katex >}}
\frac{1\,Go}{256\,Kbps}=\frac{1\,Go}{32\,Ko/s} \approx 30\,000\,s
{{< /katex >}}

Pour calculer le temps de réception depuis le serveur on utilisera le débit d'envoi de celui-ci, inférieur au débit d'écriture du disque dur.

{{< katex >}}
\frac{1\,Go}{860\,Kbps}=\frac{8.10^6\,Kb}{860\,Kbps}\approx9300\,s\approx2h30
{{< /katex >}}

### Question 3

Inférieur à 1MTU sur IP, le message sera contenu dans un seul paquet donc prendra le temps de la latence, à savoir 50ms,pour arriver à destination.

### Question 4

Non, cette fois la question 1 n'est plus valide!

### Question 5

J'ai réduit ma latence très fortement en prenant la fibre, mais de l'autre côté la latence est toujours celle de l'ADSL.
Donc la latence n'a pas changé.

### Question 6

À l'envoi, on prend le débit du disque dur:

{{< katex >}}
\frac{1\,Go}{1\,Mops}=\frac{8.10^3\,Mb}{8\,Mbps}=1000\,s\approx16\,mn
{{< /katex >}}

La durée de réception n'a pas changé car limitée par le débit d'envoi du serveur.

# TD2 Bis - Calculs d'adresses

## Exercice 1

### Question 1

192.168.122.3 fait partie de mon réseau mais pas 192.168.113.1

### Question 2

Attention! Certaines adresses de mon réseau sont réservées:

{{< katex >}}
\begin{array}{l}
	\textcolor{Red}{192.168.0.0} \\
	192.168.0.1 \\
	\cdots \\
	192.168.255.254 \\
	\textcolor{Red}{192.168.255.255}
\end{array}
{{< /katex >}}

La première sert à **désigner** le réseau. \\
La deuxième sert au **broadcast**, c'est-à-dire sélectionner toutes les adresses du réseau.

Sur le réseau 192.168.10.1**/16**, **16** bits sont réservés, il me reste donc **16** bits sur lesquels donner des adresses.
En tenant compte des 2 adresses réservées, le nombre d'adresses que j'ai donc à ma disposition est de:

{{< katex >}}
2^{16}-2=65534
{{< /katex >}}

### Question 3

Je peux avoir 256 sous-réseaux, avec dans chacun un nombre d'adresses de {{< katex >}}2^8-2=254{{< /katex >}}

## Exercice 2

### Question 1

#### a)

On met le bit de poids fort à gauche. L'adresse IP en question s'écrit donc en décimal:

{{< katex >}}
\newcommand{\p}{\footnotesize}
\newcommand{\n}{\normalsize}
\newcommand{\red}{\textcolor{Red}}

\begin{array}{cccccccc}
  \p 128 & \p 64 & \p 32 & \p 16 & \p 8 & \p 4 & \p 2 & \p 1 \\
	0   & 0  & 1  & 0  & 1 & 1 & 0 & 1
\end{array} \\
1+4+8+32 = 45
{{< /katex >}}

#### b)

De la même manière, la valeur de mon masque 0b11100000 est 32+64+128=224 en décimal.

#### c)

Sur un /3, trois bits sont réservés. Donc toutes les possibilités d'adressages sont:

{{< katex >}}
\begin{array}{r|c}
	001 & 00000  \\
	    & 00001  \\
	    & \cdots \\
	    & 11110  \\
	001 & 11111  \\
\end{array}
{{< /katex >}}

Ce qui fait un nombre d'adresses possibles de:

{{< katex >}}
2^5-2=30
{{< /katex >}}

### Question 2

#### a)

{{< katex >}}
\begin{array}{l|c|c}
	adresse & 96 & 01100000 \\
	\hline
	masque & 240 & 11110000 \\
\end{array}
{{< /katex >}}

Adresses disponibles sur le réseau:

{{< katex >}}
2^4-2=14
{{< /katex >}}

#### b)

Je dois allouer 2 bits à chacun de mes sous-réseaux car il me faut au minimum:

- une adresse de sélection
- une adresse à allouer à une machine
- une adresse de broadcast

Il me faut donc des /6 dans mon réseau /4.

#### c)

Chacun des sous réseaux comporte 2^2-2=2 adresses.

## Exercice 3

### Question 1

#### a)

{{< katex >}}
20 = 16+4 \\
\begin{array}{cccc}
	11111111 & 11111111 & 11110000 & 00000000 \\
	255      & 255      & 240      & 0 \\
\end{array}
{{< /katex >}}

Le masque correspondant à /20 est donc 255.255.240.0

#### b)

On sait que la limite du masque se situe sur le 3ème paquet de bits. Le réseau sera donc compris entre
164.32.0.0/20 et 164.32.255.0/20.

On sait que notre adresse 164.32.120.18/20 est sur le réseau, donc à partir de notre 120, on en déduit
le binaire et à partir du masque on cherche la plus "petite" adresse possible. C'est grâce à elle
qu'on va nommer notre réseau.

Pour convertir 120 en binaire, il faut partir du bit de poids fort et se demander s'il est contenu dans le nombre.
Si oui, on le soustrait au nombre décimal et on recommence l'opération jusqu'à obtenir 0.

{{< katex >}}

\begin{array}{l|r}
	120 &
	\begin{array}{c}
		\p 128 & \p 64 & \p 32 & \p 16 & \p 8 & \p 4 & \p 2 & \p 1 \\
		 0 & \red 1 \\
	\end{array} \\
	\hline
\end{array} \\

\begin{array}{l|r}
	120-64 = 56  &
	\begin{array}{c}
		\p 128 & \p 64 & \p 32 & \p 16 & \p 8 & \p 4 & \p 2 & \p 1 \\
		 0 & 1 & \red 1 \\
	\end{array} \\
	\hline
\end{array} \\

\begin{array}{l|r}
	56-32 = 24 &
	\begin{array}{c}
		\p 128 & \p 64 & \p 32 & \p 16 & \p 8 & \p 4 & \p 2 & \p 1 \\
		 0 & 1 & 1 & \red 1 \\
	\end{array} \\
	\hline
\end{array} \\

\begin{array}{l|r}
	24-16 = 8 &
	\begin{array}{c}
		\p 128 & \p 64 & \p 32 & \p 16 & \p 8 & \p 4 & \p 2 & \p 1 \\
		 0 & 1 & 1 & 1 & \red 1 \\
	\end{array} \\
	\hline
\end{array} \\

\begin{array}{l|r}
	8-8=0  &
	\begin{array}{c}
		\p 128 & \p 64 & \p 32 & \p 16 & \p 8 & \p 4 & \p 2 & \p 1 \\
		 0 & 1 & 1 & 1 & 1 & 0 & 0 & 0 \\
	\end{array} \\
	\hline
\end{array} \\

{{< /katex >}}

On sait donc que 120 = 0b01111000. Connaissant le masque, on sait
aussi que le réseau est défini à partir des 4 derniers bits.

{{< katex >}}
0111\underbrace{1000}_{\p Réseau} \\
{{< /katex >}}

Donc la plus "faible" adresse du réseau est:

{{< katex >}}
0111\underbrace{0000}_{\p Réseau} \\
{{< /katex >}}

Or,

{{< katex >}}
0b0111000 = 112
{{< /katex >}}

Donc l'adresse du réseau est:

{{< katex >}}
\fbox{164.32.112.0/20}
{{< /katex >}}

#### c)

Sur mon /20, il me reste 12 bits d'adressage. Le nombre d'adresses que j'ai de disponibles
est donc de:

{{< katex >}}
2^{12} - 2 = \fbox{4094}
{{< /katex >}}

### Question 2

{{< katex >}}
252 = 255-3 \\
252 = 0b11111100
{{< /katex >}}

<center>Il y a donc:</center>

{{< katex >}}
\begin{array}{c}
	11111111 & 11111111 & 11111100 & 00000000 \\
	255 & 255 & 252 & 0 \\
\end{array}
{{< /katex >}}

<center>**22** bits réseaux fixés par ce masque.</center>

### Question 3

On a sur 164.32.120.18 /20 un nombre d'adresses possibles sur 12 bits.
Donc 2^12 = 4096 possibilités.

Or 20 sous-réseaux de 1000 machines représente 20000 machines donc 20000 adresses.
C'est donc impossible.

## Exercice 4

En IPv6, chaque bit hexadécimal est codé sur 4 bits binaire. Donc un paquet de 4 bits
hexadécimaux est codé sur 16 bits binaires.

### Question 1

#### a)

Un masque de sous réseau pour un /64 en IPv6 est ffff:ffff:ffff:ffff:0000:0000:0000:0000
(4 pacquets de 4 bits hexadécimaux).

#### b)

#### c)

Sur un /64, il y a 2^64 adresses possibles

{{< katex >}}
2^{64} //
= (2^{10})^6 x 2^4 //
= (2^{10})^3 x (2^{10})^3i x 2^4
= 2^{30} x 2^{30} x 2^4
{{< /katex >}}
