---
title: Bienvenue
type: docs
---

# Licence Pro Adsillh 2019 / 2020

## Bienvenue

Ce site web constitue le rassemblement des notes prises pendant les cours et
mises en forme. L'idée est qu'il soit aussi collaboratif que possible.
N'hésitez donc pas à le consulter ainsi qu'à rajouter / modifier / améliorer
ce que bon vous semble !

Attention, je suis un étudiant :) donc peut-être que je risque d'écrire des
bêtises... Vérifiez donc les infos et si vous trouvez quelquechose qui
peut être amélioré, n'hésitez pas à vous créer un compte sur
[Framagit](https://framagit.org) et à apporter vos modifications au projet,
vous pouvez aussi bien entendu également forker le site!

Enfin, n'hésitez pas à me contacter!

## Hiérarchie

Le menu suit la structure des cours, à savoir classé par matière et par
chapitre. Un blog est également disponible pour les notes annexes ou les
petits trics à partager...

